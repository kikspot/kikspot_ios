//
//  VenueDetailsViewController.swift
//  KikSpot
//
//  Created by Apple on 03/04/18.
//  Copyright © 2018 Shruti Mittal. All rights reserved.
//

import UIKit
import MBProgressHUD
import CoreLocation
import ReadMoreTextView

var BackButtonComingFrom = ""

class VenueDetailsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate,CLLocationManagerDelegate{
    
    @IBOutlet var rewardValuesView: UIView!
    
    @IBOutlet var ActiveRewardLabel: UILabel!
    @IBOutlet var rewardLabelValue: UILabel!
    var imagePicker:UIImagePickerController?=UIImagePickerController()
//    var popover:UI?=nil
//    var controller:UIViewController = UIViewController()
    var locationImage : UIImage!
    
    @IBOutlet var rewardsImage: UIImageView!
    @IBOutlet var rewardValueViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var rewardlabelHeightConstraints: NSLayoutConstraint!
    var locationId : String!
    var locationName : String!
    var activeRewardvalue : String!
    var Rated = false
    let locationManager = CLLocationManager()
    var didFinishCallingLocation = false
    var longitute: String!
    var userLocationRatingID: String!
    
    var latitude: String!
    
    var sliderValue : String!
    
    var locationCommentsArray : NSMutableArray = []
    var ActiveRuleDetailsArray: NSMutableArray = []
    
    
    @IBOutlet weak var rateThisVenueLAbel: UILabel!
    @IBOutlet weak var locationURLLabel: UILabel!
    @IBOutlet weak var locationNameLabel: UILabel!
    @IBOutlet weak var locationImageView: UIImageView!
    @IBOutlet weak var reviewsTableView: UITableView!
    @IBOutlet weak var activeRulesTableView: UITableView!
//        {
//        didSet {
//            activeRulesTableView.estimatedRowHeight = 100
//            activeRulesTableView.rowHeight = UITableViewAutomaticDimension
//        }
//    }
    @IBOutlet weak var locationAdressLabel: UILabel!
    @IBOutlet weak var locationDistanceLabel: UILabel!
    @IBOutlet weak var ratingSlider: UISlider!
    @IBOutlet weak var addPhotobtn: UIButton!
    @IBOutlet weak var addCommentBtn: UIButton!
    @IBOutlet weak var ratingLabel: UILabel!
    //  let imagePicker: UIImagePickerController! = UIImagePickerController()
    @IBAction func SliderValueChanges(_ sender: UISlider) {
        //        var currentValue = Int(sender.value)
        //        self.ratingLabel.text = "\(currentValue)"
        print(self.ratingSlider.value)
        let value = round(self.ratingSlider.value)
        let s = NSString(format: "%.0f", value)
        //        ratinglabel?.text = String("Rating: \(s)")
        var ratingStr = "Rating: "
        ratingStr = "\(ratingStr)\(s)"
        print(ratingStr)
        ratingLabel.text = ratingStr
        self.sliderValue = s as String!
        //   Rated = true
        //        sendRatingsToServer()
    }
    @IBAction func SliderChanged(_ sender: UISlider) {
        //        var currentValue = Int(sender.value)
        //        self.ratingLabel.text = "\(currentValue)"
        let value = round(self.ratingSlider.value)
        let s = NSString(format: "%.0f", value)
        //        ratinglabel?.text = String("Rating: \(s)")
        var ratingStr = "Rating: "
        ratingStr = "\(ratingStr)\(s)"
        print(ratingStr)
        ratingLabel.text = ratingStr
        print(self.ratingSlider.value)
        Rated = true
        sendRatingsToServer()
    }
    
    // MARK: AddPhotoButtonPressed
    @IBAction func AddPhotoPressed(_ sender: Any) {
        if !Rated {
            self.present(Common.showAlertWithTitle("Kikspot", message: "Please Rate to Add Photo", cancelButton: "OK"), animated: true, completion: nil)
        }
        else {
            let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.alert)
            let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
            {
                UIAlertAction in
                self.openCamera()
            }
            let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
            {
                UIAlertAction in
                self.openGallary()
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
            {
                UIAlertAction in
            }
            // Add the actions
            imagePicker?.delegate = self
            alert.addAction(cameraAction)
            alert.addAction(gallaryAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
//            present(alert, animated: true, completion: nil)
            // Present the controller
//            if UIDevice.current.userInterfaceIdiom == .phone
//            {
//                self.present(alert, animated: true, completion: nil)
//            }
//            else
//            {
//                alert.modalPresentationStyle = UIModalPresentationStyle.popover
//                self.present(alert, animated: true, completion: nil)
////                controller.modalPresentationStyle = UIModalPresentationStyle.popover
////                controller.present(alert, animated: false, completion: nil)
////                self.present(controller, animated: true, completion: nil)
////                popover=UIPopoverPresentationController(contentViewController: alert)
////                popover!.present(from: addPhotobtn.frame, in: self.view, permittedArrowDirections: UIPopoverArrowDirection.any, animated: true)
////                popover = UIPopoverPresentationController(presentedViewController: alert, presenting: alert)
//
//            }
        }
    }
    
    
    //
    //
    //
    //        if Rated {
    //
    //            if (UIImagePickerController.isSourceTypeAvailable(.camera)) {
    //                if UIImagePickerController.availableCaptureModes(for: .rear) != nil {
    //                    imagePicker.allowsEditing = false
    //                    imagePicker.delegate = self
    //                    imagePicker.sourceType = .camera
    //                    imagePicker.cameraCaptureMode = .photo
    //                    present(imagePicker, animated: true, completion: {})
    //                } else {
    //                    // postAlert("Rear camera doesn't exist", message: "Application cannot access the camera.")
    //                    print("Application cannot access the camera.");
    //                }
    //            } else {
    //                //postAlert("Camera inaccessable", message: "Application cannot access the camera.")
    //                print("Camera inaccessable");
    //            }
    //
    //        }
    //        else {
    //
    //            self.present(Common.showAlertWithTitle("Kikspot", message:"Please Rate to Add Image", cancelButton: "OK"), animated: true, completion: nil)
    //
    //        }
    //
    //    }
    
    // Mark Get Location Details
    func fetchLocationDetail() {
        let userId =  UserDefaults.standard.value(forKey: UserId) as! String
        let parameters : [String : String]  = [
            "locationAPIId": self.locationId,
            "latitude": self.latitude,
            "longitude": self.longitute,
            "userId": userId
            ]
        print("parameters \(parameters)")
        if userId != ""
        {
            let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                //    let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                //    loadingNotification.mode = MBProgressHUDMode.indeterminate
                //    loadingNotification.label.text = "Loading"
                request(baseURL + getLocationDetails, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                        //print("Progress: \(progress.fractionCompleted)")
                    }
                    .validate { request, response, data in
                        // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                        return .success
                    }
                    .responseJSON { response in
                        if let JSON:[String:AnyObject]  = response.result.value as! [String : AnyObject]?  {
                            print("JSON: \(JSON)")
                            if let locationCommt = JSON ["locationComments"] as? NSArray{
                                let locationDataHolder : NSMutableArray = []
                                if locationCommt.count > 0 {
                                    for i in 0 ..< locationCommt.count {
                                        let tempDic = locationCommt .object(at: i) as! [String : AnyObject]
                                        let locationC = locationComments()
                                        locationC.comment = (tempDic["comment"] as? String)!
                                        locationC.firstName = (tempDic["firstName"] as? String)!
                                        locationC.ratedUserName = (tempDic["username"] as? String)!
                                        locationC.ratedImage = (tempDic["image"] as? String)!
                                        
                                        let givenRating = tempDic["rating"] as? NSNumber
                                        locationC.givenRating = "\(givenRating!)"
                                        
                                        locationC.ratingDate = (tempDic["ratingDate"] as? String)!
                                        
                                        locationDataHolder.add(locationC)
                                    }
                                    self.locationCommentsArray = locationDataHolder
                                    
                                    self.reviewsTableView .reloadData()
                                    
                                    print("printing location commnets \( self.locationCommentsArray)")
                                    
                                }
                                else {
                                    self.locationCommentsArray .removeAllObjects()
                                }
                            }
                            if let rewardDetails = JSON ["rulesDetails"] as? NSArray{
                                let activeRulesDataHolder : NSMutableArray = []
                                print(rewardDetails)
                                    for i in 0 ..< rewardDetails.count {
                                        let tempDic = rewardDetails .object(at: i) as! [String : AnyObject]
                                let rewarddetailval = RewardDetailValues()
                                        let redeemcreds = tempDic["Creds"] as? NSNumber
                                        rewarddetailval.redeemCreds = "\(redeemcreds!)"
                                        print(rewarddetailval.redeemCreds)
                                        rewarddetailval.rewardValue = (tempDic["Game Rule"] as? String)!
                                         print(rewarddetailval.rewardValue)
                                        activeRulesDataHolder.add(rewarddetailval)
//                                        self.rewardLabelValue.text = "\(rewarddetailval.rewardValue)"+":"+"\(rewarddetailval.redeemCreds)"
                                    }
                                self.ActiveRuleDetailsArray = activeRulesDataHolder
                                self.activeRulesTableView.reloadData()
                            }
                            if let recommendations = JSON ["recommendationLocationDetails"] as? NSDictionary{
                                print(recommendations)
                                
                                if recommendations.count > 0 {
                                    self.locationNameLabel?.text = recommendations["locationName"] as? String
                                    self.locationNameLabel?.adjustsFontSizeToFitWidth = true
                                    self.locationAdressLabel?.text = recommendations["address"] as? String
                                    self.locationAdressLabel?.adjustsFontSizeToFitWidth = true
                                    
                                    self.locationDistanceLabel?.text = recommendations["distance"] as? String
                                    self.locationDistanceLabel?.adjustsFontSizeToFitWidth = true
                                    
                                    self.locationURLLabel?.text = recommendations["url"] as? String
                                    self.locationURLLabel?.adjustsFontSizeToFitWidth = true
                                    
                                    self.rateThisVenueLAbel?.adjustsFontSizeToFitWidth = true
                                    
                                    if let name = recommendations["locationName"] as? String {
                                        
                                        self.rateThisVenueLAbel?.text = "Rate \(name) Now!"
                                    }
                                    
                                    var iconURL = recommendations["iconUrl"] as? String
                                    if (iconURL == nil) {
                                        iconURL = ""
                                    }
                                    self.locationImageView?.image = UIImage(named: "defaultIcon")
                                    self.locationImageView?.downloadImageFrom(link: iconURL! , contentMode: UIViewContentMode.scaleAspectFit)
                                    
                                }
                            }
                            else {
                                if let error = JSON["ERROR"] as? String {
                                    print(error)
                                    // let error = stats as! String
                                    self.navigationController?.popViewController(animated: true)
                                    self.present(Common.showAlertWithTitle("Kikspot", message: "Unable to fetch location Website URL!", cancelButton: "OK"), animated: true, completion: nil)
                                }
                                MBProgressHUD.hide(for: self.view, animated: true);
                            }
                            MBProgressHUD.hide(for: self.view, animated: true);
                            
                        }
                        else {
                            MBProgressHUD.hide(for: self.view, animated: true);
                            print(response.result.error!)
                            self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                        }
                }
            }else {
                MBProgressHUD.hide(for: self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
        }
        //  getLocationReviews()
    }
    
    // MARK: AddcommentButtonPressed
    @IBAction func AddCommentPressed(_ sender: Any) {
        
        
        
        
        if !Rated {
            
            self.present(Common.showAlertWithTitle("Kikspot", message: "Please Rate to Add Comment", cancelButton: "OK"), animated: true, completion: nil)
        }
        else {
            
            
            
            
            let userDefaults : UserDefaults = UserDefaults.standard
            var ratinglocId :String = ""
            if (userDefaults.object(forKey: UserlocationRatingID) != nil) {
                ratinglocId =  UserDefaults.standard.value(forKey: UserlocationRatingID) as! String
            }
            
            //  Rated = !Rated
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let addComments = storyboard.instantiateViewController(withIdentifier: "AddComment") as! AddCommentViewController
            addComments.PreviousSliderValue = sliderValue
            addComments.latitude = latitude
            addComments.longitute = longitute
            addComments.locationID = locationId
            addComments.userRatingID = ratinglocId
            
            self.navigationController?.pushViewController(addComments, animated: true)
            
            
        }
        
        
    }
    
    // Mark Slider Value
    //    @IBAction func slider_value_Changing(_ sender: Any) {
    //
    //
    //        print(self.ratingSlider.value)
    //        let value = round(self.ratingSlider.value)
    //        let s = NSString(format: "%.0f", value)
    ////        let delayInSeconds = 0.5
    ////        let delay = DispatchTime.now() + Double(Int64(delayInSeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
    ////        DispatchQueue.main.asyncAfter(deadline: delay) {
    ////            self.ratinglabel.text = String("Rating: \(s)")
    ////        }
    //        ratinglabel.text = String("Rating: \(s)")
    //        self.sliderValue = s as String!
    //        Rated = true
    //
    //        sendRatingsToServer()
    //
    //
    //    }
    
    
    @IBAction func ShareButtonPressed(_ sender: Any) {
        
        let userDefaults : UserDefaults = UserDefaults.standard
        var referral :String = ""
        if (userDefaults.value(forKey: userreferralCode) != nil) {
            referral =  UserDefaults.standard.value(forKey: userreferralCode) as! String
        }
        
        var url :String = ""
        if (userDefaults.value(forKey: urlToShare) != nil) {
            url =  UserDefaults.standard.value(forKey: urlToShare) as! String
        } else {
            url = "http://www.kikspot.com/"
        }
        let textToShare = "This spot might be the place to go... Check it out, courtesy of Kikspot, the app that makes it super simple to find it, hit it and rate it: \(self.locationName!).  Download Kikspot here: \(url).  Use my referral code: \(referral) while creating your account"
        
        if  let myWebsite = NSURL(string:"\(baseURL)/recommendations/locationdetails.do?locationAPIId=\(self.locationId!)")
        {
            print(myWebsite)
            let objectsToShare = [textToShare, myWebsite] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            self.present(activityVC, animated: true, completion: nil)
            activityVC.completionWithItemsHandler = { (activity: UIActivityType?, completed: Bool, returnedItems: [Any]?, error: Error?) in
                //  activityVC.completionWithItemsHandler = {(activityType, completed:Bool, returnedItems:[AnyObject]?, error: NSError?) in
                
                // Return if cancelled
                if (!completed) {
                    print(" cancelled***********")
                    return
                } else {
                    let userDefaults : UserDefaults = UserDefaults.standard
                    var userId :String = ""
                    if (userDefaults.object(forKey: UserId) != nil) {
                        userId =  UserDefaults.standard.value(forKey: UserId) as! String
                    }
                    // let userId =  NSUserDefaults.standardUserDefaults().valueForKey(UserId) as! String
                    
                    let parameters : [String : String]  = [
                        "userId": userId,
                        "type": "Share Recommendation List"
//                        Share Comments
                    ]
                    if userId != ""
                    {
                        let reachability: Reachability = Reachability.forInternetConnection()
                        if  reachability.currentReachabilityStatus().rawValue != 0  {
                            request(baseURL + sendShareStatus, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                                .downloadProgress(queue: DispatchQueue.utility) { progress in
                                    //print("Progress: \(progress.fractionCompleted)")
                                }
                                .validate { request, response, data in
                                    // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                                    return .success
                                }
                                .responseJSON { response in
                                    //                            request(.POST, baseURL + sendShareStatus , parameters: parameters, encoding :.json)
                                    //                                .responseJSON { response in
                                    // result of response serialization
                                    
                                    if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                                        print("JSON: \(JSON)")
                                        if let userGameCred = JSON ["userGameCreds"] as? NSDictionary{
                                            let creds = userGameCred["earnedCreds"] as? NSNumber
                                            
                                            self.present(Common.showAlertWithTitle("Kikspot", message: "Congrats! You just earned \(creds!) cred! Go to the Notifications Screen for details", cancelButton: "OK"), animated: true, completion: nil)
                                        }
                                        
                                    }
                                    else {
                                        
                                        print(response.result.error!)
                                        
                                    }
                            }
                        }else {
                            //   MBProgressHUD.hideForView(self.view, animated: true);
                            self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
                        }
                        
                    }
                    
                }
                //activity complete
                //some code here
                
                
            }
            
        }
        
    }
    //    @IBAction func SliderValueChanged(_ sender: Any) {
    //
    //        //        let value  = round(slidervalue.value)
    //        //    //    ratinglabel.text = String("Rating: \(value)")
    //        let value = round(self.ratingSlider.value)
    //        let s = NSString(format: "%.0f", value)
    ////
    ////        let delayInSeconds = 0.5
    ////        let delay = DispatchTime.now() + Double(Int64(delayInSeconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
    ////        DispatchQueue.main.asyncAfter(deadline: delay) {
    //            self.ratinglabel.text = String("Rating: \(s)")
    ////        }
    //
    //        print(self.ratingSlider.value)
    //
    //        Rated = true
    //
    //
    //    }
    @IBOutlet var ratinglabel: UILabel!
    
    override func viewWillDisappear(_ animated: Bool) {
        
        didFinishCallingLocation = false
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let alreadyRated = UserDefaults.standard.bool(forKey:isRated)
        
        if alreadyRated {
            Rated = false
            
        }
        
        print(alreadyRated)
        print(!alreadyRated)
        print(Rated)
        self.rewardValueViewHeightConstraint.constant = 0
        self.rewardsImage.isHidden = true
//        self.rewardLabelValue.isHidden = true
        self.ActiveRewardLabel.isHidden = true
//        self.rewardlabelHeightConstraints.constant = 0
        self.navigationItem.title = "Venue"
//        locationName
        
        self.navigationItem.setHidesBackButton(true, animated: false)
        
        //        let homeButton : UIBarButtonItem = UIBarButtonItem(title: "< Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(RewardsViewController.goback(_:)))
        
        let homeButton : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "Back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(AddCommentViewController.goback(_:)))
        
        self.navigationItem.leftBarButtonItem = homeButton
        self.reviewsTableView.contentInset = UIEdgeInsetsMake(20.0, 0.0, 0.0, 0.0)
        self.reviewsTableView.delegate = self
        self.reviewsTableView.estimatedRowHeight = 264 ;
        self.reviewsTableView.rowHeight = UITableViewAutomaticDimension ;
        
        let tapURL = UITapGestureRecognizer(target: self, action: #selector(VenueDetailsViewController.URLLabelTapped))
        // tap.delegate = self
        self.locationURLLabel.addGestureRecognizer(tapURL)
        
        //  reviewsTableView.rowHeight = UITableViewAutomaticDimension
        //   reviewsTableView.estimatedRowHeight = 60
        
        //self.tableView.rowHeight = UITableViewAutomaticDimension
        
        //tableView.reloadData()
        print(self.activeRewardvalue);
        if (activeRewardvalue == "1") || (BackButtonComingFrom == "Rewards"){
            self.rewardValueViewHeightConstraint.constant = 85
            self.rewardsImage.isHidden = false
//            self.rewardLabelValue.isHidden = false
            self.ActiveRewardLabel.isHidden = false
//            self.rewardlabelHeightConstraints.constant = 53
//            let rewarddetailval = RewardDetailValues()
//            print(rewarddetailval.redeemCreds)
            
//            self.rewardLabelValue.text = "Active Reward Now with redeem Credits \(rewarddetailval.redeemCreds)\n \(rewarddetailval.rewardValue)"
        }
        
        if self.didFinishCallingLocation == false {
            self.locationManager.requestAlwaysAuthorization()
            
            // For use in foreground
            self.locationManager.requestWhenInUseAuthorization()
            
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
            }
        }
        
        
        
    }
    
    // Mark View Loads
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        //   self.reviewsTableView.estimatedRowHeight = 264.0
        //   self.reviewsTableView.rowHeight = UITableViewAutomaticDimension
        
        var minsliderImage = UIImage(named: "slider")!
        let myInsets : UIEdgeInsets = UIEdgeInsetsMake(13, 37, 13, 37)
        minsliderImage = minsliderImage.resizableImage(withCapInsets: myInsets)
        
        var maxsliderImage = UIImage(named: "slider")!
        let insets : UIEdgeInsets = UIEdgeInsetsMake(13, 37, 13, 37)
        maxsliderImage = maxsliderImage.resizableImage(withCapInsets: insets)
        
        self.ratingSlider.setMinimumTrackImage(minsliderImage, for: UIControlState())
        self.ratingSlider.setMaximumTrackImage(maxsliderImage, for: UIControlState())
        self.ratingSlider.setThumbImage(UIImage(named: "scroll_round.png"), for: UIControlState())
        
        //tableView.estimatedRowHeight = 264
        //tableView.rowHeight = UITableViewAutomaticDimension
        
        // fetchLocationDetail()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        
        if self.didFinishCallingLocation == false {
            self.locationManager.requestAlwaysAuthorization()
            
            // For use in foreground
            self.locationManager.requestWhenInUseAuthorization()
            
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
            }
        }
        
        getUserRating()
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        if self.didFinishCallingLocation == false {
            // self.callTheServiceForRecommendation("\(locValue.latitude)", longitude: "\(locValue.longitude)")
            
            
        }
        
        self.didFinishCallingLocation = true
        locationManager.stopUpdatingLocation()
        latitude = "\(locValue.latitude)"
        longitute = "\(locValue.longitude)"
        fetchLocationDetail()
        
    }
    
    func goback(_ sender: UIBarButtonItem) {
        // Perform your custom actions
        // ...
        // Go  to the mentioned ViewController
        
        if BackButtonComingFrom == "Rewards" {
            navigationController?.popViewController(animated: true)
        }else if BackButtonComingFrom == "Home" {
            let gamelevel = self.storyboard?.instantiateViewController(withIdentifier: "tabbarcontroller") as!  TabBarViewController
            self.navigationController?.pushViewController(gamelevel, animated: false)
        }
        
//        let gamelevel = self.storyboard?.instantiateViewController(withIdentifier: "tabbarcontroller") as!  TabBarViewController
//        
//        self.navigationController?.pushViewController(gamelevel, animated: false)
//        navigationController?.popViewController(animated: true)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        print("Got an image")
        if let pickedImage:UIImage = (info[UIImagePickerControllerOriginalImage]) as? UIImage {
            let selectorToCall = Selector("imageWasSavedSuccessfully:didFinishSavingWithError:context:")
            UIImageWriteToSavedPhotosAlbum(pickedImage, self, selectorToCall, nil)
        }
        
        imagePicker?.dismiss(animated: true, completion: {
            // Anything you want to happen when the user saves an image
        })
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        print("User canceled image")
        dismiss(animated: true, completion: {
            // Anything you want to happen when the user selects cancel
        })
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        var count:Int?
        if tableView == reviewsTableView {
        count = self.locationCommentsArray.count
        }else if tableView == activeRulesTableView{
            count = self.ActiveRuleDetailsArray.count
        }
        return count!
    }
    func decode(_ s: String) -> String? {
        let data = s.data(using: .utf8)!
        return String(data: data, encoding: .nonLossyASCII)
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if tableView == reviewsTableView {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! VenueDetailsTableViewCell
        let reviewcommnets = self.locationCommentsArray[(indexPath as NSIndexPath).row] as! locationComments
        cell.name.text = reviewcommnets.firstName
        cell.timeLabel.text = reviewcommnets.ratingDate
        //  cell.rating_comments.text = reviewcommnets.comment;
            let d = decode(reviewcommnets.comment)
        cell.commentslabel.text = d
//            reviewcommnets.comment
        let iconURL = reviewcommnets.ratedImage as? String
        print(".....\(iconURL!)")
        if (iconURL == nil || iconURL! == "") {
            cell.imageviewHeightConstraints.constant = 0
        }
        else
        {
            cell.imageviewHeightConstraints.constant = 130
            cell.ratingImageView.downloadImageFrom(link:iconURL!, contentMode: UIViewContentMode.scaleAspectFit)
        }
        cell.contentView.setNeedsLayout()
        cell.contentView.layoutIfNeeded()
        return cell
        }else if tableView == activeRulesTableView{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            let RuleValueLabel = cell.viewWithTag(101) as? UILabel
            let ActiveRules = self.ActiveRuleDetailsArray[indexPath.row] as! RewardDetailValues
            /*COMMENTED CODE : THIS CODE IS FOR READ MORE/LESS
             let readMoreTextView = cell.contentView.viewWithTag(100) as! ReadMoreTextView
            readMoreTextView.text = "\(ActiveRules.rewardValue)"
            
            let readMoreTextAttributes = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont.boldSystemFont(ofSize: 15)
            ]
            let readLessTextAttributes = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont.boldSystemFont(ofSize: 15)
            ]
            let readTextAttributes = [
                NSForegroundColorAttributeName: UIColor(red: 255/255, green: 249/255, blue: 156/255, alpha: 1),
                NSFontAttributeName: UIFont.systemFont(ofSize: 15)
            ]
            readMoreTextView.attributedReadMoreText = NSAttributedString(string: "...More", attributes: readMoreTextAttributes as [String : Any])
            readMoreTextView.attributedReadLessText = NSAttributedString(string: "...Less", attributes: readLessTextAttributes)
            readMoreTextView.attributedText = NSAttributedString(string: "\(ActiveRules.rewardValue)", attributes: readTextAttributes as [String : Any])
            
            
            readMoreTextView.onSizeChange = { [unowned tableView] _ in
                tableView.reloadData()
            }
            readMoreTextView.setNeedsUpdateTrim()
            readMoreTextView.layoutIfNeeded()*/
            
            if self.ActiveRuleDetailsArray.count == 1 {
                self.rewardValueViewHeightConstraint.constant = 65
                self.rewardsImage.isHidden = false
                self.ActiveRewardLabel.isHidden = false
            }else if self.ActiveRuleDetailsArray.count >= 2 {
                self.rewardValueViewHeightConstraint.constant = 85
                self.rewardsImage.isHidden = false
                self.ActiveRewardLabel.isHidden = false
            }

            RuleValueLabel!.text = "\(ActiveRules.rewardValue)"
//            +":"+"\(ActiveRules.redeemCreds)"

            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == activeRulesTableView{
            let rulesView = self.storyboard?.instantiateViewController(withIdentifier: "RulesViewController") as! RulesViewController
            rulesView.locationID = self.locationId
            rulesView.callServiceFore = "oneLocation"
            RewardsComingFromLocation = "yes"
            self.navigationController?.pushViewController(rulesView, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var dimension:CGFloat?
        if tableView == reviewsTableView {
            dimension = UITableViewAutomaticDimension;
        }else if tableView == activeRulesTableView{
            dimension = UITableViewAutomaticDimension;
        }
        return dimension!
    }
    
    //func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //    return UITableViewAutomaticDimension
    // }
    
    func getLocationReviews() {
        
        
        let userId =  UserDefaults.standard.value(forKey: UserId) as! String
        
        let parameters : [String : String]  = [
            "userId" : userId ,
            "locationAPIId": self.locationId,
            ]
        
        print("parameters \(parameters)")
        
        
        if userId != ""
        {
            let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading"
                request(baseURL + getLocationReviewsComments, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                        //print("Progress: \(progress.fractionCompleted)")
                    }
                    .validate { request, response, data in
                        // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                        return .success
                    }
                    .responseJSON { response in
                        if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                            
                            print("JSON: \(JSON)")
                            
                            
                            //                            if let recommendations = JSON ["recommendationLocationDetails"] as? NSDictionary{
                            //
                            //                                if recommendations.count > 0 {
                            //                                    self.locationNameLabel?.text = recommendations["locationName"] as? String
                            //                                    self.locationNameLabel?.adjustsFontSizeToFitWidth = true
                            //                                    self.locationAdressLabel?.text = recommendations["address"] as? String
                            //                                    self.locationAdressLabel?.adjustsFontSizeToFitWidth = true
                            //                                    var iconURL = recommendations["image"] as? String
                            //                                    if (iconURL == nil) {
                            //                                        iconURL = ""
                            //                                    }
                            //                                    self.locationImageView?.image = UIImage(named: "icon_167@2x.png")
                            //                                    self.locationImageView?.downloadImageFrom(link: iconURL! , contentMode: UIViewContentMode.scaleAspectFit)
                            //
                            //                                }
                            //                            }
                            //                            else {
                            //                                if let error = JSON["ERROR"] as? String {
                            //                                    print(error)
                            //                                    // let error = stats as! String
                            //                                    self.navigationController?.popViewController(animated: true)
                            //                                    self.present(Common.showAlertWithTitle("Kikspot", message: "Unable to fetch location Website URL!", cancelButton: "OK"), animated: true, completion: nil)
                            //                                }
                            //                                MBProgressHUD.hide(for: self.view, animated: true);
                            //                            }
                            MBProgressHUD.hide(for: self.view, animated: true);
                            
                        }
                        else {
                            MBProgressHUD.hide(for: self.view, animated: true);
                            print(response.result.error)
                            self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                        }
                }
            }else {
                MBProgressHUD.hide(for: self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
        }
        
    }
    
    func sendRatingsToServer() {
        
        let userDefaults : UserDefaults = UserDefaults.standard
        var userId :String = ""
        if (userDefaults.object(forKey: UserId) != nil) {
            userId =  UserDefaults.standard.value(forKey: UserId) as! String
        }
        // let userId =  NSUserDefaults.standardUserDefaults().valueForKey(UserId) as! String
        //        var userComment : String = ""
        //        if addComment_textView != nil {
        //            userComment = self.addComment_textView.text
        //        }
        if latitude == nil || longitute == nil {
            self.present(Common.showAlertWithTitle("Kikspot", message: "Oops! Unable to fetch your location. Please try again later", cancelButton: "OK"), animated: true, completion: nil)
        } else {
            let parameters : [String : String]  = [
                "userId": userId,
                "locationAPIId": locationId,
                "comment": "",
                "rating": self.sliderValue,
                "latitude" : self.latitude,
                "longitude" : self.longitute
            ]
            print(parameters)
            if userId != ""
            {
                let reachability: Reachability = Reachability.forInternetConnection()
                if  reachability.currentReachabilityStatus().rawValue != 0  {
                    let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                    loadingNotification.mode = MBProgressHUDMode.indeterminate
                    loadingNotification.label.text = "Loading"
                    request(baseURL + sendLocationRatingToServer, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                        .downloadProgress(queue: DispatchQueue.utility) { progress in
                            //print("Progress: \(progress.fractionCompleted)")
                        }
                        .validate { request, response, data in
                            // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                            return .success
                        }
                        .responseJSON { response in
                            
                            // result of response serialization
                            
                            if let JSON : [String:AnyObject] = response.result.value as? [String:AnyObject] {
                                print("JSON: \(JSON)")
                                if let error = JSON["ERROR"] as? String {
                                    
                                    self.Rated = false
                                    self.ratingSlider.value = 0
                                    
                                    //   let value = round(self.ratingSlider.value)
                                    //   let s = NSString(format: "%.0f", value)
                                    self.ratingLabel?.text = "Rating : 0"
                                    //  let ratingString = self.sliderValue as NSString
                                    
                                    self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                }
                                else {
                                    //  self.addComment_textView.text = ""
                                    //  userComment = ""
                                    //  self.navigationController?.popViewController(animated: true)
                                    
                                    //                                    if let ratingId = JSON["userLocationRatingId"] as? String {
                                    //
                                    //                                    self.userLocationRatingID = ratingId
                                    //
                                    //                                    }
                                    self.Rated = true
                                    
                                    if let ratingi = JSON["userLocationRatingId"] as? NSNumber {
                                        // let user = userId as! NSNumber
                                        UserDefaults.standard.set("\(ratingi)" ,forKey: UserlocationRatingID)
                                        //   self.userLocationRatingID = ratingi
                                        
                                        print("......\(ratingi)")
                                        
                                    }
                                    
                                    
                                    // print("......\(ratingId)")
                                    print("......\(self.userLocationRatingID)")
                                    self.present(Common.showAlertWithTitle("Kikspot", message: "Rating Submitted!\n Choose to add a comment or picture and really let Spotters know how you feel!", cancelButton: "OK"), animated: true, completion: nil)
                                }
                                MBProgressHUD.hide(for: self.view, animated: true);
                            }
                            else {
                                MBProgressHUD.hide(for: self.view, animated: true);
                                print(response.result.error)
                                self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                            }
                    }
                }else {
                    MBProgressHUD.hide(for: self.view, animated: true);
                    self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
                }
                
            }
            // let req = request(.GET, baseURL + sendLocationRatingToServer, parameters: parameters, encoding :.JSON)
            // debugPrint(req)
        }
    }
    
    func getUserRating() {
        
        let userDefaults : UserDefaults = UserDefaults.standard
        var userId :String = ""
        if (userDefaults.object(forKey: UserId) != nil) {
            userId =  UserDefaults.standard.value(forKey: UserId) as! String
        }
        // let userId =  NSUserDefaults.standardUserDefaults().valueForKey(UserId) as! String
        
        let parameters : [String : String]  = [
            "userId": userId,
            "locationAPIId": locationId
        ]
        if userId != ""
        {
            let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading"
                request(baseURL + getUserRatingForALocation, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                        //print("Progress: \(progress.fractionCompleted)")
                    }
                    .validate { request, response, data in
                        // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                        return .success
                    }
                    .responseJSON { response in
                        
                        // result of response serialization
                        
                        if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                            print("JSON: \(JSON)")
                            
                            if let error = JSON["ERROR"] as? String {
                                
                                
                                
                                //self.presentViewController(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                            }
                            else {
                                
                                if let ratings = JSON ["userRecentLocationRating"] as? NSDictionary{
                                    for i in 0 ..< ratings.count {
                                        //let tempDic = ratings.objectAtIndex(i)
                                        
                                        //                                        if let comment = ratings["comment"] as? String {
                                        //                                            self.addComment_textView.text  = comment
                                        //                                        }
                                        
                                        let rating = ratings["rating"] as? NSNumber
                                        self.sliderValue = "\(rating!)"
                                        self.ratingLabel?.text = "Rating: \(rating!)"
                                        //   self.putImageforSliderValue(self.sliderValue)
                                        
                                        
                                        
                                        let ratingString = self.sliderValue as NSString
                                        self.ratingSlider.value = (ratingString.floatValue)
                                        //
                                        //                                        if let lastRated = ratings["recentRatingDate"] as? String {
                                        //                                            self.lastRated_label.text  = lastRated
                                        //                                            self.lastRated_label.adjustsFontSizeToFitWidth = true
                                        //
                                        //                                        }
                                        
                                    }
                                }
                            }
                            MBProgressHUD.hide(for: self.view, animated: true);
                        }
                        else {
                            MBProgressHUD.hide(for: self.view, animated: true);
                            print(response.result.error)
                            
                            
                            //                            self.presentViewController(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                        }
                }
            }else {
                MBProgressHUD.hide(for: self.view, animated: true);
                
                
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
            
        }
        //        let req = request(.GET, baseURL + getUserRatingForALocation, parameters: parameters, encoding :.JSON)
        //        debugPrint(req)
        
        
    }
    
    
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker!.sourceType = UIImagePickerControllerSourceType.camera
            self .present(imagePicker!, animated: true, completion: nil)
        }
        else
        {
            let alertWarning = UIAlertController(title: "Warning", message:"You don't have camera" , preferredStyle: UIAlertControllerStyle.alert)
            alertWarning.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            }))
            self.present(alertWarning, animated: true, completion: nil)
//            (title:"Warning", message: "You don't have camera", delegate:nil, cancelButtonTitle:"OK", otherButtonTitles:"")
//            alertWarning.show()
        }
    }
    
    func openGallary()
    {
        imagePicker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(imagePicker!, animated: true, completion: nil)
    }
    
    //PickerView Delegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        //var image: UIImage!
        
        // fetch the selected image
        if picker.allowsEditing {
            locationImage = info[UIImagePickerControllerEditedImage] as! UIImage
//            self.sendLocationImagesToServer()
        } else {
            locationImage = info[UIImagePickerControllerOriginalImage] as! UIImage
//            self.sendLocationImagesToServer()
        }
        
        // self.imageView.image = image
        // Do something about image by yourself
        
        // dissmiss the image picker controller window
        self.dismiss(animated: true, completion: nil)
        
        
        let refreshAlert = UIAlertController(title: "Kikspot", message: "OK to let other Spotters check this pic out, right?", preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "YES", style: .default, handler: { (action: UIAlertAction!) in
            print("Handle Ok logic here")
            self.sendLocationImagesToServer()
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "NO", style: .default, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
        
        present(refreshAlert, animated: true, completion: nil)
        
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.imagePicker = UIImagePickerController()
        dismiss(animated: true, completion: nil)
    }
    
    func sendLocationImagesToServer() {
        let userId =  UserDefaults.standard.value(forKey: UserId) as! String
        let reachability: Reachability = Reachability.forInternetConnection()
        if  reachability.currentReachabilityStatus().rawValue != 0  {
            
            
            
            let userDefaults : UserDefaults = UserDefaults.standard
            var ratinglocId :String = ""
            if (userDefaults.object(forKey: UserlocationRatingID) != nil) {
                ratinglocId =  UserDefaults.standard.value(forKey: UserlocationRatingID) as! String
            }
            
            
            let imageData = UIImageJPEGRepresentation(locationImage, 0.7)
            
            let parameters: [String : String] = [
                "locationAPIId": self.locationId,
                "userId": userId,
                "latitude" : self.latitude,
                "longitude" : self.longitute,
                "userLocationRatingId" : ratinglocId
            ]
            
            print(".......\(parameters)")
            
            
            var theJSONText : NSString  = ""
            
            do {
                let theJSONData = try JSONSerialization.data(
                    withJSONObject: parameters ,
                    options: JSONSerialization.WritingOptions(rawValue: 0))
                theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)!
                
                
            } catch let error as NSError {
                print(error)
            }
            
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = MBProgressHUDMode.indeterminate
            loadingNotification.label.text = "Loading"
            
            //  let urlRequest = urlRequestWith(baseURL + sendLocImageToServer, parameters: theJSONText  , imageData: imageData!)
            upload(
                multipartFormData: { multipartFormData in
                    // multipartFormData.append(data: imageData, name: "images", fileName: "images.png", mimeType: "image/jpeg")
                    multipartFormData.append(imageData!, withName: "images", fileName: "images.png", mimeType: "image/jpeg")
                    multipartFormData.append(theJSONText.data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)!, withName: "json")
            },
                to: baseURL + sendLocImageToServer,
                encodingCompletion: { encodingResult in
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            debugPrint(response)
                            if let JSON:[String:AnyObject] = response.result.value as! [String : AnyObject]?  {
                                print("JSON: \(JSON)")
                                if let status = JSON["STATUS"] as? String {
                                    
                                    if (status == "User Recommendation Location Images Added Successfully") {
                                        
//                                        self.present(Common.showAlertWithTitle("Kikspot", message: status, cancelButton: "OK"), animated: true, completion: nil)
                                        
                                        let refreshAlert = UIAlertController(title: "Kikspot", message: status, preferredStyle: UIAlertControllerStyle.alert)
                                        
                                        refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                                            print("Handle Ok logic here")
                                            self.fetchLocationDetail()
                                        }))
                                        
                                         self.present(refreshAlert, animated: true, completion: nil)
                                    }
                                }
                                else {
                                    if let error = JSON["ERROR"] as? String {
                                        // let error = stats as! String
                                        self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                    }
                                }
                                MBProgressHUD.hide(for: self.view, animated: true)
                            }
                            else {
                                MBProgressHUD.hide(for: self.view, animated: true);
                                print(response.result.error)
                                self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                            }
                            MBProgressHUD.hide(for: self.view, animated: true);
                        }
                    case .failure(let encodingError):
                        print(encodingError)
                    }
            }
            )
            
        }
        
    }
    
    func URLLabelTapped() {
        
        var addressToLinkTo = ""
        addressToLinkTo = self.locationURLLabel.text!
        
        if addressToLinkTo == ""  {
            
//            addressToLinkTo = "http://www.kikspot.com"
        }else{
        // }
        addressToLinkTo = addressToLinkTo.addingPercentEscapes(using: String.Encoding.utf8)!
        
        let url = URL(string: addressToLinkTo)
        UIApplication.shared.openURL(url!)
        }
    }
    
    
    
    
}
extension UIImageView {
    func downloadImageFrom(link:String, contentMode: UIViewContentMode) {
        print("********** link is \(link)")
        if (link != "") {
            URLSession.shared.dataTask( with: URL(string:link)!, completionHandler: {
                (data, response, error) -> Void in
                DispatchQueue.main.async {
                    self.contentMode =  contentMode
                    if let data = data { self.image = UIImage(data: data) }
                }
            }).resume()
        }
    }
}


