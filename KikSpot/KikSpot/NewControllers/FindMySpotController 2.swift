//
//  FindMySpotController.swift
//  KikSpot
//
//  Created by knsmac003 on 3/20/18.
//  Copyright © 2018 Shruti Mittal. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import GoogleMaps
import MapKit
import MBProgressHUD
var RewardsComingFromLocation:String!

@available(iOS 11.0, *)
class FindMySpotController: UIViewController,UITableViewDataSource,UITableViewDelegate,CLLocationManagerDelegate,UIScrollViewDelegate {
    
    // variables for location
    var didFinishCallingLocation = false
    let locationManager = CLLocationManager()
    var longitute: String!
    var latitude: String!
    var selectedSort: String = "close"
    var alreadySelected : String = "close"
    var requestFilter : Int = 1
    var newFetchBool = 0
    var refreshControl:UIRefreshControl!
    
    @IBOutlet weak var starWidthConstraint: NSLayoutConstraint!
    var pointerToken: String = ""
    
    var recomendationArray : NSMutableArray = []
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    @IBOutlet weak var tableView: UITableView!
    
    
    @IBAction func mapbuttonPressed(_ sender: Any) {
        
        let mapItems : NSMutableArray = []
        let mapString : NSMutableString = ""
        for i in 0  ..< self.recomendationArray.count {
            let recommend = recomendationArray[i] as! Recommendation
            mapString.append("\(recommend.latitude),\(recommend.longitude)|")
            
            let place = MKPlacemark(coordinate: CLLocationCoordinate2DMake((recommend.latitude as NSString).doubleValue, (recommend.longitude as NSString).doubleValue), addressDictionary: nil)
            let mapItem = MKMapItem(placemark: place)
            mapItem.name = recommend.locationName
            mapItems .add(mapItem)
        }
        
        let array = mapItems as NSArray
        MKMapItem.openMaps(with: array as! [MKMapItem], launchOptions: nil)
        
        
        
    }
    
    @IBAction func rewardButtonPressed(_ sender: Any) {
        
        /*let rulesView = self.storyboard?.instantiateViewController(withIdentifier: "Rewards") as! RewardsViewController
         
         self.navigationController?.pushViewController(rulesView, animated: true)*/
        
        //        let locationApID_array : NSMutableArray = []
        //        for i in 0  ..< self.recomendationArray.count {
        //            let recommend = recomendationArray[i] as! Recommendation
        //            let locationId = recommend.locationAPIId
        //            locationApID_array .add(locationId)
        //        }
        let rulesView = self.storyboard?.instantiateViewController(withIdentifier: "RulesViewController") as! RulesViewController
        //        rulesView.locationId_array = locationApID_array
        rulesView.callServiceFore = "allLocation"
        self.navigationController?.pushViewController(rulesView, animated: true)
        
        
    }
    
    
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        UserDefaults.standard.set(false, forKey: isRated)
        
        self.navigationController?.isNavigationBarHidden = true
        
        segmentControl.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.white], for: UIControlState.selected)
        
        self.refreshControl = UIRefreshControl()
        
        self.refreshControl?.attributedTitle = NSAttributedString(string: "Pull to refresh", attributes: [NSForegroundColorAttributeName: UIColor.white])
        
        //  self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        
        self.refreshControl.addTarget(self, action: #selector(FindMySpotController.refreshTableView), for: UIControlEvents.valueChanged)
        
        self.refreshControl.tintColor = UIColor.white
        
        self.tableView.addSubview(refreshControl)
        
        
        
        
        
        // Do any additional setup after loading the view.
        
    }
    
    //    func TableRefresh(){
    //        requestFilter = requestFilter+1
    //        if requestFilter <= 3 {
    //            self.refreshTableView()
    //        }
    //    }
    
    func refreshTableView()
        
    {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.startUpdatingLocation()
        requestFilter = 1
        //        locationManager.stopUpdatingLocation()
        
        let userId =  UserDefaults.standard.value(forKey: UserId) as! String
        
        let parameters : [String : Any]  = [
            
            "userId": userId,
            
            "latitude": self.latitude,
            
            "longitude": self.longitute,
            
            "pageToken" :"",
            //                self.pointerToken,
            
            "sortBy" : selectedSort,
            "requestFilter":requestFilter
            
        ]
        
        print(parameters)
        
        if userId != ""
            
        {
            
            let reachability: Reachability = Reachability.forInternetConnection()
            
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading"
                request(baseURL + refreshRecommendationLocation, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                    }
                    
                    .validate { request, response, data in
                        return .success
                    }
                    .responseJSON { response in
                        if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                            print("JSON: \(JSON)")
                            
                            if let recommendations = JSON ["recommendationLocations"] as? NSArray{
                                
                                self.pointerToken =  (JSON ["pageToken"] as? String)!
                                
                                let dataHolder : NSMutableArray = []
                                
                                for i in 0 ..< recommendations.count {
                                    
                                    let tempDic = recommendations .object(at: i) as! [String : AnyObject]
                                    
                                    let recommend = Recommendation()
                                    
                                    recommend.distance = (tempDic["distance"] as? String)!
                                    
                                    recommend.trending = (tempDic["trending"] as? String)!
                                    
                                    var iconURL = tempDic["iconUrl"] as? String
                                    
                                    if (iconURL == nil) {
                                        
                                        iconURL = ""
                                        
                                    }
                                    
                                    recommend.iconUrl = iconURL!
                                    
                                    let isNew = tempDic["isNew"] as? NSNumber
                                    
                                    recommend.isNew = "\(isNew!)"
                                    
                                    let hasActiveRule = tempDic["hasActiveRules"] as? NSNumber
                                    
                                    recommend.hasActiveRules = "\(hasActiveRule!)"
                                    let newlyAdded = tempDic["newlyAdded"] as? NSNumber
                                    recommend.newlyAdded = "\(newlyAdded!)"
                                    let rates = tempDic["rates"] as? NSNumber
                                    recommend.rates = "\(rates!)"
                                    //                                    let hasActiveRewards = tempDic["hasActiveRewards"] as? NSNumber
                                    //
                                    //                                    recommend.hasActiveRewards = "\(hasActiveRewards!)"
                                    
                                    let longitude = tempDic["longitude"] as? NSNumber
                                    
                                    recommend.longitude = "\(longitude!)"
                                    
                                    recommend.address = (tempDic["address"] as? String)!
                                    
                                    
                                    recommend.locationAPIId = (tempDic["locationAPIId"] as? String)!
                                    
                                    recommend.locationName = (tempDic["locationName"] as? String)!
                                    
                                    // recommend.latitude = (tempDic["latitude"] as? NSNumber!)!
                                    
                                    let latitude = tempDic["latitude"] as? NSNumber
                                    
                                    recommend.latitude = "\(latitude!)"
                                    
                                    let rating = tempDic["rating"] as? NSNumber
                                    
                                    recommend.rating = "\(rating!)"
                                    
                                    dataHolder .add(recommend)
                                    
                                }
                                
                                //                                if self.selectedSort == "close" {
                                
                                //                                    self.recomendationArray.addObjectsFromArray(dataHolder as AnyObject as! [NSMutableArray])
                                
                                //                                }
                                
                                //                                else {
                                
                                self.recomendationArray = dataHolder
                                
                                //  }
                                
                                print(self.recomendationArray.count)
                                
                                self.tableView .reloadData()
                                
                                MBProgressHUD.hide(for: self.view, animated: true);
                                
                            }
                                
                                
                                
                            else {
                                
                                if let error = JSON["ERROR"] as? String {
                                    
                                    // let error = stats as! String
                                    
                                    MBProgressHUD.hide(for: self.view, animated: true);
                                    
                                    self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                    
                                }
                                
                                // if (self.refreshControl != nil) {
                                
                                self.refreshControl.endRefreshing()
                                
                                
                                
                                //  self.refreshControl.removeFromSuperview()
                                
                                // self.refreshControl = nil
                                
                                //  }
                                
                                
                                
                            }
                            
                            
                            
                        }
                            
                        else {
                            
                            MBProgressHUD.hide(for: self.view, animated: true);
                            
                            print(response.result.error!)
                            
                            self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                            
                        }
                        self.refreshControl.endRefreshing()
                }
                
            }else {
                
                MBProgressHUD.hide(for: self.view, animated: true);
                
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
                
            }
            
            //MBProgressHUD.hideForView(self.view, animated: true);
            
        }
        
        // let req = request(.GET, baseURL + RecommendationURL, parameters: parameters, encoding :.JSON)
        
        // debugPrint(req)
        
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated) // No need for semicolon
        
        if self.didFinishCallingLocation == false {
            self.locationManager.requestAlwaysAuthorization()
            self.tableView.isHidden = true
            // For use in foreground
            self.locationManager.requestWhenInUseAuthorization()
            
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
            }
        }
        self.navigationController?.isNavigationBarHidden = true
        RewardsComingFromLocation = ""
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated) // No need for semicolon
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        if self.didFinishCallingLocation == false {
            //     let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            //    loadingNotification.mode = MBProgressHUDMode.indeterminate
            //    loadingNotification.label.text = "Loading"
            self.callTheServiceForRecommendation("\(locValue.latitude)", longitude: "\(locValue.longitude)")
        }
        
        self.didFinishCallingLocation = true
        locationManager.stopUpdatingLocation()
        latitude = "\(locValue.latitude)"
        longitute = "\(locValue.longitude)"
        UserDefaults.standard.set(latitude , forKey: currentLatitude)
        UserDefaults.standard.set(longitute , forKey: currentLongitude)
        
        
        
        // self.callTheServiceForRecommendation("\(locValue.latitude)", longitude: "\(locValue.longitude)")
    }
    
    internal func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        //        self.presentViewController(Common.showAlertWithTitle("Kikspot", message: "Kikspot needs access to your location. Please turn on Location Services in your device settings.", cancelButton: "OK"), animated: false, completion: nil)
        
        self.present(Common.location_setting_popUp(false), animated: false, completion: nil)
    }
    
    
    func getuserNotification() {
        
        let userId =  UserDefaults.standard.value(forKey: UserId) as! String
        
        let parameters : [String : String]  = [
            "userId": userId
        ]
        if userId != ""
        {
            let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                request(baseURL + getUnreadNotificationCount, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                    }
                    .validate { request, response, data in
                        return .success
                    }
                    .responseJSON { response in
                        if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                            
                            print("JSON: \(JSON)")
                            
                            if let supply = JSON["COUNT"] as? NSNumber{
                                let numberFormatter = NumberFormatter()
                                numberFormatter.numberStyle = NumberFormatter.Style.decimal
                                if let result = numberFormatter.string(from: supply) {
                                    if result != "0"{
                                        
                                        if let tabItems = self.tabBarController?.tabBar.items as NSArray?
                                        {
                                            // In this case we want to modify the badge number of the second tab:
                                            let tabItem = tabItems[2] as! UITabBarItem
                                            tabItem.badgeValue = result
                                        }
                                        
                                    }else {
                                        
                                        if let tabItems = self.tabBarController?.tabBar.items as NSArray?
                                        {
                                            // In this case we want to modify the badge number of the second tab:
                                            let tabItem = tabItems[2] as! UITabBarItem
                                            tabItem.badgeValue = nil
                                        }
                                        
                                    }
                                }
                            }
                            if let error = JSON ["ERROR"] as? String {
                                
                                print(" .....unread..........\(error)")
                                
                            }
                        }
                        else {
                            print(response.result.error!)
                            self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                        }
                }
            }else {
                // MBProgressHUD.hideForView(self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
        }
        
        
    }
    
    
    func callTheServiceForRecommendation( _ latitude:String, longitude:String) {
        
        locationManager.stopUpdatingLocation()
        let userId =  UserDefaults.standard.value(forKey: UserId) as! String
        //        self.recomendationArray .removeAllObjects();
        let dataHolder : NSMutableArray = []
        let parameters : [String : Any]  = [
            "userId": userId,
            "latitude": latitude,
            "longitude": longitude,
            "pageToken" : self.pointerToken,
            "sortBy" : selectedSort,
            "requestFilter":requestFilter
        ]
        print(parameters)
        if userId != ""
        {
            let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading"
                
                request(baseURL + RecommendationURL, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                        //print("Progress: \(progress.fractionCompleted)")
                    }
                    .validate { request, response, data in
                        // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                        return .success
                    }
                    .responseJSON { response in
                        if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                            
                            print("JSON: \(JSON)")
                            
                            if let recommendations = JSON ["recommendationLocations"] as? NSArray{
                                if recommendations.count > 0 {
                                    self.pointerToken =  (JSON ["pageToken"] as? String)!
                                    
                                    for i in 0 ..< recommendations.count {
                                        
                                        
                                        let tempDic = recommendations .object(at: i) as! [String : AnyObject]
                                        let recommend = Recommendation()
                                        recommend.distance = (tempDic["distance"] as? String)!
                                        recommend.trending = (tempDic["trending"] as? String)!
                                        recommend.address = (tempDic["address"] as? String)!
                                        
                                        
                                        var iconURL = tempDic["iconUrl"] as? String
                                        if (iconURL == nil) {
                                            iconURL = ""
                                        }
                                        recommend.iconUrl = iconURL!
                                        //   recommend.iconUrl = (tempDic["iconUrl"] as? String)!
                                        let isNew = tempDic["isNew"] as? NSNumber
                                        recommend.isNew = "\(isNew!)"
                                        let hasActiveRule = tempDic["hasActiveRules"] as? NSNumber
                                        recommend.hasActiveRules = "\(hasActiveRule!)"
                                        let newlyAdded = tempDic["newlyAdded"] as? NSNumber
                                        recommend.newlyAdded = "\(newlyAdded!)"
                                        let rates = tempDic["rates"] as? NSNumber
                                        recommend.rates = "\(rates!)"
                                        //                                    let hasActiveReward = tempDic["hasActiveRewards"] as? NSNumber
                                        //                                    recommend.hasActiveRewards = "\(hasActiveReward!)"
                                        
                                        let longitude = tempDic["longitude"] as? NSNumber
                                        recommend.longitude = "\(longitude!)"
                                        
                                        recommend.locationAPIId = (tempDic["locationAPIId"] as? String)!
                                        recommend.locationName = (tempDic["locationName"] as? String)!
                                        // recommend.latitude = (tempDic["latitude"] as? NSNumber!)!
                                        let latitude = tempDic["latitude"] as? NSNumber
                                        recommend.latitude = "\(latitude!)"
                                        let rating = tempDic["rating"] as? NSNumber
                                        recommend.rating = "\(rating!)"
                                        dataHolder .add(recommend)
                                    }
                                    self.recomendationArray = dataHolder
                                    
                                    //                                let nameArray = ["Ramu","JAGAN","Steve","Swift"]
                                    //                                let namesArrayData = NSKeyedArchiver.archivedData(withRootObject: nameArray)
                                    //                                UserDefaults.standard.set(namesArrayData, forKey: "arrayData")
                                    
                                    //                                let nameArray = self.recomendationArray
                                    //                                var nameArray : NSMutableArray = []
                                    //                                 nameArray = self.recomendationArray;
                                    //                                let namesArrayData = NSKeyedArchiver.archivedData(withRootObject: nameArray)
                                    //                                UserDefaults.standard.set(namesArrayData, forKey: recommendationsArray)
                                    
                                    
                                    //                                let placesData = NSKeyedArchiver.archivedData(withRootObject: self.recomendationArray)
                                    //                                UserDefaults.standard.set(placesData, forKey: recommendationsArray)
                                    
                                    
                                    recomendationsArray = self.recomendationArray;
                                    print("recomendationsArray is %@",recomendationsArray)
                                    
                                    print(self.recomendationArray.count)
                                    
                                    
                                    
                                    MBProgressHUD.hide(for: self.view, animated: true);
                                    self.tableView.isHidden = false
                                    self.tableView.reloadData()
                                }else{
                                    if let error = JSON["ERROR"] as? String {
                                        // let error = stats as! String
                                        MBProgressHUD.hide(for: self.view, animated: true);
                                        
                                        self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                    }
                                    self.recomendationArray = []
                                    self.tableView.isHidden = false
                                    self.tableView.reloadData()
                                }
                            }
                            else {
                                if let error = JSON["ERROR"] as? String {
                                    // let error = stats as! String
                                    MBProgressHUD.hide(for: self.view, animated: true);
                                    
//                                    self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                }
                                self.recomendationArray = []
                                self.tableView.isHidden = false
                                self.tableView.reloadData()
                            }
                            MBProgressHUD.hide(for: self.view, animated: true);
                        }
                        else {
                            MBProgressHUD.hide(for: self.view, animated: true);
                            print(response.result.error!)
                            self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                        }
                }
            }else {
                MBProgressHUD.hide(for: self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
            //MBProgressHUD.hideForView(self.view, animated: true);
        }
        // let req = request(.GET, baseURL + RecommendationURL, parameters: parameters, encoding :.JSON)
        // debugPrint(req)
        getuserNotification()
        
        // runNotificationCountBackground()
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return self.recomendationArray.count
        }else{
            return 1
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell:UITableViewCell!
        if (indexPath.section == 1) {
            cell = tableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath)
        }else{
            // Code for show your data set
            cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
            
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            let recommend = recomendationArray[(indexPath as NSIndexPath).row] as! Recommendation
            //            cell.
            if recomendationArray.count > (indexPath as NSIndexPath).row {
                
                
                let locationName = cell.viewWithTag(102) as? UILabel
                let locationImage = cell.viewWithTag(101) as? UIImageView
                let trendingImage = cell.viewWithTag(100) as? UIImageView
                let ratingImage = cell.viewWithTag(104) as? UIImageView
                let locationAddress = cell.viewWithTag(103) as? UILabel
                let locationdistance = cell.viewWithTag(105) as? UILabel
                let ActiveReward = cell.viewWithTag(106) as? UILabel
                let RewardIcon = cell.viewWithTag(107) as? UIImageView
                let StarIcon = cell.viewWithTag(108) as? UIImageView
                let newlyAdded = cell.viewWithTag(109) as? UILabel
                let rates = cell.viewWithTag(110) as? UILabel
                ActiveReward?.adjustsFontSizeToFitWidth = true
                newlyAdded?.adjustsFontSizeToFitWidth = true
                
                if recommend.hasActiveRules == "1" {
                    ActiveReward?.isHidden = false
                    RewardIcon?.isHidden = false
                } else{
                    ActiveReward?.isHidden = true
                    RewardIcon?.isHidden = true
                }
                
                if recommend.newlyAdded == "1"{
                    rates?.isHidden = true
                    //                    newlyAdded?.text = "NEWLY ADDED!"
                    newlyAdded?.isHidden = false
                    StarIcon?.isHidden = false
                } else {
                    if recommend.rates != "0"{
                        rates?.isHidden = false
                        newlyAdded?.isHidden = true
                        if recommend.rates == "1"{
                            rates?.text = "\(recommend.rates) Rate"
                        }else{
                            rates?.text = "\(recommend.rates) Rates"
                        }
                        StarIcon?.isHidden = true
                    }else{
                        newlyAdded?.isHidden = true
                        StarIcon?.isHidden = true
                    }
                }
                
                locationImage?.layer.masksToBounds = true
                //            locationImage?.layer.borderWidth = 1
                //            locationImage?.layer.borderColor = UIColor (red:15.0/255.0, green: 124.0/255.0, blue: 228.0/255.0, alpha: 1.0).cgColor
                
                
                locationName?.text = recommend.locationName
                locationName?.adjustsFontSizeToFitWidth = true
                locationAddress?.adjustsFontSizeToFitWidth = true
                locationdistance?.adjustsFontSizeToFitWidth = true
                locationAddress?.text = recommend.address
                locationdistance?.text = recommend.distance
                
                locationImage?.image = UIImage(named: "defaultIcon")  //set placeholder image first.
                print ("!!!!!!!!!!!! \(recommend.iconUrl)")
                locationImage?.downloadImageFrom(link: recommend.iconUrl, contentMode: UIViewContentMode.scaleAspectFit)
                
                if recommend.trending == "up" {
                    
                    trendingImage?.image = UIImage(named: "hot1")
                }
                else {
                    trendingImage?.image = UIImage(named: "not1")
                }
                
                if recommend.hasActiveRules == "0" {
                    cell.layer.borderColor = UIColor.white.cgColor
                }else {
                    cell.layer.borderColor = UIColor.red.cgColor
                }
                
                
                
                
                
                let rating = recommend.rating as NSString
                let ratingFloat = rating.floatValue
                let value = round(ratingFloat)
                let ratingString = NSString(format: "%.0f", value)
                
                if ratingString == "1" {
                    ratingImage!.image = UIImage(named: "1")!
                }else if ratingString == "2" {
                    ratingImage!.image = UIImage(named: "2")!
                }else if ratingString == "3" {
                    ratingImage!.image = UIImage(named: "3")!
                }else if ratingString == "4" {
                    ratingImage!.image = UIImage(named: "4")!
                }else if ratingString == "5" {
                    ratingImage!.image = UIImage(named: "5")!
                }else if ratingString == "6" {
                    ratingImage!.image = UIImage(named: "6")!
                }else if ratingString == "7" {
                    ratingImage!.image = UIImage(named: "7")!
                }else if ratingString == "8" {
                    ratingImage!.image = UIImage(named: "8")!
                }else if ratingString == "9" {
                    ratingImage!.image = UIImage(named: "9")!
                }else if ratingString == "10" {
                    ratingImage!.image = UIImage(named: "10")!
                }else {
                    ratingImage!.image = nil
                }
            }
        }
        //        if indexPath.row != self.tableView(tableView, numberOfRowsInSection: indexPath.section)-1 {
        //            // Code for show your data set
        //            cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        //
        //            cell.selectionStyle = UITableViewCellSelectionStyle.none
        //            let recommend = recomendationArray[(indexPath as NSIndexPath).row] as! Recommendation
        ////            cell.
        //            if recomendationArray.count > (indexPath as NSIndexPath).row {
        //
        //
        //                let locationName = cell.viewWithTag(102) as? UILabel
        //                let locationImage = cell.viewWithTag(101) as? UIImageView
        //                let trendingImage = cell.viewWithTag(100) as? UIImageView
        //                let ratingImage = cell.viewWithTag(104) as? UIImageView
        //                let locationAddress = cell.viewWithTag(103) as? UILabel
        //                let locationdistance = cell.viewWithTag(105) as? UILabel
        //                let ActiveReward = cell.viewWithTag(106) as? UILabel
        //                let RewardIcon = cell.viewWithTag(107) as? UIImageView
        //                let StarIcon = cell.viewWithTag(108) as? UIImageView
        //                let newlyAdded = cell.viewWithTag(109) as? UILabel
        //                let rates = cell.viewWithTag(110) as? UILabel
        //                ActiveReward?.adjustsFontSizeToFitWidth = true
        //                newlyAdded?.adjustsFontSizeToFitWidth = true
        //
        //                if recommend.hasActiveRules == "1" {
        //                    ActiveReward?.isHidden = false
        //                    RewardIcon?.isHidden = false
        //                } else{
        //                    ActiveReward?.isHidden = true
        //                    RewardIcon?.isHidden = true
        //                }
        //
        //                if recommend.newlyAdded == "1"{
        //                    rates?.isHidden = true
        ////                    newlyAdded?.text = "NEWLY ADDED!"
        //                    newlyAdded?.isHidden = false
        //                    StarIcon?.isHidden = false
        //                } else {
        //                    if recommend.rates != "0"{
        //                        rates?.isHidden = false
        //                        newlyAdded?.isHidden = true
        //                        if recommend.rates == "1"{
        //                        rates?.text = "\(recommend.rates) Rate"
        //                        }else{
        //                        rates?.text = "\(recommend.rates) Rates"
        //                        }
        //                        StarIcon?.isHidden = true
        //                    }else{
        //                    newlyAdded?.isHidden = true
        //                    StarIcon?.isHidden = true
        //                    }
        //                }
        //
        //                locationImage?.layer.masksToBounds = true
        //                //            locationImage?.layer.borderWidth = 1
        //                //            locationImage?.layer.borderColor = UIColor (red:15.0/255.0, green: 124.0/255.0, blue: 228.0/255.0, alpha: 1.0).cgColor
        //
        //
        //                locationName?.text = recommend.locationName
        //                locationName?.adjustsFontSizeToFitWidth = true
        //                locationAddress?.adjustsFontSizeToFitWidth = true
        //                locationdistance?.adjustsFontSizeToFitWidth = true
        //                locationAddress?.text = recommend.address
        //                locationdistance?.text = recommend.distance
        //
        //                locationImage?.image = UIImage(named: "defaultIcon")  //set placeholder image first.
        //                print ("!!!!!!!!!!!! \(recommend.iconUrl)")
        //                locationImage?.downloadImageFrom(link: recommend.iconUrl, contentMode: UIViewContentMode.scaleAspectFit)
        //
        //                if recommend.trending == "up" {
        //
        //                    trendingImage?.image = UIImage(named: "hot1")
        //                }
        //                else {
        //                    trendingImage?.image = UIImage(named: "not1")
        //                }
        //
        //                if recommend.hasActiveRules == "0" {
        //                    cell.layer.borderColor = UIColor.white.cgColor
        //                }else {
        //                    cell.layer.borderColor = UIColor.red.cgColor
        //                }
        //
        //
        //
        //
        //
        //                let rating = recommend.rating as NSString
        //                let ratingFloat = rating.floatValue
        //                let value = round(ratingFloat)
        //                let ratingString = NSString(format: "%.0f", value)
        //
        //                if ratingString == "1" {
        //                    ratingImage!.image = UIImage(named: "1")!
        //                }else if ratingString == "2" {
        //                    ratingImage!.image = UIImage(named: "2")!
        //                }else if ratingString == "3" {
        //                    ratingImage!.image = UIImage(named: "3")!
        //                }else if ratingString == "4" {
        //                    ratingImage!.image = UIImage(named: "4")!
        //                }else if ratingString == "5" {
        //                    ratingImage!.image = UIImage(named: "5")!
        //                }else if ratingString == "6" {
        //                    ratingImage!.image = UIImage(named: "6")!
        //                }else if ratingString == "7" {
        //                    ratingImage!.image = UIImage(named: "7")!
        //                }else if ratingString == "8" {
        //                    ratingImage!.image = UIImage(named: "8")!
        //                }else if ratingString == "9" {
        //                    ratingImage!.image = UIImage(named: "9")!
        //                }else if ratingString == "10" {
        //                    ratingImage!.image = UIImage(named: "10")!
        //                }else {
        //                    ratingImage!.image = nil
        //                }
        //            }
        //        }
        //        else {
        //            cell = tableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath)
        //
        //        }
        return cell
    }
    
    @IBAction func segmentSelected(_ sender: Any) {
        
        
        self.alreadySelected = self.selectedSort
        print("selected index: \((sender as AnyObject).selectedSegmentIndex)")
        
        
        switch segmentControl.selectedSegmentIndex
        {
        case 0:
            
            selectedSort = "close"
            callTheFunction()
            
            break
            
        case 1:
            
            selectedSort = "trending"
            callTheFunction()
            
            break
            
        default:
            
            break
            
        }
        
        
        
        
    }
    
    func callTheFunction() {
        
        self.pointerToken = ""
        if self.alreadySelected != self.selectedSort {
            if latitude != nil && longitute != nil {
                requestFilter = 1
                self.callTheServiceForRecommendation(latitude, longitude: longitute)}
            
        } else {
            //           self.presentViewController(Common.showAlertWithTitle("Kikspot", message: "Kikspot needs access to your location. Please turn on Location Services in your device settings.", cancelButton: "OK"), animated: false, completion: nil)
            self.present(Common.location_setting_popUp(false), animated: false, completion: nil)
        }
        
    }
    
    
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //
    //    }
    
    //    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    ////        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
    ////        footerView.backgroundColor = UIColor.clear
    ////
    ////        return footerView
    //
    //    }
    //
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //
    //        return
    //    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if (indexPath.section == 1) {
            let mainViewController = storyboard!.instantiateViewController(withIdentifier: "ContactUs") as! ContactUsViewController
            mainViewController.comingFrom = "Home"
            self.navigationController?.pushViewController(mainViewController, animated: true)
        }else{
            let recommend = recomendationArray[(indexPath as NSIndexPath).row] as! Recommendation
            let locationId = recommend.locationAPIId
            let locationname = recommend.locationName
            //            let ActiveRewardValue = recommend.hasActiveRewards
            let ActiveRuleValue = recommend.hasActiveRules
            
            let venueDetails = self.storyboard?.instantiateViewController(withIdentifier: "VenueDetails") as! VenueDetailsViewController
            venueDetails.locationId = locationId
            venueDetails.locationName = locationname
            venueDetails.activeRewardvalue = ActiveRuleValue
            BackButtonComingFrom = "Home"
            self.navigationController?.pushViewController(venueDetails, animated: true)
        }
        //            if indexPath.row != self.tableView(tableView, numberOfRowsInSection: indexPath.section)-1 {
        //            let recommend = recomendationArray[(indexPath as NSIndexPath).row] as! Recommendation
        //            let locationId = recommend.locationAPIId
        //            let locationname = recommend.locationName
        ////            let ActiveRewardValue = recommend.hasActiveRewards
        //            let ActiveRuleValue = recommend.hasActiveRules
        //
        //            let venueDetails = self.storyboard?.instantiateViewController(withIdentifier: "VenueDetails") as! VenueDetailsViewController
        //            venueDetails.locationId = locationId
        //            venueDetails.locationName = locationname
        //            venueDetails.activeRewardvalue = ActiveRuleValue
        //            BackButtonComingFrom = "Home"
        //            self.navigationController?.pushViewController(venueDetails, animated: true)
        //            }else{
        //                let mainViewController = storyboard!.instantiateViewController(withIdentifier: "ContactUs") as! ContactUsViewController
        //                self.navigationController?.pushViewController(mainViewController, animated: true)
        //            }
    }
    
    //MARK:- ScrollView Delegate
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        // Change 10.0 to adjust the distance from bottom
        if maximumOffset - currentOffset <= 10.0 {
            requestFilter = requestFilter+1
            //            if self.recomendationArray.count < 60 {
            if requestFilter <= 3 {
                self.callTheServiceForRecommendation("\(self.latitude!)", longitude: "\(self.longitute!)")
            }else{
                
            }
        }
    }
    func removeAPlaceFromRecommendation(_ forIndex: IndexPath)
    {
        
        locationManager.stopUpdatingLocation()
        let userId =  UserDefaults.standard.value(forKey: UserId) as! String
        let recommend = recomendationArray[forIndex.row] as! Recommendation
        let parameters : [String : String]  = [
            "userId": userId,
            "locationAPIId": recommend.locationAPIId,
            "sortBy": selectedSort
        ]
        if userId != ""
        {
            let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading"
                request(baseURL + removeRecommendationURL, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                        //print("Progress: \(progress.fractionCompleted)")
                    }
                    .validate { request, response, data in
                        // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                        return .success
                    }
                    .responseJSON { response in
                        if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                            if let status = JSON["STATUS"] as? String {
                                
                                if (status == "SUCCESS") {
                                    
                                    self.recomendationArray .removeObject(at: forIndex.row)
                                    //                                   self.places_tableView .reloadData()
                                    self.tableView.reloadData()
                                    
                                }
                                else {
                                    
                                    if let error = JSON["ERROR"] as? String {
                                        
                                        // let error = stats as! String
                                        
                                        self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                        
                                    }
                                    
                                }
                                
                            }
                            
                            MBProgressHUD.hide(for: self.view, animated: true);
                            
                        }
                            
                        else {
                            
                            MBProgressHUD.hide(for: self.view, animated: true);
                            
                            print(response.result.error as Any)
                            
                            self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                            
                        }
                        
                }
                
            }else {
                
                MBProgressHUD.hide(for: self.view, animated: true);
                
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
                
            }
            
        }
        
        //        let req = request(.GET, baseURL + removeRecommendationURL, parameters: parameters, encoding :.JSON)
        
        //        debugPrint(req)
        
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
        
    {
        
        let backView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: tableView.frame.size.height))
        backView.backgroundColor = UIColor(red: 115/255, green: 65/255, blue: 166/255, alpha: 1)
        let frame = tableView.rectForRow(at: indexPath)
        let myImage = UIImageView(frame: CGRect(x: 30, y: frame.size.height/2-15, width: 16, height: 16))
        myImage.image = UIImage(named: "rewards1")!
        backView.addSubview(myImage)
        
        let imgSize: CGSize = tableView.frame.size
        UIGraphicsBeginImageContextWithOptions(imgSize, false, UIScreen.main.scale)
        let context = UIGraphicsGetCurrentContext()
        backView.layer.render(in: context!)
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        //        let rules = UITableViewRowAction(style: .normal, title: "\u{1F4DD}\n Rewards") { action, index in
        
        let rules = UITableViewRowAction(style: .default, title: "\u{0000}\n Rewards") { action, index in
            let recommend = self.recomendationArray[indexPath.row] as! Recommendation
            let locationId = recommend.locationAPIId
            let rulesView = self.storyboard?.instantiateViewController(withIdentifier: "RulesViewController") as! RulesViewController
            rulesView.locationID = locationId
            rulesView.locationName = recommend.locationName
            rulesView.callServiceFore = "oneLocation"
            RewardsComingFromLocation = "yes"
            self.navigationController?.pushViewController(rulesView, animated: true)
        }
        //        rules.backgroundColor = UIColor(red: 115/255, green: 65/255, blue: 166/255, alpha: 1)
        rules.backgroundColor = UIColor(patternImage: newImage)
        
        let web = UITableViewRowAction(style: .normal, title: "\u{1F30F}\n Web") { action, index in
            //            let recommend = self.recomendationArray[(indexPath as NSIndexPath).section] as! Recommendation
            let recommend = self.recomendationArray[(indexPath as NSIndexPath).row] as! Recommendation
            let locationId = recommend.locationAPIId
            let locationname = recommend.locationName
            let venueDetails = self.storyboard?.instantiateViewController(withIdentifier: "FindMySpotDetailsViewController") as! FindMySpotDetailsViewController
            venueDetails.locationId = locationId
            venueDetails.locationName = locationname
            self.navigationController?.pushViewController(venueDetails, animated: true)
            
            //            let detailScreen = self.storyboard?.instantiateViewController(withIdentifier: "FindMySpotDetailsViewController") as! FindMySpotDetailsViewController
            //
            //            detailScreen.locationId = recommend.locationAPIId
            //
            //            self.navigationController?.pushViewController(detailScreen, animated: true)
            
        }
        web.backgroundColor = UIColor(red: 50/255, green: 159/255, blue: 188/255, alpha: 1)
        
        let map = UITableViewRowAction(style: .normal, title: "\u{1F4CD}\n Map") { action, index in
            print("share button tapped")
            let recommend = self.recomendationArray[(indexPath as NSIndexPath).row] as! Recommendation
            let lat  = (recommend.latitude as NSString).doubleValue
            let long = (recommend.longitude as NSString).doubleValue
            let place = MKPlacemark(coordinate: CLLocationCoordinate2DMake(lat, long), addressDictionary: nil)
            let mapItem = MKMapItem(placemark: place)
            mapItem.name = recommend.locationName
            mapItem.openInMaps(launchOptions: nil)
        }
        map.backgroundColor = UIColor(red: 15/255, green: 124/255, blue: 228/255, alpha: 1)
        
        let backView1 = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: tableView.frame.size.height))
        backView1.backgroundColor = UIColor(red: 137/255, green: 152/255, blue: 51/255, alpha: 1)
        let frame1 = tableView.rectForRow(at: indexPath)
        let myImage1 = UIImageView(frame: CGRect(x: 30, y: frame1.size.height/2-15, width: 16, height: 16))
        myImage1.image = UIImage(named: "trash")!
        backView1.addSubview(myImage1)
        let imgSize1: CGSize = tableView.frame.size
        UIGraphicsBeginImageContextWithOptions(imgSize1, false, UIScreen.main.scale)
        let context1 = UIGraphicsGetCurrentContext()
        backView1.layer.render(in: context1!)
        let newImage1: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        let trash = UITableViewRowAction(style: .normal, title: " \u{0000}\n Trash") { action, index in
            self.removeAPlaceFromRecommendation(((indexPath as NSIndexPath) as IndexPath))
        }
        //        trash.backgroundColor = UIColor(red: 137/255, green: 152/255, blue: 51/255, alpha: 1)
        trash.backgroundColor = UIColor(patternImage: newImage1)
        
        return [rules, web, trash,map]
        
        //        return [rules, web, trash]
        
    }
    //    @available(iOS 11.0, *)
    //    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
    //        let deleteAction = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
    //            // Call edit action
    //
    //            // Reset state
    //            success(true)
    //        })
    //        deleteAction.image = UIImage(named: "ic_delete")
    //        deleteAction.backgroundColor = .red
    //        return UISwipeActionsConfiguration(actions: [deleteAction])
    //    }
    
    
    
    //    func runNotificationCountBackground() {
    //        var image: UIImage? = nil
    //
    //        DispatchQueue.global().async {
    //            do {
    //               self.getuserNotification()
    //
    //
    //            } catch {
    //                print("Failed")
    //            }
    //            DispatchQueue.main.async(execute: {
    //
    //                if let tabItems = self.tabBarController?.tabBar.items as NSArray!
    //                {
    //                    // In this case we want to modify the badge number of the second tab:
    //                    let tabItem = tabItems[1] as! UITabBarItem
    //                    tabItem.badgeValue = "5"
    //                }
    //
    //
    //
    //            })
    //        }
    //    }
    
}
//extension UIImageView {
//    func downloadImageFrom(link:String, contentMode: UIViewContentMode) {
//        print("********** link is \(link)")
//        if (link != "") {
//            URLSession.shared.dataTask( with: URL(string:link)!, completionHandler: {
//                (data, response, error) -> Void in
//                DispatchQueue.main.async {
//                    self.contentMode =  contentMode
//                    if let data = data { self.image = UIImage(data: data) }
//                }
//            }).resume()
//        }
//    }
//}

