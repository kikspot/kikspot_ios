//
//  NotificationsViewController.swift
//  KikSpot
//
//  Created by knsmac003 on 3/20/18.
//  Copyright © 2018 Shruti Mittal. All rights reserved.
//

import UIKit
import MBProgressHUD


class NotificationsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var NotificationTableView: UITableView!

   
    var NotificationArray : NSMutableArray = []
    var selectedIndexPath : IndexPath!
    
    //    lazy   var searchBars:UISearchBar = UISearchBar(frame: CGRectMake(200,50, 200, 20))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       // self.navigationItem.title = "Notifications"
        
        self.NotificationTableView.delegate = self
        self.NotificationTableView.estimatedRowHeight = 90.0 ;
        self.NotificationTableView.rowHeight = UITableViewAutomaticDimension ;
        
        
    
      //  self.setNavigationBarItem()
             // self.navigationItem.title = "Notifications"
       // let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.black]
       // self.navigationController!.navigationBar.titleTextAttributes = titleDict as? [String : AnyObject]
        // Do any additional setup after loading the view.
        
        self.getuserNotification()
    }
    
    

    
    override func viewWillAppear(_ animated: Bool) {
        
         self.tabBarController?.navigationItem.title = "Notifications"
        

        
        self.NotificationTableView.estimatedRowHeight = 90.0 ;
        self.NotificationTableView.rowHeight = UITableViewAutomaticDimension ;
        self.tabBarController?.navigationItem.rightBarButtonItem = nil
        //self.setNavigationBarItem()
      //  self.navigationItem.title = "NOTIFICATIONS"
       // self.navigationController?.title = "Notification"

        //let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.black]
       // self.navigationController!.navigationBar.titleTextAttributes = titleDict as? [String : AnyObject]
        // Do any additional setup after loading the view.
        
        
       
        self.getuserNotification()
         self.getNotificationCount()
        super.viewWillAppear(animated)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getuserNotification() {
        
        let userId =  UserDefaults.standard.value(forKey: UserId) as! String
        
        let parameters : [String : String]  = [
            "userId": userId
        ]
        if userId != ""
        {
            let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading"
                request(baseURL + getNotificationsList, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                        //print("Progress: \(progress.fractionCompleted)")
                    }
                    .validate { request, response, data in
                        // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                        return .success
                    }
                    .responseJSON { response in
                        
                        
                        // result of response serialization
                        
                        if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                            
                            print("JSON: \(JSON)")
                            
                            
                            //                            if let status = JSON["STATUS"] as? String {
                            //                                if (status == "Success") {
                            
                            if let notification = JSON ["Notifications"] as? NSArray{
                                
                                if notification.count > 0 {
                                    let dataHolder : NSMutableArray = []
                                    for i in 0 ..< notification.count {
                                        let tempDic = notification .object(at: i) as! [String : AnyObject]
                                        
                                        let notifications = NotificationLists()
                                        
                                        let triggereID = tempDic["TriggeredId"] as? NSNumber
                                        if (triggereID != nil) {
                                            notifications.triggeredId = "\(triggereID!)"
                                        }
                                        notifications.notification = (tempDic["Notification:"] as? String)!
                                        notifications.notificationType = (tempDic["NotificationType"] as? String)!
                                        notifications.date = (tempDic["Date:"] as? String)!
                                        let notifyId = tempDic["NotificationId"] as? NSNumber
                                        notifications.notificationId = "\(notifyId!)"
                                        dataHolder.add(notifications)
                                    }
                                    self.NotificationArray = dataHolder
                                    self.NotificationTableView.reloadData()
                                }
                            }
                            
                            
                            MBProgressHUD.hide(for: self.view, animated: true);
                        }
                        else {
                            MBProgressHUD.hide(for: self.view, animated: true);
                          //  print(response.result.error)
                            self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                        }
                        MBProgressHUD.hide(for: self.view, animated: true);
                }
            }else {
                MBProgressHUD.hide(for: self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
            //            let req = request(.GET, baseURL + ListNotifications, parameters: parameters, encoding :.JSON)
            //            debugPrint(req)
        }
        
        
    }
    
    
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
//        return NotificationArray.count
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
          print("count: \(self.NotificationArray.count)")
     
        return self.NotificationArray.count
    }
    
    /*func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }*/
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        //  let cellidentifier = "cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as UITableViewCell
        let notify = NotificationArray[(indexPath as NSIndexPath).row] as! NotificationLists
        
        
        
        let notification_label = cell.viewWithTag(101) as? UILabel
        let date_label = cell.viewWithTag(102) as? UILabel
        notification_label!.numberOfLines = 0
        notification_label!.lineBreakMode = NSLineBreakMode.byWordWrapping
        let font =  UIFont.systemFont(ofSize: 11)
        notification_label!.font = font
        notification_label!.text = notify.notification
        
        notification_label!.sizeToFit()
        notification_label?.text = notify.notification
        date_label?.text = notify.date
        date_label?.adjustsFontSizeToFitWidth = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete {
            //            numbers.removeAtIndex(indexPath.row)
            //            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
            self.deleteANotification(indexPath)
        }
    }
    
    func deleteANotification (_ indexPath: IndexPath) {
        
        let userId =  UserDefaults.standard.value(forKey: UserId) as! String
        let notify = NotificationArray[(indexPath as NSIndexPath).row] as! NotificationLists
        
        let parameters : [String : String]  = [
            "userId": userId,
            "notificationId": notify.notificationId
        ]
        if userId != ""
        {
            let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading"
                
                request(baseURL + deleteANotificationFromList, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                        //print("Progress: \(progress.fractionCompleted)")
                    }
                    .validate { request, response, data in
                        // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                        return .success
                    }
                    .responseJSON { response in
                        
                        let convertedString = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                        print(convertedString as Any)
                        if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                            print("JSON: \(JSON)")
                            if let status = JSON["STATUS"] as? String {
                                
                                if (status == "SUCCESS") {
                                    self.NotificationTableView.beginUpdates()
                                    //if NotificationArray.count > 1 {
                                    self.NotificationTableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
                                    // }
                                    self.NotificationArray.removeObject(at: (indexPath as NSIndexPath).row)
                                    self.NotificationTableView.endUpdates()
                                    // self.tableView .reloadData()
                                }
                                else {
                                    if let error = JSON["ERROR"] as? String {
                                        // let error = stats as! String
                                        self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                    }
                                }
                            }
                            MBProgressHUD.hide(for: self.view, animated: true);
                        }
                        else {
                            MBProgressHUD.hide(for: self.view, animated: true);
                          //  print(response.result.error)
                            self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                        }
                        MBProgressHUD.hide(for: self.view, animated: true);
                }
            }else {
                MBProgressHUD.hide(for: self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
            //            let req = request(.GET, baseURL + ListNotifications, parameters: parameters, encoding :.JSON)
            //            debugPrint(req)
        }
        
    }
    
    func getNotificationCount() {
        
        let userId =  UserDefaults.standard.value(forKey: UserId) as! String
        
        let parameters : [String : String]  = [
            "userId": userId
        ]
        if userId != ""
        {
            let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                request(baseURL + getUnreadNotificationCount, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                    }
                    .validate { request, response, data in
                        return .success
                    }
                    .responseJSON { response in
                        if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                            
                            print("JSON: \(JSON)")
                            
                            if let supply = JSON["COUNT"] as? NSNumber{
                                let numberFormatter = NumberFormatter()
                                numberFormatter.numberStyle = NumberFormatter.Style.decimal
                                if let result = numberFormatter.string(from: supply) {
                                    if result != "0"{
                                        
                                        if let tabItems = self.tabBarController?.tabBar.items as NSArray?
                                        {
                                            // In this case we want to modify the badge number of the second tab:
                                            let tabItem = tabItems[2] as! UITabBarItem
                                            tabItem.badgeValue = result
                                        }
                                        
                                    }else {
                                        
                                        if let tabItems = self.tabBarController?.tabBar.items as NSArray?
                                        {
                                            // In this case we want to modify the badge number of the second tab:
                                            let tabItem = tabItems[2] as! UITabBarItem
                                            tabItem.badgeValue = nil
                                        }
                                        
                                    }
                                }
                            }
                            if let error = JSON ["ERROR"] as? String {
                                
                                print(" .....unread..........\(error)")
                                
                            }
                        }
                        else {
                            print(response.result.error!)
                            self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                        }
                }
            }else {
                // MBProgressHUD.hideForView(self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
        }
        
        
    }
}
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


