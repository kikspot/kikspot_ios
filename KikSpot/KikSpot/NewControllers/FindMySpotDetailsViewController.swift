//
//  FindMySpotDetailsViewController.swift
//  KikSpot
//
//  Created by Apple on 10/08/18.
//  Copyright © 2018 Shruti Mittal. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
import CoreLocation

class FindMySpotDetailsViewController: UIViewController,CLLocationManagerDelegate {
    
    
    @IBOutlet var locationURL_label: UILabel!
    
    @IBOutlet var phoneNumber_label: UILabel!
    
    @IBOutlet var address_label: UILabel!
    @IBOutlet var locationName_label: UILabel!
    var locationId : String!
    var locationName : String!
    var longitute: String!
    let locationManager = CLLocationManager()
    var didFinishCallingLocation = false
    var latitude: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        fetchLocationDetail()
        self.navigationItem.title = "FIND THE SPOT"
//        self.setNavigationBarItem()
        
        self.locationName_label.adjustsFontSizeToFitWidth = true
        self.address_label.adjustsFontSizeToFitWidth = true
        self.phoneNumber_label.adjustsFontSizeToFitWidth = true
        self.locationURL_label.adjustsFontSizeToFitWidth = true
        
        self.locationURL_label.isUserInteractionEnabled = true
        self.phoneNumber_label.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(FindMySpotDetailsViewController.PhoneLabelTapped))
        // tap.delegate = self
        self.phoneNumber_label.addGestureRecognizer(tap)
        
        let tapURL = UITapGestureRecognizer(target: self, action: #selector(FindMySpotDetailsViewController.URLLabelTapped))
        // tap.delegate = self
        self.locationURL_label.addGestureRecognizer(tapURL)
        
        if self.didFinishCallingLocation == false {
            self.locationManager.requestAlwaysAuthorization()
            
            // For use in foreground
            self.locationManager.requestWhenInUseAuthorization()
            
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
            }
        }
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        
        if self.didFinishCallingLocation == false {
            self.locationManager.requestAlwaysAuthorization()
            
            // For use in foreground
            self.locationManager.requestWhenInUseAuthorization()
            
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
            }
        }
       
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        if self.didFinishCallingLocation == false {
            // self.callTheServiceForRecommendation("\(locValue.latitude)", longitude: "\(locValue.longitude)")
            
            
        }
        
        self.didFinishCallingLocation = true
        locationManager.stopUpdatingLocation()
        latitude = "\(locValue.latitude)"
        longitute = "\(locValue.longitude)"
        fetchLocationDetail()
        
    }
    
    func fetchLocationDetail() {
        
        
        let userId =  UserDefaults.standard.value(forKey: UserId) as! String
        
        let parameters : [String : String]  = [
            "locationAPIId": self.locationId,
            "latitude": self.latitude,
            "longitude": self.longitute,
            "userId": userId
            ]
        if userId != ""
        {
            let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading"
                request(baseURL + getLocationDetails, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                        //print("Progress: \(progress.fractionCompleted)")
                    }
                    .validate { request, response, data in
                        // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                        return .success
                    }
                    .responseJSON { response in
                        if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                            
                            print("JSON: \(JSON)")
                            if let recommendations = JSON ["recommendationLocationDetails"] as? NSDictionary{
                                // var addressToLinkTo = ""
                                //for (var i = 0; i < recommendations.count; i++) {
                                // let tempDic = recommendations.objectAtIndex(i);
                                if recommendations.count > 0 {
                                    
                                    self.locationName_label.text = (recommendations["locationName"] as? String)!
                                    self.phoneNumber_label.text = (recommendations["phoneNo"] as? String)!
                                    self.locationURL_label.text = (recommendations["url"] as? String)!
                                    
                                    self.address_label.text = (recommendations["address"] as? String)!
                                    let ratingNumb = recommendations["ratings"] as? NSNumber
                                    let ratingStng = "\(ratingNumb!)"
                                    //                                    addressToLinkTo = (recommendations["url"] as? String!)!
                                    //
                                    //                                    if addressToLinkTo == ""  {
                                    //
                                    //                                        addressToLinkTo = "http://www.kikspot.com"
                                    //                                    }
                                    //                                    // }
                                    //                                    addressToLinkTo = addressToLinkTo.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
                                    //
                                    //                                    let url = NSURL(string: addressToLinkTo)
                                    //                                    UIApplication.sharedApplication().openURL(url!)
                                    let rating = ratingStng as NSString
                                    let ratingFloat = rating.floatValue
                                    let value = round(ratingFloat)
                                    let ratingString = NSString(format: "%.0f", value)
                                    
//                                    if ratingString == "1" {
//                                        self.self.ratingImage!.image = UIImage(named: "1.png")!
//                                    }else if ratingString == "2" {
//                                        self.ratingImage!.image = UIImage(named: "2.png")!
//                                    }else if ratingString == "3" {
//                                        self.ratingImage!.image = UIImage(named: "3.png")!
//                                    }else if ratingString == "4" {
//                                        self.ratingImage!.image = UIImage(named: "4.png")!
//                                    }else if ratingString == "5" {
//                                        self.ratingImage!.image = UIImage(named: "5.png")!
//                                    }else if ratingString == "6" {
//                                        self.ratingImage!.image = UIImage(named: "6.png")!
//                                    }else if ratingString == "7" {
//                                        self.ratingImage!.image = UIImage(named: "7.png")!
//                                    }else if ratingString == "8" {
//                                        self.ratingImage!.image = UIImage(named: "8.png")!
//                                    }else if ratingString == "9" {
//                                        self.ratingImage!.image = UIImage(named: "9.png")!
//                                    }else if ratingString == "10" {
//                                        self.ratingImage!.image = UIImage(named: "10.png")!
//                                    }
//
                                }
                                MBProgressHUD.hide(for: self.view, animated: true);
                            }
                            else {
                                if let error = JSON["ERROR"] as? String {
                                    print(error)
                                    // let error = stats as! String
                                    self.navigationController?.popViewController(animated: true)
                                    self.present(Common.showAlertWithTitle("Kikspot", message: "Unable to fetch location Website URL!", cancelButton: "OK"), animated: true, completion: nil)
                                }
//                                MBProgressHUD.hide(for: self.view, animated: true);
                            }
                            MBProgressHUD.hide(for: self.view, animated: true);
                            
                        }
                        else {
                            MBProgressHUD.hide(for: self.view, animated: true);
                            print(response.result.error as Any)
                            self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                        }
                }
            }else {
                MBProgressHUD.hide(for: self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
        }
        
    }
    
    
    func URLLabelTapped() {
        
        var addressToLinkTo = ""
        addressToLinkTo = self.locationURL_label.text!
        
        if addressToLinkTo == ""  {
            
            addressToLinkTo = "http://www.kikspot.com"
        }
        // }
        addressToLinkTo = addressToLinkTo.addingPercentEscapes(using: String.Encoding.utf8)!
        
        let url = URL(string: addressToLinkTo)
        UIApplication.shared.openURL(url!)
    }
    
    func PhoneLabelTapped () {
        var phoneNumber = self.phoneNumber_label.text
        phoneNumber = phoneNumber?.replacingOccurrences(of: " ", with: "")
        if let phoneCallURL:URL = URL(string:"tel://\(phoneNumber!)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.openURL(phoneCallURL);
            }
        }
        else {
            self.present(Common.showAlertWithTitle("Kikspot", message: "Calling feature is not available", cancelButton: "OK"), animated: true, completion: nil)
        }
    }

}
