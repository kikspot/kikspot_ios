//
//  MoreViewController.swift
//  KikSpot
//
//  Created by knsmac003 on 3/20/18.
//  Copyright © 2018 Shruti Mittal. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import MapKit
import MBProgressHUD

class MoreViewController: UIViewController,UITableViewDataSource , UITableViewDelegate, CLLocationManagerDelegate {
    
    var didFinishCallingLocation = false
    let locationManager = CLLocationManager()
    var longitute: String!
    var latitude: String!
    var common = Common()
    @IBOutlet weak var privacyPolicyLabel: UIButton!
    @IBOutlet weak var termsConditionsLabel: UIButton!
    @IBOutlet weak var moreTableView: UITableView!
    var recomendationArray : NSMutableArray = []
     var moreArray : NSMutableArray = []
     var moreImageArray : NSMutableArray = []
//    @IBOutlet var places_tableView : UITableView!
    
    var pointerToken: String = ""
    var selectedSort: String = "close"
    var alreadySelected : String = "close"

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = nil
     //   getShareUrl()
        let tapURL = UITapGestureRecognizer(target: self, action: #selector(MoreViewController.URLLabelTapped))
        // tap.delegate = self
        self.termsConditionsLabel.addGestureRecognizer(tapURL)
        let tapURL1 = UITapGestureRecognizer(target: self, action: #selector(MoreViewController.URLLabelTapped1))
        self.privacyPolicyLabel.addGestureRecognizer(tapURL1)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func URLLabelTapped1() {
        let tabbar = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as!  PrivacyPolicyViewController
        tabbar.urlFor = "PrivacyPolicy"
        self.navigationController?.pushViewController(tabbar, animated: true)
        
    }
    func URLLabelTapped() {
        let tabbar = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as!  PrivacyPolicyViewController
        tabbar.urlFor = "TermsAndConditions"
        self.navigationController?.pushViewController(tabbar, animated: true)
//                var addressToLinkTo = ""
//        addressToLinkTo = "http://www.kikspot.com/privacy-policy/"
//        addressToLinkTo = addressToLinkTo.addingPercentEscapes(using: String.Encoding.utf8)!
//        let url = URL(string: addressToLinkTo)
//        UIApplication.shared.openURL(url!)
    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
      
          self.tabBarController?.navigationItem.title = "More Options"
          self.navigationItem.hidesBackButton = true
         self.navigationItem.rightBarButtonItem = nil
        self.tabBarController?.navigationItem.rightBarButtonItem = nil
        moreArray = ["Find My spot","Profile","Settings","How to Play","Invite","Contact Us","Log Out"]
        moreImageArray = ["findmyspot.png","rewards.png","profile1.png","settings1.png","howtoplay.png","invite.png","contact-us.png","logout1.png"]
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        // print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        if self.didFinishCallingLocation == false {
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = MBProgressHUDMode.indeterminate
            loadingNotification.label.text = "Loading"
            self.callTheServiceForRecommendation("\(locValue.latitude)", longitude: "\(locValue.longitude)")
        }
        
        self.didFinishCallingLocation = true
        locationManager.stopUpdatingLocation()
        latitude = "\(locValue.latitude)"
        longitute = "\(locValue.longitude)"
        UserDefaults.standard.set(latitude , forKey: currentLatitude)
        UserDefaults.standard.set(longitute , forKey: currentLongitude)
    }
    
    func callTheServiceForRecommendation( _ latitude:String, longitude:String)
    {
        
        locationManager.stopUpdatingLocation()
        let userId =  UserDefaults.standard.value(forKey: UserId) as! String
        
        let parameters : [String : String]  = [
            "userId": userId,
            "latitude": latitude,
            "longitude": longitude,
            "pageToken" : self.pointerToken,
            "sortBy" : selectedSort
        ]
        print(parameters)
        if userId != ""
        {
            let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading"
                
                request(baseURL + RecommendationURL, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                    }
                    .validate { request, response, data in
                        return .success
                    }
                    .responseJSON { response in
                        if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                            
                            print("JSON: \(JSON)")
                            if let recommendations = JSON ["recommendationLocations"] as? NSArray{
                                self.pointerToken =  (JSON ["pageToken"] as? String)!
                                let dataHolder : NSMutableArray = []
                                for i in 0 ..< recommendations.count {
                                    
                                    
                                    let tempDic = recommendations .object(at: i) as! [String : AnyObject]
                                    let recommend = Recommendation()
                                    recommend.distance = (tempDic["distance"] as? String)!
                                    recommend.trending = (tempDic["trending"] as? String)!
                                    
                                    var iconURL = tempDic["iconUrl"] as? String
                                    if (iconURL == nil) {
                                        iconURL = ""
                                    }
                                    recommend.iconUrl = iconURL!
                                    //   recommend.iconUrl = (tempDic["iconUrl"] as? String)!
                                    let isNew = tempDic["isNew"] as? NSNumber
                                    recommend.isNew = "\(isNew!)"
                                    let hasActiveRule = tempDic["hasActiveRules"] as? NSNumber
                                    recommend.hasActiveRules = "\(hasActiveRule!)"
                                    let longitude = tempDic["longitude"] as? NSNumber
                                    recommend.longitude = "\(longitude!)"
                                    
                                    recommend.locationAPIId = (tempDic["locationAPIId"] as? String)!
                                    recommend.locationName = (tempDic["locationName"] as? String)!
                                    // recommend.latitude = (tempDic["latitude"] as? NSNumber!)!
                                    let latitude = tempDic["latitude"] as? NSNumber
                                    recommend.latitude = "\(latitude!)"
                                    let rating = tempDic["rating"] as? NSNumber
                                    recommend.rating = "\(rating!)"
                                    dataHolder .add(recommend)
                                }
                                
                                self.recomendationArray = dataHolder
                                // }
                                print(self.recomendationArray.count)
                                MBProgressHUD.hide(for: self.view, animated: true);
                                self.moreTableView.reloadData()
                            }
                            else {
                                if let error = JSON["ERROR"] as? String {
                                    // let error = stats as! String
                                    MBProgressHUD.hide(for: self.view, animated: true);
                                    self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                }
                            }
                            MBProgressHUD.hide(for: self.view, animated: true);
                        }
                        else {
                            MBProgressHUD.hide(for: self.view, animated: true);
                            print(response.result.error!)
                            self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                        }
                }
            }else {
                MBProgressHUD.hide(for: self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
        }
    }
    
    // MARK : GetURL and REferral Code
    func getShareUrl() {
        
        let userDefaults : UserDefaults = UserDefaults.standard
        var userId :String = ""
        if (userDefaults.object(forKey: UserId) != nil) {
            userId =  UserDefaults.standard.value(forKey: UserId) as! String
        }
        // let userId =  NSUserDefaults.standardUserDefaults().valueForKey(UserId) as! String
        
        let parameters : [String : String]  = [
            "userId": userId,
            ]
        if userId != ""
        {
            let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                //                let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
                //                loadingNotification.mode = MBProgressHUDMode.Indeterminate
                //                loadingNotification.label.text = "Loading"
                request(baseURL + getShareURLservice, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                        //print("Progress: \(progress.fractionCompleted)")
                    }
                    .validate { request, response, data in
                        // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                        return .success
                    }
                    .responseJSON { response in
                        if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                            
                            print("JSON: \(JSON)")
                            if let error = JSON["ERROR"] as? String {
                                // self.presentViewController(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                print(error)
                            }
                            else {
                                if  let url = JSON ["URL"] as? String {
                                    UserDefaults.standard.set("\(url)" , forKey: urlToShare)
                                }
                                if let message = JSON ["message"] as? String {
                                    UserDefaults.standard.set("\(message)" , forKey: messageToShare)
                                }
                                if let referralCode = JSON ["referralCode"] as? String {
                                    UserDefaults.standard.set("\(referralCode)" , forKey: userreferralCode)
                                }
                            }
                            // MBProgressHUD.hideForView(self.view, animated: true);
                        }
                        else {
                            //  MBProgressHUD.hideForView(self.view, animated: true);
                            print(response.result.error as Any)
                            //                            self.presentViewController(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                        }
                }
            }else {
                //   MBProgressHUD.hideForView(self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
            
        }
        
        
    }
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.moreArray.count
        
      //  return 10
    }
    
    /*func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell:UITableViewCell!
        cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        
        let locationName = cell.viewWithTag(101) as? UILabel ;
        
        let imageView = cell.viewWithTag(102) as? UIImageView

        locationName?.text = self.moreArray[indexPath.row] as? String
        
        imageView?.image = UIImage(named: moreImageArray[indexPath.row] as! String)
        
      //  locationName?.text = "arun" ;
        
        
        return cell ;
    }
    
    
    
func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    
    if ((indexPath as NSIndexPath).row == 0){
        
        let tabbar = self.storyboard?.instantiateViewController(withIdentifier: "tabbarcontroller") as!  TabBarViewController
        tabbar.selectedIndex = 0
          self.navigationController?.pushViewController(tabbar, animated: true)
    
    }
//    else if ((indexPath as NSIndexPath).row == 1){
//
//        self.recomendationArray = recomendationsArray
//
//        let locationApID_array : NSMutableArray = []
//        for i in 0  ..< self.recomendationArray.count {
//            let recommend = recomendationArray[i] as! Recommendation
//            let locationId = recommend.locationAPIId
//            locationApID_array .add(locationId)
//        }
//        let rulesView = self.storyboard?.instantiateViewController(withIdentifier: "RulesViewController") as! RulesViewController
//        rulesView.locationId_array = locationApID_array
//        rulesView.callServiceFore = "listOfLocation"
//        self.navigationController?.pushViewController(rulesView, animated: true)
//
//    }
    else if ((indexPath as NSIndexPath).row == 1) {
    
//    let userDefaults : UserDefaults = UserDefaults.standard
//    var isUserLimitedLook :String = ""
//    if (userDefaults.value(forKey: isUserALimitedLook) != nil) {
//    isUserLimitedLook =  userDefaults.value(forKey: isUserALimitedLook) as! String
//    }
//    if isUserLimitedLook == "YES" {
//
//    let alertView = UIAlertController(title: "You need to log in first", message: "Please Register to access this section.", preferredStyle: .alert)
//    alertView.addAction(UIAlertAction(title: "Register Now", style: .default, handler: { (alertAction) -> Void in
//
//
//    let mainViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
//    self.navigationController?.pushViewController(mainViewController, animated: true)
//    let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
//    self.slideMenuController()?.changeMainViewController(nvc, close: true)
//    }))
//    alertView.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//    present(alertView, animated: true, completion: nil)
//    }
  //  else {
    
    let mainViewController = storyboard.instantiateViewController(withIdentifier: "EditProfile") as! EditProfileViewController
    mainViewController.cameFrom = "sideBar"
    self.navigationController?.pushViewController(mainViewController, animated: true)
    
 //   }
    }
    else if ((indexPath as NSIndexPath).row == 2){
    
    let mainViewController = storyboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
    self.navigationController?.pushViewController(mainViewController, animated: true)
    
    }
    else if ((indexPath as NSIndexPath).row == 4){
        
        let userDefaults : UserDefaults = UserDefaults.standard
        var url :String = ""
        if (userDefaults.value(forKey: urlToShare) != nil) {
            url =  UserDefaults.standard.value(forKey: urlToShare) as! String
        } else {
            url = "http://www.kikspot.com/"
        }
        
        var message :String = ""
        var referral :String = ""
        if (userDefaults.value(forKey: userreferralCode) != nil) {
            referral =  UserDefaults.standard.value(forKey: userreferralCode) as! String
        }
        
        message = "Hey, join me on this app so we can find the spot, hit the spot and rate the spot anywhere, anytime and get everyone involved in the fun. Download and check Kikspot out here: \(url). Use my referral code: \(referral) while creating your account"
        let objectsToShare = [message]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        
        self.present(activityVC, animated: true, completion: nil)
        activityVC.completionWithItemsHandler = { (activity: UIActivityType?, completed: Bool, returnedItems: [Any]?, error: Error?) in
            
            // Return if cancelled
            if (!completed) {
                print(" cancelled***********")
                return
            } else {
                let userDefaults : UserDefaults = UserDefaults.standard
                var userId :String = ""
                if (userDefaults.object(forKey: UserId) != nil) {
                    userId =  UserDefaults.standard.value(forKey: UserId) as! String
                }
                // let userId =  NSUserDefaults.standardUserDefaults().valueForKey(UserId) as! String
                
                let parameters : [String : String]  = [
                    "userId": userId,
                    "type": "Share App"
                ]
                if userId != ""
                {
                    let reachability: Reachability = Reachability.forInternetConnection()
                    if  reachability.currentReachabilityStatus().rawValue != 0  {
                        request(baseURL + sendShareStatus, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                            .downloadProgress(queue: DispatchQueue.utility) { progress in
                                //print("Progress: \(progress.fractionCompleted)")
                            }
                            .validate { request, response, data in
                                // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                                return .success
                            }
                            .responseJSON { response in
                                if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                                    
                                    print("JSON: \(JSON)")
                                    if let userGameCred = JSON ["userGameCreds"] as? NSDictionary{
                                        let creds = userGameCred["earnedCreds"] as? NSNumber
                                        
                                        self.present(Common.showAlertWithTitle("Kikspot", message: "Congrats! You just earned \(creds!) cred! Go to the Notifications Screen for details", cancelButton: "OK"), animated: true, completion: nil)
                                    }
                                }
                                else {
                                    
                                    print(response.result.error!)
                                    
                                }
                        }
                    }else {
                        //   MBProgressHUD.hideForView(self.view, animated: true);
                        self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
                    }
                    
                }
                
            }
            //activity complete
            //some code here
            
            
        }
    }
    else if ((indexPath as NSIndexPath).row == 5){
    let mainViewController = storyboard.instantiateViewController(withIdentifier: "ContactUs") as! ContactUsViewController
        self.navigationController?.pushViewController(mainViewController, animated: true)
    }
  
    else if ((indexPath as NSIndexPath).row == 7){
    
    let userDefaults : UserDefaults = UserDefaults.standard
    var url : String = ""
    if (userDefaults.value(forKey: urlToShare) != nil) {
    url =  UserDefaults.standard.value(forKey: urlToShare) as! String
    } else {
    url = "http://www.kikspot.com/"
    }
    
    var message :String = ""
    //        if (userDefaults.valueForKey(messageToShare) != nil) {
    //            message =  NSUserDefaults.standardUserDefaults().valueForKey(messageToShare) as! String
    //        } else {
    //            message = "Hey! Checkout this new app, Kikspot!"
    //        }
    var referral :String = ""
    if (userDefaults.value(forKey: userreferralCode) != nil) {
    referral =  UserDefaults.standard.value(forKey: userreferralCode) as! String
    }
    
    message = "Hey, join me on this app so we can find the spot, hit the spot and rate the spot anywhere, anytime and get everyone involved in the fun. Download and check Kikspot out here: \(url). Use my referral code: \(referral) while creating your account."
    let objectsToShare = [message]
    let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
    
    self.present(activityVC, animated: true, completion: nil)
    activityVC.completionWithItemsHandler = { (activity: UIActivityType?, completed: Bool, returnedItems: [Any]?, error: Error?) in
    
    // Return if cancelled
    if (!completed) {
    print(" cancelled***********")
    return
    } else {
    let userDefaults : UserDefaults = UserDefaults.standard
    var userId :String = ""
    if (userDefaults.object(forKey: UserId) != nil) {
    userId =  UserDefaults.standard.value(forKey: UserId) as! String
    }
    // let userId =  NSUserDefaults.standardUserDefaults().valueForKey(UserId) as! String
    
    let parameters : [String : String]  = [
    "userId": userId,
    "type": "Share App"
    ]
    if userId != ""
    {
    let reachability: Reachability = Reachability.forInternetConnection()
    if  reachability.currentReachabilityStatus().rawValue != 0  {
    request(baseURL + sendShareStatus, method: .post, parameters: parameters, encoding: JSONEncoding.default)
    .downloadProgress(queue: DispatchQueue.utility) { progress in
    //print("Progress: \(progress.fractionCompleted)")
    }
    .validate { request, response, data in
    // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
    return .success
    }
    .responseJSON { response in
    if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
    
    print("JSON: \(JSON)")
    if let userGameCred = JSON ["userGameCreds"] as? NSDictionary{
    let creds = userGameCred["earnedCreds"] as? NSNumber
    
    self.present(Common.showAlertWithTitle("Kikspot", message: "Congrats! You just earned \(creds!) cred! Go to the Notifications Screen for details!", cancelButton: "OK"), animated: true, completion: nil)                                         }
    }
    else {
    
    print(response.result.error!)
    
    }
    }
    }else {
    //   MBProgressHUD.hideForView(self.view, animated: true);
    self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
    }
    
    }
    
    }
    }
    // }
    }
    else if ((indexPath as NSIndexPath).row == 3){
        
        
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "GameLevelDetails") as! GameLevelDetails
        self.navigationController?.pushViewController(mainViewController, animated: true)

        
    }
    

    else if ((indexPath as NSIndexPath).row == 6){
    
    // NSUserDefaults.standardUserDefaults().removePersistentDomainForName(NSBundle.mainBundle().bundleIdentifier!)
    UserDefaults.standard.removeObject(forKey: UserId)
    UserDefaults.standard.removeObject(forKey: savedUserName)
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    ////
    let mainViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
    self.navigationController?.pushViewController(mainViewController, animated: true)
    
 //   let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
    // leftViewController.mainViewController = nvc
   // self.slideMenuController()?.changeMainViewController(nvc, close: true)
    }
    
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
