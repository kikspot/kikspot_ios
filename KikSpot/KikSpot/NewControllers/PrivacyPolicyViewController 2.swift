//
//  PrivacyPolicyViewController.swift
//  KikSpot
//
//  Created by Shruti Mittal on 02/04/19.
//  Copyright © 2019 Shruti Mittal. All rights reserved.
//

import UIKit
import MBProgressHUD

class PrivacyPolicyViewController: UIViewController,UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    var urlFor:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        if urlFor == "PrivacyPolicy"{
        webView.loadRequest(NSURLRequest(url: NSURL(string: "https://www.kikspot.com/privacy-policy")! as URL) as URLRequest)
            self.navigationItem.title = "Privacy Policy"
        }else if urlFor == "TermsAndConditions"{
            webView.loadRequest(NSURLRequest(url: NSURL(string: "https://www.kikspot.com/terms-of-use")! as URL) as URLRequest)
            self.navigationItem.title = "Terms of Use"
        }
        webView.delegate = self
//        self.navigationItem.title = "Privacy Policy"
        // Do any additional setup after loading the view.
    }
    func webViewDidStartLoad(_ webView: UIWebView) {
        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Loading"
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        MBProgressHUD.hide(for: self.view, animated: true);
    }
}
