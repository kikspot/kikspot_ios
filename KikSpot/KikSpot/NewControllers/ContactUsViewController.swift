//
//  ContactUsViewController.swift
//  KikSpot
//
//  Created by Shruti Mittal on 01/04/18.
//  Copyright © 2018 Shruti Mittal. All rights reserved.
//

import UIKit
import MBProgressHUD

class ContactUsViewController: UIViewController,UITextViewDelegate {
    
    @IBOutlet weak var feedbackTextView: UITextView!
    
    var newBackButton: UIBarButtonItem!
    var comingFrom:String = ""

    
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Contact Us"
        self.navigationItem.hidesBackButton = true
        newBackButton = UIBarButtonItem(image: UIImage(named: "Back.png"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(ContactUsViewController.back(_:)))
        self.navigationItem.leftBarButtonItem = newBackButton;

        
        let userDefaults : UserDefaults = UserDefaults.standard
        var userName :String = ""
        if (userDefaults.object(forKey: savedUserName) != nil) {
            userName =  UserDefaults.standard.value(forKey: savedUserName) as! String
        }
        self.emailFromLabel.text! = userName
        var usermail :String = ""
        if (userDefaults.object(forKey: UserEmail) != nil) {
            usermail =  UserDefaults.standard.value(forKey: UserEmail) as! String
        }
        self.emailAddressLabel.text! = usermail
        
        feedbackTextView.layer.cornerRadius = 10;
        feedbackTextView.clipsToBounds = true
        sendbutton.layer.cornerRadius = 5
        sendbutton.clipsToBounds = true
    }
    func back(_ sender: UIBarButtonItem) {
        // Perform your custom actions
        // ...
        // Go back to the previous ViewController
        if comingFrom == "Home"{
            let tabbar = self.storyboard?.instantiateViewController(withIdentifier: "tabbarcontroller") as!  TabBarViewController
            tabbar.selectedIndex = 0
            self.navigationController?.pushViewController(tabbar, animated: true)
        }else{
           self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func sendButtonPressed(_ sender: Any) {
        let comment : String = self.feedbackTextView.text!;
        if comment == ""
        {
            self.present(Common.showAlertWithTitle("Kikspot", message: "Please Enter Comments", cancelButton: "OK"), animated: true, completion: nil)
        }
        else if self.commentsTextView.text.characters.count >= 300{
            self.present(Common.showAlertWithTitle("Kikspot", message: "The Comments should not exists greater than 300 characters ", cancelButton: "OK"), animated: true, completion: nil)
        }
        else {
            let characterset = CharacterSet(charactersIn:
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789~@!#$%^&*()_+:<>?/=-][|*.,"
            )
            if comment.rangeOfCharacter(from: characterset.inverted) != nil {
                print("true")
            } else {
                print("false")
            }
            let userId =  UserDefaults.standard.value(forKey: UserId) as! String
            // let imageData = UIImageJPEGRepresentation(self.imageView.image!, 0.7)
            let parameters: [String : String] = [
                "feedbackMessage": comment,
                "userId": userId,
                
                ]
            var theJSONText : NSString  = ""
            
            do {
                let theJSONData = try JSONSerialization.data(
                    withJSONObject: parameters ,
                    options: JSONSerialization.WritingOptions(rawValue: 0))
                theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)!
                
                
            } catch let error as NSError {
                
                
                print(error)
            }
            
            let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading"
                
                //  let urlRequest = urlRequestWith(baseURL + sendLocImageToServer, parameters: theJSONText  , imageData: imageData!)
                upload(
                    multipartFormData: { multipartFormData in
                        // multipartFormData.append(data: imageData, name: "images", fileName: "images.png", mimeType: "image/jpeg")
                        //                        if self.selectedImage != nil {
                        //                            let imageData = UIImageJPEGRepresentation(self.selectedImage, 0.7)
                        //                            multipartFormData.append(imageData!, withName: "files", fileName: "files.png", mimeType: "image/jpeg")
                        //                        }
                        //                        if (self.selectedVideaoUrl != nil){
                        //                            let videoData = try? Data(contentsOf: self.selectedVideaoUrl)
                        //                            multipartFormData.append(videoData!, withName: "files", fileName: "files.mov", mimeType: "video/mov")
                        //                        }
                        multipartFormData.append(theJSONText.data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)!, withName: "json")
                },
                    to: baseURL + sendFeedback,
                    encodingCompletion: { encodingResult in
                        switch encodingResult {
                        case .success(let upload, _, _):
                            upload.responseJSON { response in
                                debugPrint(response)
                                if let JSON:[String:AnyObject] = response.result.value as! [String : AnyObject]?  {
                                    print("JSON: \(JSON)")
                                    
                                    
                                    if let status = JSON["STATUS"] as? String {
                                        
                                        if (status == "Feedback Added Successfully") {
                                            //   self.closePopUp()
                                            let alert = UIAlertController(title: "Kikspot", message:"Your words have been happily received! Spot on!", preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {(action: UIAlertAction!) in
                                                let tabbar = self.storyboard?.instantiateViewController(withIdentifier: "tabbarcontroller") as!  TabBarViewController
                                                tabbar.selectedIndex = 0
                                                self.navigationController?.pushViewController(tabbar, animated: true)
                                                
                                            }))
                                            self.present(alert, animated: true, completion: nil)
//                                            self.present(Common.showAlertWithTitle("KikSpot", message: "Feedback added successfully", cancelButton: "OK"), animated: true, completion: nil)
//                                           let morevc = self.storyboard?.instantiateViewController(withIdentifier: "MoreOptions") as! MoreViewController
//                                            self.navigationController?.pushViewController(morevc, animated: true)
                                        }
                                    }
                                        
                                    else {
                                        if let error = JSON["ERROR"] as? String {
                                            // let error = stats as! String
                                            self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                        }
                                    }
                                    MBProgressHUD.hide(for: self.view, animated: true);
                                }
                                else {
                                    MBProgressHUD.hide(for: self.view, animated: true);
                                    print(response.result.error as Any)
                                    self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                                }
                                MBProgressHUD.hide(for: self.view, animated: true);
                            }
                        case .failure(let encodingError):
                            print(encodingError)
                        }
                }
                )
                
            }else {
                MBProgressHUD.hide(for: self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
            // let req = request(.GET, baseURL + sendFeedback, parameters: parameters , encoding :.JSON)
            // debugPrint(req)
//            } else {
//                self.present(Common.showAlertWithTitle("KikSpot", message: "Please enter valid characters", cancelButton: "OK"), animated: true, completion: nil)
//                print("doesn't exist")
//            }
        }
        
        
    }
    @IBOutlet var sendbutton: UIButton!
    @IBOutlet weak var commentsTextView: UITextView!
    @IBOutlet weak var emailAddressLabel: UILabel!
    @IBOutlet weak var emailFromLabel: UILabel!
    //    override func viewDidLoad() {
    //        super.viewDidLoad()
    //
    //        // Do any additional setup after loading the view.
    //    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        return true
    }
    
    func textViewDidBeginEditing (_ textView: UITextView) {
        
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
//        if(text == " ") {
//            return true
//        }
        let currentText = textView.text ?? ""
        var characterset = CharacterSet(charactersIn:"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789~@!#$%^&*()_+:<>?/=-][|*., ")
//        following code for accepting the emoji's 
//        characterset.insert(charactersIn: "\u{1F300}"..<"\u{1F700}")
//        characterset.insert(charactersIn: "\u{1F600}"..<"\u{1F64F}")
//        characterset.insert(charactersIn: "\u{1F300}"..<"\u{1F5FF}")
//        characterset.insert(charactersIn: "\u{1F1E6}"..<"\u{1F1FF}")
//        characterset.insert(charactersIn: "\u{2600}"..<"\u{26FF}")
//        characterset.insert(charactersIn: "\u{E0020}"..<"\u{E007F}")
//        characterset.insert(charactersIn: "\u{FE00}"..<"\u{FE0F}")
//        characterset.insert(charactersIn: "\u{1F900}"..<"\u{1F9FF}")
        /*
        0x1F600...0x1F64F, // Emoticons
        0x1F300...0x1F5FF, // Misc Symbols and Pictographs
        0x1F680...0x1F6FF, // Transport and Map
        0x1F1E6...0x1F1FF, // Regional country flags
        0x2600...0x26FF, // Misc symbols
        0x2700...0x27BF, // Dingbats
        0xE0020...0xE007F, // Tags
        0xFE00...0xFE0F, // Variation Selectors
        0x1F900...0x1F9FF, // Supplemental Symbols and Pictographs
        127000...127600, // Various asian characters
        65024...65039, // Variation selector
        9100...9300, // Misc items
        8400...8447: // Combining Diacritical Marks for Symbols
        */
        guard let stringRange = Range(range, in: currentText)
            else
        {
            return false
        }
        let changedText = currentText.replacingCharacters(in: stringRange, with: text)
        if changedText.rangeOfCharacter(from: characterset.inverted) != nil {
            print("true")
            return false
        } else {
            print("false")
            return true
        }
    }
}

