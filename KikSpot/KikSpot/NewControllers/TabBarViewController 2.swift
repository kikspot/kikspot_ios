//
//  TabBarViewController.swift
//  KikSpot
//
//  Created by knsmac003 on 3/20/18.
//  Copyright © 2018 Shruti Mittal. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {
    @IBOutlet weak var tabbar: UITabBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
  
        self.navigationItem.setHidesBackButton(true, animated: false)
        
        let backButton : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "Back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(TabBarViewController.back(_:)))
        
        self.navigationItem.leftBarButtonItem = backButton
        
        
//        let logButton : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "Howtoearn.png"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(TabBarViewController.howtoplay(_:)))
//        
//        self.navigationItem.rightBarButtonItem = logButton
        
      
        
        }
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
//        if tabBar.tag == 1 {
//            
//        }
//        print(item.tag)
//        if item.tag == 3 {
//            let rulesView = self.storyboard?.instantiateViewController(withIdentifier: "RulesViewController") as! RulesViewController
//            rulesView.callServiceFore = "listOfLocation"
//            self.navigationController?.pushViewController(rulesView, animated: true)
//        }
    }
    
    func back(_ sender: UIBarButtonItem) {
        // Perform your custom actions
        // ...
        // Go back to the previous ViewController
        
        let tabbar = self.storyboard?.instantiateViewController(withIdentifier: "tabbarcontroller") as!  TabBarViewController
        
        self.navigationController?.pushViewController(tabbar, animated:false)
    
      //  self.tabBarController?.selectedIndex = 0 ;
         //
    }
    func howtoplay(_ sender: UIBarButtonItem) {
        // Perform your custom actions
        // ...
        // Go  to the mentioned ViewController
        
        let gamelevel = self.storyboard?.instantiateViewController(withIdentifier: "GameLevelDetails") as!  GameLevelDetails
        
        self.navigationController?.pushViewController(gamelevel, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
         //  self.navigationController?.isNavigationBarHidden = true
        super.viewWillAppear(animated) // No need for semicolon
        let navigationBarAppearace = UINavigationBar.appearance()
        
        navigationBarAppearace.barTintColor = defaultNavBarColor
        
        navigationBarAppearace.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        
       
        
        
    }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
