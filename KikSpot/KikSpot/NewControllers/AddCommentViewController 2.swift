//
//  AddCommentViewController.swift
//  KikSpot
//
//  Created by knsmac003 on 4/18/18.
//  Copyright © 2018 Shruti Mittal. All rights reserved.
//

import UIKit
import MBProgressHUD

class AddCommentViewController: UIViewController,UITextViewDelegate {

    @IBOutlet weak var labelInsideTextView: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var addCommentbtn: UIButton!
    @IBOutlet weak var textviewComments: UITextView!
    @IBOutlet weak var username: UILabel!
    var activetextView: UITextView!
    
    var locationID: String!
    var PreviousSliderValue : String!
    var longitute: String!
    var latitude: String!
     var userRatingID: String!
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
          print("loctaionID: \(locationID)")
           print("value: \(PreviousSliderValue)")
           print("long: \(longitute)")
           print("lat: \(latitude)")
          print("lat: \(userRatingID)")
        
        
         self.textviewComments.delegate = self
        self.navigationItem.title = "Add Comments"
        self.labelInsideTextView.isHidden = false
        self.navigationItem.setHidesBackButton(true, animated: false)
        

        
   let homeButton : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "Back.png"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(AddCommentViewController.goback(_:)))
        
        self.navigationItem.leftBarButtonItem = homeButton
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AddCommentViewController.dismissKeyboard))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        tap.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tap)
        
        let userDefaults : UserDefaults = UserDefaults.standard
        var userName :String = ""
        if (userDefaults.object(forKey: savedUserName) != nil) {
            userName =  UserDefaults.standard.value(forKey: savedUserName) as! String
        }
        self.username.text! = userName
        
        self.addCommentbtn.layer.cornerRadius = 10;
        self.addCommentbtn.clipsToBounds = true
        self.addCommentbtn.layer.cornerRadius = 5
        self.addCommentbtn.clipsToBounds = true

        // Do any additional setup after loading the view.
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func addBtnPressed(_ sender: Any) {
        
        
        sendCommentsToServer()
        
        
    }
    
    func goback(_ sender: UIBarButtonItem) {
      
          self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        deregsterFromKeyboardNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        regsterForKeyboardNotifications()
      //  self.textviewComments.becomeFirstResponder()
        
        
    }
    
    func regsterForKeyboardNotifications()
    {
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(AddCommentViewController.keyboardisShown(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AddCommentViewController.keyboardWillgetHidden(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    func deregsterFromKeyboardNotifications()
    {
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardisShown(_ notification: Notification)
    {
        
        self.scrollView.isScrollEnabled = true
        let info : NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size

        
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
    //   self.scrollView.setContentOffset(CGPoint(x: 0,y: 50), animated: true)

        
   
            if (activetextView != nil)
        {
         
            if (!aRect.contains(activetextView!.frame.origin))
            {
                self.scrollView.scrollRectToVisible(activetextView! .frame, animated: true)
            }
        }
        
        
    }
    
    
    func keyboardWillgetHidden(_ notification: Notification)
    {
        self.scrollView.contentInset = UIEdgeInsets.zero
        self.scrollView.scrollIndicatorInsets = UIEdgeInsets.zero
        
        self.activetextView?.resignFirstResponder()
        self.scrollView.setContentOffset(CGPoint(x: 0,y: 0), animated: true)
        
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        activetextView = textView
          self.labelInsideTextView.isHidden = true
        return true
    }
    
    func textViewDidBeginEditing (_ textView: UITextView) {
        activetextView = textView
        
        if textView.text.isEmpty {
            
            self.labelInsideTextView.isHidden = false
            
            
            
        }
        else {
            self.labelInsideTextView.isHidden = true
            
        }
    
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        activetextView = nil
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if textView.text.isEmpty {
            self.labelInsideTextView.isHidden = false
        }
        else {
         self.labelInsideTextView.isHidden = true
            
        }
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    func encode(_ s: String) -> String {
        let data = s.data(using: .nonLossyASCII, allowLossyConversion: true)!
        return String(data: data, encoding: .utf8)!
    }
    
    func sendCommentsToServer() {
        
        let userDefaults : UserDefaults = UserDefaults.standard
        var userId :String = ""
        if (userDefaults.object(forKey: UserId) != nil) {
            userId =  UserDefaults.standard.value(forKey: UserId) as! String
        }
    
        var userComment : String = ""
        if textviewComments != nil {
//            userComment = self.textviewComments.text
            let e = encode(self.textviewComments.text)
            print(e.count)
            if e.count >= 1000{
                self.present(Common.showAlertWithTitle("Kikspot", message: "Exceeds the maximum limit of characters", cancelButton: "OK"), animated: true, completion: nil)
            }else{
            userComment = e
                let parameters : [String : String]  = [
                    "userId": userId,
                    "locationAPIId": locationID,
                    "comment": userComment,
                    "rating": PreviousSliderValue,
                    "latitude" : latitude,
                    "longitude" : longitute,
                    "userLocationRatingId" : userRatingID,
                    ]
                print(parameters)
                
                
                if userId != ""
                {
                    let reachability: Reachability = Reachability.forInternetConnection()
                    if  reachability.currentReachabilityStatus().rawValue != 0  {
                        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                        loadingNotification.mode = MBProgressHUDMode.indeterminate
                        loadingNotification.label.text = "Loading"
                        request( baseURL + sendLocationRatingToServer, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                            .downloadProgress(queue: DispatchQueue.utility) { progress in
                                //print("Progress: \(progress.fractionCompleted)")
                            }
                            .validate { request, response, data in
                                // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                                return .success
                            }
                            .responseJSON { response in
                                if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                                    
                                    print("JSON: \(JSON)")
                                    if let error = JSON["ERROR"] as? String {
                                        //    self.sliderValue = "0"
                                        //   let ratingString = self.sliderValue as NSString
                                        //   self.rating_slider.value = (ratingString.floatValue)
                                        //   self.rating_imageView.image = nil
                                        self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                        self.textviewComments.resignFirstResponder()
                                    }
                                    else {
                                        self.textviewComments.text = ""
                                        userComment = ""
                                        
                                        
                                        let defaults = UserDefaults.standard
                                        defaults.set(true, forKey: isRated)
                                        if let ratingi = JSON["userLocationRatingId"] as? NSNumber {
                                            // let user = userId as! NSNumber
                                            UserDefaults.standard.set("\(ratingi)" ,forKey: UserlocationRatingID)
                                            //   self.userLocationRatingID = ratingi
                                            
                                            print("......\(ratingi)")
                                            
                                        }
                                        
                                        
                                        self.navigationController?.popViewController(animated: true)
                                        self.present(Common.showAlertWithTitle("Kikspot", message: "Rating Submitted", cancelButton: "OK"), animated: true, completion: nil)
                                        
                                        // self.navigationController?.popViewController(animated: true)
                                    }
                                    MBProgressHUD.hide(for: self.view, animated: true);
                                }
                                else {
                                    MBProgressHUD.hide(for: self.view, animated: true);
                                    print(response.result.error!)
                                    //                            self.presentViewController(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                                }
                        }
                    }else {
                        MBProgressHUD.hide(for: self.view, animated: true);
                        self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
                    }
                    
                }
            }
        }
        

  
        
    }



}
