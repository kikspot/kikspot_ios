//
//  ScoreBoardViewController.swift
//  KikSpot
//
//  Created by knsmac003 on 3/20/18.
//  Copyright © 2018 Shruti Mittal. All rights reserved.
//

import UIKit
import MBProgressHUD

class ScoreBoardViewController: UIViewController {
    
    var gameDetailsArray: NSArray!
    var getCredFor: String!
    var locationString: String!
    var timeString: String!
    
   

    @IBOutlet var yourNextLevelLabel: UILabel!
    @IBOutlet var yourCurrentLevelLabel: UILabel!
    @IBOutlet weak var yourCredsLabel: UILabel!
    @IBOutlet weak var yourRankLabel: UILabel!
    @IBOutlet weak var credsToNextLevelLabel: UILabel!
    
    @IBOutlet weak var scoreBoardCollectionView: UICollectionView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
         segmentControl.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.white], for: UIControlState.selected)
        segmentControl.selectedSegmentIndex = 1;

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func howtoplay(_ sender: UIBarButtonItem) {
        // Perform your custom actions
        // ...
        // Go  to the mentioned ViewController
        
        let gamelevel = self.storyboard?.instantiateViewController(withIdentifier: "GameLevelDetails") as!  GameLevelDetails
        
        self.navigationController?.pushViewController(gamelevel, animated: true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.navigationItem.setHidesBackButton(true, animated: false)
//        removeNavigationBarRightItem()

        
        self.tabBarController?.navigationItem.title = "Scoreboard"
        
    //    self.scoreBoardCollectionView.layer.cornerRadius = 10
   //     self.scoreBoardCollectionView.layer.masksToBounds = true
        //  self.navigationItem.title = "Scoreboard"
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: self.scoreBoardCollectionView.frame.size.width/3, height: 40)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        self.scoreBoardCollectionView.collectionViewLayout = layout
        
        let logButton : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "Howtoplay.png"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(ScoreBoardViewController.howtoplay(_:)))
        
        self.tabBarController?.navigationItem.rightBarButtonItem = logButton
        
        locationString = "contry"
        timeString = "alltime"
        
        /*if self.getCredFor == "Friend" {
            getlistOfCredsOFFriends()
        }
        else {
            getlistOfCredsOFEveryone()
        }*/
      //  getlistOfCredsOFFriends()
        
        getUserScoreCard()
        
    }
    
   
    

    @IBAction func segmentSelected(_ sender: Any) {
        
         print("selected index: \((sender as AnyObject).selectedSegmentIndex)")
        
        
        switch segmentControl.selectedSegmentIndex
        {
        case 0:
            getlistOfCredsOFFriends()
            
            
        case 1:
            getlistOfCredsOFEveryone()
            
        default:
            
            break
            
        }
    
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func getUserScoreCard() {
        
        
        let userId =  UserDefaults.standard.value(forKey: UserId) as! String
        
        let parameters : [String : String]  = [
            "userId": userId,
            ]
        if userId != ""
        {
            let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading"
                
                request(baseURL + getGameDetailsForUser, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                        //print("Progress: \(progress.fractionCompleted)")
                    }
                    .validate { request, response, data in
                        // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                        return .success
                    }
                    .responseJSON { response in
                        
                        // result of response serialization
                        print(response.data as Any)
                        
                        let convertedString = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                        print(convertedString as Any)
                        if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                            print("JSON: \(JSON)")
                            if let error = JSON["ERROR"] as? String {
                                self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                             //   MBProgressHUD.hide(for: self.view, animated: true);
                            }
                            else {
                                if let minCredsForUser = JSON ["MinCreds"] as? NSArray{
                                    
                                 //   self.minCreds = minCredsForUser
                                    let creds = JSON["CREDS"] as? NSNumber
                                    self.yourCredsLabel.text = "Your Creds: \(creds!)"
                                    
                                    let Rank = JSON["Rank"] as? NSNumber
                                    self.yourRankLabel.text = "Your Rank: \(Rank!)"
                                    
                                    let CurrentLevel = JSON["LEVEL"] 
                                    self.yourCurrentLevelLabel.text = "Current Level: \(CurrentLevel!)"
                                    
                                    let NextLevel = JSON["NEXTLEVEL"]
                                    self.yourNextLevelLabel.text = "Next Level: \(NextLevel!)"
                                    
                                    let pointToNextLevel = JSON["POINTSNEEDEDFORNEXTLEVEL"] as? NSNumber
                                    if (pointToNextLevel != nil) {
                                        self.credsToNextLevelLabel.text = "\(pointToNextLevel!) Creds to the Next level"
                                    }
                                  
                                }
                                
                            }
                            
                          //  self.getlistOfCredsOFFriends()
                            MBProgressHUD.hide(for: self.view, animated: true);
                        }
                            
                        else {
                            MBProgressHUD.hide(for: self.view, animated: true);
                           // print(response.result.error)
                            self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                            
                        }
                }
            }else {
                MBProgressHUD.hide(for: self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
        }
//     getlistOfCredsOFFriends()
        getlistOfCredsOFEveryone()
        
    }
    
    func getlistOfCredsOFEveryone() {
        
        
        let userId =  UserDefaults.standard.value(forKey: UserId) as! String
        
        let parameters : [String : String]  = [
            "userId": userId,
            "time": timeString,
            "location": locationString,
            ]
        if userId != ""
        {
            let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading"
                request(baseURL + getGameDetailsForEveryOne, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                        //print("Progress: \(progress.fractionCompleted)")
                    }
                    .validate { request, response, data in
                        // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                        return .success
                    }
                    .responseJSON { response in
                        
                        
                        // result of response serialization
                        
                        if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                            
                            print("JSON: \(JSON)")
                            if let error = JSON["ERROR"] as? String {
                                
                                 MBProgressHUD.hide(for: self.view, animated: true);
                                self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                self.scoreBoardCollectionView.isHidden = true
                                
                            }
                            else {
                                if let userCreds = JSON ["USERSCREDS"] as? NSArray{
                                    
                                    let dataHolder : NSMutableArray = []
                                    for i in 0 ..< userCreds.count {
                                        let tempDic = userCreds .object(at: i) as! [String : AnyObject]
                                        let gameDetail = GameDetail()
                                        let change = tempDic["CHANGE"] as? NSNumber
                                        gameDetail.change = "\(change!)"
                                        let position = tempDic["POSITION"] as? NSNumber
                                        gameDetail.position = "\(position!)"
                                        let creds = tempDic["CREDS"] as? NSNumber
                                        gameDetail.creds = "\(creds!)"
                                        // gameDetail.name = (tempDic["NAME"] as? String!)!
                                        let username = (tempDic["NAME"] as? String)!
                                        if username != nil {
                                            gameDetail.name = "\(username)"
                                        }
                                        
                                        dataHolder.add(gameDetail)
                                    }
                                    self.gameDetailsArray = dataHolder
                                    self.scoreBoardCollectionView.isHidden = false
                                    self.scoreBoardCollectionView.reloadData()
                                    
                                }
                                
                            }
                            MBProgressHUD.hide(for: self.view, animated: true);
                        }
                            
                        else {
                            MBProgressHUD.hide(for: self.view, animated: true);
                          //  print(response.result.error)
                            self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                        }
                }
            }else {
                MBProgressHUD.hide(for: self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
        }
        //        let req = request(.GET, baseURL + getGameDetailsForUser, parameters: parameters, encoding :.JSON)
        //                debugPrint(req)
        MBProgressHUD.hide(for: self.view, animated: true);
    }
    
    func getlistOfCredsOFFriends() {
        
        
        let userId =  UserDefaults.standard.value(forKey: UserId) as! String
        
        let parameters : [String : String]  = [
            "userId": userId,
            "time": "alltime",
            "location": "country",
            ]
        if userId != ""
        {
            let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
//                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
//                loadingNotification.mode = MBProgressHUDMode.indeterminate
//                loadingNotification.label.text = "Loading"
                
                request(baseURL + getGameDetailsForFriends, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                        //print("Progress: \(progress.fractionCompleted)")
                    }
                    .validate { request, response, data in
                        // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                        return .success
                    }
                    .responseJSON { response in
                        
                        
                        // result of response serialization
                        
                        if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                            
                            print("JSON: \(JSON)")
                            if let error = JSON["ERROR"] as? String {
                                MBProgressHUD.hide(for: self.view, animated: true);
                                self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                             self.scoreBoardCollectionView.isHidden = true
                               
                            }
                            else {
                                if let userCreds = JSON ["TOPCREDUSERS"] as? NSArray{
                                    
                                    let dataHolder : NSMutableArray = []
                                    for i in 0 ..< userCreds.count {
                                        let tempDic = userCreds .object(at: i) as! [String : AnyObject]
                                        let gameDetail = GameDetail()
                                        let change = tempDic["CHANGE"] as? NSNumber
                                        gameDetail.change = "\(change!)"
                                        let position = tempDic["POSITION"] as? NSNumber
                                        gameDetail.position = "\(position!)"
                                        let creds = tempDic["CREDS"] as? NSNumber
                                        gameDetail.creds = "\(creds!)"
                                        gameDetail.name = (tempDic["NAME"] as? String)!
                                        
                                        dataHolder.add(gameDetail)
                                    }
                                    self.gameDetailsArray = dataHolder
                                    self.scoreBoardCollectionView.isHidden = false
                                    self.scoreBoardCollectionView .reloadData()
                                    
                                }
                                
                            }
                            MBProgressHUD.hide(for: self.view, animated: true);
                        }
                            
                        else {
                            MBProgressHUD.hide(for: self.view, animated: true);
                            print(response.result.error!)
                            self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                        }
                }
            }else {
                MBProgressHUD.hide(for: self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
        }
        //        let req = request(.GET, baseURL + getGameDetailsForUser, parameters: parameters, encoding :.JSON)
        //                debugPrint(req)
        
    }
    
    
    func numberOfSectionsInCollectionView(_ collectionView: UICollectionView) -> Int {
        if (gameDetailsArray != nil) {
            return self.gameDetailsArray.count + 1
        }
        else {
            return 1
        }//row number
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3 //each row have 15 columns
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        cell.layer.cornerRadius = 0
        cell.layer.masksToBounds = true
        cell.layer.borderWidth = 0.5
        cell.layer.borderColor = blueColorr.cgColor
        let label = cell.viewWithTag(101) as? UILabel
       // label?.text = "Shruti"
        if (indexPath as NSIndexPath).section == 0 {
            cell.backgroundColor = blueColorr
            label?.textColor = UIColor.white
            if (indexPath as NSIndexPath).row == 0 {
                label?.text = "Rank"
            } else if (indexPath as NSIndexPath).row == 1 {
                label?.text = "Name"
            }else if (indexPath as NSIndexPath).row == 2 {
                label?.text = "Cred"
            }
            
        } else  {
            cell.layer.cornerRadius = 0
            cell.layer.masksToBounds = true
            cell.layer.borderWidth = 0.5
            cell.layer.borderColor = blueColorr.cgColor
            cell.backgroundColor = UIColor.clear
            label?.textColor = UIColor.white
            let gameDetail = self.gameDetailsArray.object(at: (indexPath as NSIndexPath).section - 1) as! GameDetail
            if (indexPath as NSIndexPath).row == 0 {
                label?.text = gameDetail.position
            } else if (indexPath as NSIndexPath).row == 1 {
                label?.text = gameDetail.name
            }else if (indexPath as NSIndexPath).row == 2 {
                label?.text = gameDetail.creds
            }
            
//            else if (indexPath as NSIndexPath).row == 3 {
//                if gameDetail.change == "0"{
//                    label?.text = "-"
//                }
//                else {
//                    label?.text = gameDetail.change
//                }
//            }
            
        }
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.scoreBoardCollectionView.frame.size.width/3, height: 40)
    }
    

}
