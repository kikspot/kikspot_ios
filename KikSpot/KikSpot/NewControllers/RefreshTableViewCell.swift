//
//  RefreshTableViewCell.swift
//  KikSpot
//
//  Created by knsmac003 on 1/21/19.
//  Copyright © 2019 Shruti Mittal. All rights reserved.
//

import UIKit

class RefreshTableViewCell: UITableViewCell {

    @IBOutlet weak var progressBar: UIActivityIndicatorView!
    @IBOutlet weak var progressLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func startStopLoading(_ isStart : Bool)
    {
        if(isStart)
        {
            progressBar.startAnimating()
            progressLabel.text = "Fetching Data"
        }
        else
        {
            progressBar.stopAnimating()
            progressLabel.text = "Push for more Locations..."
        }
    }

}
