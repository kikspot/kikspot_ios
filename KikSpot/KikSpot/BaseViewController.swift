//
//  BaseViewController.swift
//  KikSpot
//
//  Created by Shruti Mittal on 29/10/15.
//  Copyright © 2015 Shruti Mittal. All rights reserved.
//

import Foundation
import UIKit


class BaseViewController: UIViewController {
    
    var newBackButton: UIBarButtonItem!
    
    override func viewDidLoad () {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true
         newBackButton = UIBarButtonItem(title: "< Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(BaseViewController.back(_:)))
        self.navigationItem.leftBarButtonItem = newBackButton;
    }
    
    func back(_ sender: UIBarButtonItem) {
        // Perform your custom actions
        // ...
        // Go back to the previous ViewController
        self.navigationController?.popViewController(animated: true)
    }

    
    
}
