//
//  ViewController.swift
//  KikSpot
//
//  Created by Shruti Mittal on 27/10/15.
//  Copyright © 2015 Shruti Mittal. All rights reserved.
//

//import Alamofire
import UIKit
import MBProgressHUD
import TwitterKit
//import SwiftyJSON

class LoginViewController: UIViewController,FBSDKLoginButtonDelegate{
    
    @IBOutlet var userNameTextField: UITextField!
    @IBOutlet var passWordTextField: UITextField!
    @IBOutlet var facebook_button : UIButton!
    @IBOutlet var scrollView: UIScrollView!
    var activeField: UITextField?
 //   let rightSideBar = RightSideBarViewController()
    //  @IBOutlet weak var facebookLogin_Button: FBSDKLoginButton!
    var emailText : String = ""
    var  TwitterprofilePicURL:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.barTintColor = defaultNavBarColor
        navigationBarAppearace.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        //  #if DEBUG
        // userNameTextField.text = "shruti@knstek.com"
        //  passWordTextField.text = "shruti"
        //  #endif
        removeNavigationBarItem()
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.userNameTextField.frame.height))
        userNameTextField.leftView = paddingView
        userNameTextField.leftViewMode = UITextFieldViewMode.always
        userNameTextField .setValue(defaultColor, forKeyPath: "_placeholderLabel.textColor")
        let paddingView1 = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.passWordTextField.frame.height))
        passWordTextField.leftViewMode = UITextFieldViewMode.always
        passWordTextField.leftView = paddingView1
        passWordTextField .setValue(defaultColor, forKeyPath: "_placeholderLabel.textColor")
    }
    
    func registerForKeyboardNotifications()
    {
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWasShown(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillBeHidden(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillBeHidden(_ notification: Notification)
    {
        self.scrollView.contentInset = UIEdgeInsets.zero
        self.scrollView.scrollIndicatorInsets = UIEdgeInsets.zero
        self.activeField?.resignFirstResponder()
        //        self.scrollView.setContentOffset(CGPoint(x: 0, y: -64), animated: true)
        self.scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    
    
    func keyboardWasShown(_ notification: Notification)
    {
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollView.isScrollEnabled = true
        let info : NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if (activeField != nil)
        {
            if (!aRect.contains(activeField!.frame.origin))
                //                if (!CGRectContainsPoint(aRect, CGPointMake(activeField!.frame.origin.x, activeField!.frame.origin.x + 64)))
            {
                self.scrollView.scrollRectToVisible(activeField!.frame, animated: true)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        removeNavigationBarItem()
        registerForKeyboardNotifications()
        self.navigationController?.isNavigationBarHidden = true
        let userDefaults : UserDefaults = UserDefaults.standard
        var userId :String = ""
        if (userDefaults.value(forKey: UserId) != nil) {
            userId =  UserDefaults.standard.value(forKey: UserId) as! String
        }
        
        if userId != "" {
            let userDefaults : UserDefaults = UserDefaults.standard
            var isUserLimitedLook :String = ""
            if (userDefaults.value(forKey: isUserALimitedLook) != nil) {
                isUserLimitedLook =  userDefaults.value(forKey: isUserALimitedLook) as! String
            }
            if isUserLimitedLook != "YES" {
                let tabbar = self.storyboard?.instantiateViewController(withIdentifier: "tabbarcontroller") as!  TabBarViewController
                self.navigationController?.pushViewController(tabbar, animated: true)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        getShareUrl()
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getShareUrl() {
        let userDefaults : UserDefaults = UserDefaults.standard
        var userId :String = ""
        if (userDefaults.object(forKey: UserId) != nil) {
            userId =  UserDefaults.standard.value(forKey: UserId) as! String
        }
        // let userId =  NSUserDefaults.standardUserDefaults().valueForKey(UserId) as! String
        let parameters : [String : String]  = [
            "userId": userId,
            ]
        if userId != ""
        {
            let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                //                let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
                //                loadingNotification.mode = MBProgressHUDMode.Indeterminate
                //                loadingNotification.label.text = "Loading"
                request(baseURL + getShareURLservice, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                        //print("Progress: \(progress.fractionCompleted)")
                    }
                    .validate { request, response, data in
                        // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                        return .success
                    }
                    .responseJSON { response in
                        if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                            
                            print("JSON: \(JSON)")
                            if let error = JSON["ERROR"] as? String {
                                // self.presentViewController(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                print(error)
                            }
                            else {
                                if  let url = JSON ["URL"] as? String {
                                    UserDefaults.standard.set("\(url)" , forKey: urlToShare)
                                }
                                if let message = JSON ["message"] as? String {
                                    UserDefaults.standard.set("\(message)" , forKey: messageToShare)
                                }
                                if let referralCode = JSON ["referralCode"] as? String {
                                    UserDefaults.standard.set("\(referralCode)" , forKey: userreferralCode)
                                }
                            }
                            // MBProgressHUD.hideForView(self.view, animated: true);
                        }
                        else {
                            //  MBProgressHUD.hideForView(self.view, animated: true);
                            print(response.result.error)
                            //                            self.presentViewController(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                        }
                }
            }else {
                //   MBProgressHUD.hideForView(self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
        }
    }
    
    
    
    @IBAction func signIn_button_pressed (_ sender: UIButton) {
        
        var username : String = self.userNameTextField.text!;
        var password : String = self.passWordTextField.text!;
        
        //        var username : String = "harithakns"
        //        var password : String = "welcome123"
        //  username = "harithakns"
        //  password = "welcome123"
        
        username = username.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        password = password.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let userDefaults : UserDefaults = UserDefaults.standard
        var deviceToken :String = ""
        if (userDefaults.value(forKey: userDeviceToken) != nil) {
            deviceToken =  UserDefaults.standard.value(forKey: userDeviceToken) as! String
        }
        
        let parameters : [String : String]  = [
            "username": username,
            "password": password,
            "deviceToken": deviceToken
        ]
        print(parameters)
        if username == "" || password == ""
        {
            self.present(Common.showAlertWithTitle("Kikspot", message: "Please fill all the fields", cancelButton: "OK"), animated: true, completion: nil)
        }
        else {
            let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading"
                request(baseURL + login, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                        //print("Progress: \(progress.fractionCompleted)")
                    }
                    .validate { request, response, data in
                        // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                        return .success
                    }
                    .responseJSON { response in
                        
                        
                        // result of response serialization
                        
                        if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                            print("JSON: \(JSON)")
                            
                            
                            if let status = JSON["STATUS"] as? String {
                                
                                UserDefaults.standard.removeObject(forKey: FbProfilePicUrl)
                                
                                if (status == "Success") {
                                    
                                    
                                    if let user = JSON["UserId"] as? NSNumber {
                                        // let user = userId as! NSNumber
                                        UserDefaults.standard.set("\(user)" , forKey: UserId)
                                        
                                    }
                                    if let useremail = JSON["Email"] as? String{
                                        UserDefaults.standard.set("\(useremail)" , forKey: UserEmail)
                                    }
                                    UserDefaults.standard.set("\(username)" , forKey: savedUserName)
                                    UserDefaults.standard.set("NO" , forKey: isUserALimitedLook)
                                    
//                                    let Alert = UIAlertController(title: "Kikspot", message: "You have successfully logged in", preferredStyle: UIAlertControllerStyle.alert)
//
//                                    Alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
//                                        print("Handle Ok logic here")
//                                        let tabbar = self.storyboard?.instantiateViewController(withIdentifier: "tabbarcontroller") as!  TabBarViewController
//                                        self.navigationController?.pushViewController(tabbar, animated: true)
//
//                                    }))
//
//                                    self.present(Alert, animated: true, completion: nil)
                                    let tabbar = self.storyboard?.instantiateViewController(withIdentifier: "tabbarcontroller") as!  TabBarViewController
                                    self.navigationController?.pushViewController(tabbar, animated: true)
                                                                        self.present(Common.showAlertWithTitle("Kikspot", message: "You have successfully logged in", cancelButton: "OK"), animated: true, completion: nil)
                                    
                                    
                                    
                                }
                            }
                            else {
                                if let error = JSON["ERROR"] as? String {
                                    // let error = stats as! String
                                    self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                }
                            }
                            MBProgressHUD.hide(for: self.view, animated: true);
                        }
                        else {
                            MBProgressHUD.hide(for: self.view, animated: true);
                            print(response.result.error!)
                            self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                        }
                }
            }else {
                MBProgressHUD.hide(for: self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
        }
        //        let req = request(.GET, baseURL + login, parameters: parameters, encoding :.JSON)
        //        debugPrint(req)
    }
    
    @IBAction func limitedLook_button_pressed (_ sender: UIButton) {
        
        let registerSecondPage = self.storyboard?.instantiateViewController(withIdentifier: "RegistrationViewController") as! RegistrationViewController
               self.navigationController?.pushViewController(registerSecondPage, animated: true)
        
        
       /* let userDefaults : UserDefaults = UserDefaults.standard
        var deviceToken :String = ""
        if (userDefaults.value(forKey: userDeviceToken) != nil) {
            deviceToken =  UserDefaults.standard.value(forKey: userDeviceToken) as! String
        }
        
        let parameters : [String : String]  = [
            "deviceToken": deviceToken
        ]
        
        print("@@@@@@@@@@@@@ \(deviceToken)")
        
        let reachability: Reachability = Reachability.forInternetConnection()
        if  reachability.currentReachabilityStatus().rawValue != 0  {
            
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = MBProgressHUDMode.indeterminate
            loadingNotification.label.text = "Loading"
            
            request(baseURL + limitedLook, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                .downloadProgress(queue: DispatchQueue.utility) { progress in
                    //print("Progress: \(progress.fractionCompleted)")
                }
                .validate { request, response, data in
                    // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                    return .success
                }
                .responseJSON { response in
            
                    
                      if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                        print("JSON: \(JSON)")
                        
                        if let status = JSON["STATUS"] as? String {
                            if (status == "Success") {
                                if let user = JSON["UserId"] as? NSNumber {
                                    // let user = userId as! NSNumber
                                    UserDefaults.standard.set("\(user)" , forKey: UserId)
                                }
                                UserDefaults.standard.set("Guest" , forKey: savedUserName)
                                UserDefaults.standard.set("YES" , forKey: isUserALimitedLook)
                                if (self.rightSideBar.menu_tableView != nil) {
                                    self.rightSideBar.menu_tableView.reloadData()
                                }
                                //                                let recomendViewController = self.storyboard?.instantiateViewControllerWithIdentifier("Recommend") as! RecommendationViewController
                                //                                self.navigationController?.pushViewController(recomendViewController, animated: true)
//                                let dashBoardViewController = self.storyboard?.instantiateViewControllerWithIdentifier("Dashboard") as! DashboardViewController
//                                self.navigationController?.pushViewController(dashBoardViewController, animated: true)
                                let recommend = self.storyboard?.instantiateViewController(withIdentifier: "Recommend") as! RecommendationViewController
                                self.navigationController?.pushViewController(recommend, animated: true)
                            }
                            
                        }
                        else {
                            if let error = JSON["ERROR"] as? String {
                                // let error = stats as! String
                                self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                            }
                        }
                        MBProgressHUD.hide(for: self.view, animated: true);
                    }
                    else {
                        MBProgressHUD.hide(for: self.view, animated: true);
                        print(response.result.error)
                        self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                    }
                    MBProgressHUD.hide(for: self.view, animated: true);
            }
        }else {
            MBProgressHUD.hide(for: self.view, animated: true);
            self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
        }*/
        
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        print("User Logged In")
        
        if ((error) != nil)
        {
            // Process error
        }
        else if result.isCancelled {
            // Handle cancellations
        }
        else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            if result.grantedPermissions.contains("email")
            {
                // Do work
                self .returnUserData()
            }
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("User Logged Out")
    }
    
    func returnUserData()
    {
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: nil)
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            
            if ((error) != nil)
            {
                // Process error
                print("Error: \(error!)")
            }
            else
            {
              //  guard let resultNew = result as? [String:Any]
             // /
               // let userEmail = resultNew["email"]  as! String
               // let userName = resultNew["name"]  as! String
//                print("fetched user: \(result)")
//                let userName : NSString = result.value(forKey: "name") as! NSString
//                print("User Name is: \(userName)")
//                let userEmail : NSString = (result.value(forKey: "email") as! NSString)
//                print("User Email is: \(userEmail)")
            }
        })
    }
    
    
    @IBOutlet var TwitterButton: UIButton!
    
    @IBAction func twitterBtnCiked(_ sender: AnyObject)
    {
        
        
        TWTRTwitter.sharedInstance().logIn { (session, error) in
            if session != nil {
                print("signed in as \(session!.userName)");
                let client = TWTRAPIClient.withCurrentUser()
                let request = client.urlRequest(withMethod: "GET",
                                                urlString: "https://api.twitter.com/1.1/account/verify_credentials.json?include_email=true",
                                                parameters: ["include_email": "true", "skip_status": "true"],
                                                error: nil)
                client.sendTwitterRequest(request) { response, data, connectionError in
                    if (connectionError == nil) {
                        
                                                    do{
                                                        let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any]
                                                        print("Json response: ", json)
                                                        let Name = json["name"] as? String
                                                        let lastName  = json["screen_name"] as? String
                                                        let email  = json["email"] as? String
                                                        print("First name: ",Name!)
                                                        print("Last name: ",lastName!)
                                                        print("Email: ",email!)
                                                        self.userNameTextField.text = email
                                                        self.passWordTextField.text = Name
                                                        
                                                        //retrieving user's profile Image
                                                        client.loadUser(withID: (session!.userID), completion: { (user,error) in
                                                        
                                                            let imageUrl = user?.profileImageURL
                                                            let url =  URL(string: imageUrl!)
                                                            _ = try?Data (contentsOf:url!)
                                                            
                                                        
                                                        })
                                                     
                                                        
                                                        self.sendTwitterDetailsToServer((json as? NSDictionary)!)

                                                        
                                                        
                                                        
                                                       } catch {
                            
                                                                }
                        
                                                }else if(connectionError != nil)  {
                                                        print("Error: \(connectionError!)")
                    }else {
                        // Authorization has been canceled by user
                        MBProgressHUD.hide(for: self.view, animated: true);
                        print("response is cancelled")
                        self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
                        
                    }
                                            }
                
                
                            } else {
                                        MBProgressHUD.hide(for: self.view, animated: true);
                
                                        self.present(Common.showAlertWithTitle("Kikspot", message: "Twitter Login Failed", cancelButton: "OK"), animated: true, completion: nil)
                
                                        NSLog("Login error: %@", error!.localizedDescription);
                                   }
                            }
        
        
    }
    
    @IBAction func facebook_button_pressed(_ sender: AnyObject) {
        
//        let FBLoginManager = FBSDKLoginManager()
//        if FBSDKAccessToken.current() != nil {
//            //For debugging, when we want to ensure that facebook login always happens
//          //  FBSDKLoginManager().logOut()
//            //Otherwise do:
//            // return
//            self.returnUserData()
//        }
//        else {
        let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Loading"

        let fbLoginManager = FBSDKLoginManager();
        fbLoginManager.loginBehavior = FBSDKLoginBehavior.systemAccount
        fbLoginManager.logIn(withReadPermissions: ["public_profile","email"], from: self) {
//            ,"user_friends","read_custom_friendlists"
            (loginResult: FBSDKLoginManagerLoginResult?, error: Error?) in
            //  fbLoginManager.logInWithPublishPermissions(["email"], fromViewController: self, handler: { (response:FBSDKLoginManagerLoginResult!, error: NSError!) in
            if(error != nil){
                // Handle error
            }
            else if(loginResult?.isCancelled)!{
                // Authorization has been canceled by user
                MBProgressHUD.hide(for: self.view, animated: true);
                print("response is cancelled")
                self.present(Common.showAlertWithTitle("Kikspot", message: "Facebook Login Failed", cancelButton: "OK"), animated: true, completion: nil)
            }
            else {
                // Authorization successful
                // println(FBSDKAccessToken.currentAccessToken())
                // no longer necessary as the token is already in the response
                print(loginResult!.token.tokenString!)
//                if ((loginResult?.grantedPermissions.contains("user_friends"))!){
//                    self.getFBUserData()
//                    fbLoginManager.logOut()
//                }
                if (loginResult?.grantedPermissions.contains("email"))!
                {
                    // self .returnUserData()
                    self.getFBUserData()
                    fbLoginManager.logOut()
                }
                else {
                    self.present(Common.showAlertWithTitle("Kikspot", message: "Sorry! We are not able to fetch email. Please register", cancelButton: "OK"), animated: true, completion: nil)
                }
            }
        }
        //}
    }
    
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    print(result!)
                    self.sendFbDetailsToServer((result as? NSDictionary)!)
                }
                else {
                    print(error!)
                }
            })
            
//            let request = FBSDKGraphRequest(graphPath: "/{user-id}/friendlists", parameters:["fields": ""], httpMethod: "GET")
//            request!.start(completionHandler: { connection, result, error in
//                // Handle the result
//                print(result)
//            })

//            let request = FBSDKGraphRequest(graphPath: "me/friends", parameters:["fields": ""], httpMethod: "GET")
//            request!.start(completionHandler: { connection, result, error in
//                // Handle the result
//                print(result)
//            })
        }
    }
    
    
    func sendFbDetailsToServer( _ details: NSDictionary) {
        
        var deviceToken :String = ""
        let userDefaults : UserDefaults = UserDefaults.standard
        
        if (userDefaults.value(forKey: userDeviceToken) != nil) {
            deviceToken =  UserDefaults.standard.value(forKey: userDeviceToken) as! String
        }
        
        let email =  details["email"] as? String
        let firstName =  details["first_name"] as? String
        let lastName =  details["last_name"] as? String
        //let profilePicURL = details["picture.type(large)"] as? String
        
        let userID = details["id"] as? String
       let profilePicURL = "http://graph.facebook.com/\(userID!)/picture?type=large"
        
        let parameters : [String : String]  = [
            "emailId": "\(email!)" ,
            "firstName": "\(firstName!)",
            "lastName": "\(lastName!)",
            "deviceToken": deviceToken
        ]
        print(parameters)
        let reachability: Reachability = Reachability.forInternetConnection()
        if  reachability.currentReachabilityStatus().rawValue != 0  {
            
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = MBProgressHUDMode.indeterminate
            loadingNotification.label.text = "Loading"
            
            request(baseURL + socialLoginURL, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                .downloadProgress(queue: DispatchQueue.utility) { progress in
                    //print("Progress: \(progress.fractionCompleted)")
                }
                .validate { request, response, data in
                    // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                    return .success
                }
                .responseJSON { response in
                     if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                        print("JSON: \(JSON)")
                        if let status = JSON["STATUS"] as? String {
                            
                            if (status == "Success") {
                                if let user = JSON["UserId"] as? NSNumber {
                                    // let user = userId as! NSNumber
                                    UserDefaults.standard.set("\(user)" , forKey: UserId)
                                }
                                UserDefaults.standard.set("NO" , forKey: isUserALimitedLook)
                                
                                UserDefaults.standard.set("\(profilePicURL)" , forKey: FbProfilePicUrl)
                                UserDefaults.standard.set("\(email!)" , forKey: savedUserName)

                                let firstLogin = JSON["FirstLogin"] as? NSNumber
                                let firstLoginString = "\(firstLogin!)"
                                if firstLoginString == "0" {

                                       let recommend = self.storyboard?.instantiateViewController(withIdentifier: "tabbarcontroller") as! TabBarViewController
                                    
                                    
                                    self.navigationController?.pushViewController(recommend, animated: true)
                                    self.present(Common.showAlertWithTitle("Kikspot", message: "You have successfully logged in", cancelButton: "OK"), animated: true, completion: nil)
                                }
                                else {
                                    
                                    let registerSecondPage = self.storyboard?.instantiateViewController(withIdentifier: "RegisterSecondPage") as! RegistrationSecondPage
                                    registerSecondPage.parentController = "SocialLogin"
                                    self.navigationController?.pushViewController(registerSecondPage, animated: true)
                                }
                                
                            }
                        }
                        else {
                            if let error = JSON["ERROR"] as? String {
                                // let error = stats as! String
                                self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                            }
                        }
                        MBProgressHUD.hide(for: self.view, animated: true);
                    }
                    else {
                        MBProgressHUD.hide(for: self.view, animated: true);
                        print(response.result.error!)
                        self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                    }
                    MBProgressHUD.hide(for: self.view, animated: true);
            }
        }else {
            MBProgressHUD.hide(for: self.view, animated: true);
            self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
        }
        
        //        let req = request(.GET, baseURL + socialLoginURL, parameters: parameters, encoding :.JSON)
        //        debugPrint(req)
        
    }
    
    
    
    
    
    func sendTwitterDetailsToServer( _ details: NSDictionary) {
        
        var deviceToken :String = ""
        let userDefaults : UserDefaults = UserDefaults.standard
        
        if (userDefaults.value(forKey: userDeviceToken) != nil) {
            deviceToken =  UserDefaults.standard.value(forKey: userDeviceToken) as! String
        }
        
        let email =  details["email"] as? String
        let firstName =  details["name"] as? String
        let lastName =  details["screen_name"] as? String
        self.TwitterprofilePicURL = (details["profile_image_url"] as? String)!
        
    //    let userID = details["id_str"] as? String
        
        
        
        
        TWTRTwitter.sharedInstance().logIn { (session, error) in
            if session != nil {

        
                
        let client = TWTRAPIClient.withCurrentUser()
        client.loadUser(withID: (session!.userID), completion: { (user,error) in
            
            let imageUrl = user?.profileImageURL
         //   let url =  URL(string: imageUrl!)
            //let data = try?Data (contentsOf:url!)
            self.TwitterprofilePicURL = imageUrl!
            
        })
            }
        }
        
        
        let parameters : [String : String]  = [
            "emailId": "\(email!)" ,
            "firstName": "\(firstName!)",
            "lastName": "\(lastName!)",
            "deviceToken": deviceToken
        ]
        print(parameters)
        let reachability: Reachability = Reachability.forInternetConnection()
        if  reachability.currentReachabilityStatus().rawValue != 0  {
            
            let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
            loadingNotification.mode = MBProgressHUDMode.indeterminate
            loadingNotification.label.text = "Loading"
            
            request(baseURL + socialLoginURL, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                .downloadProgress(queue: DispatchQueue.utility) { progress in
                    //print("Progress: \(progress.fractionCompleted)")
                }
                .validate { request, response, data in
                    // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                    return .success
                }
                .responseJSON { response in
                    if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                        print("JSON: \(JSON)")
                        if let status = JSON["STATUS"] as? String {
                            
                            if (status == "Success") {
                                if let user = JSON["UserId"] as? NSNumber {
                                    // let user = userId as! NSNumber
                                    UserDefaults.standard.set("\(user)" , forKey: UserId)
                                }
                                UserDefaults.standard.set("NO" , forKey: isUserALimitedLook)
                                
                                UserDefaults.standard.set("\(self.TwitterprofilePicURL)" , forKey: TweetProfilePicUrl)
                                UserDefaults.standard.set("\(email!)" , forKey: savedUserName)
//                                if (self.rightSideBar.menu_tableView != nil) {
//                                    self.rightSideBar.menu_tableView.reloadData()
//                                }
                                let firstLogin = JSON["FirstLogin"] as? NSNumber
                                let firstLoginString = "\(firstLogin!)"
                                if firstLoginString == "0" {
                                    //                                    let dashBoardViewController = self.storyboard?.instantiateViewControllerWithIdentifier("Dashboard") as! DashboardViewController
                                    //                                    self.navigationController?.pushViewController(dashBoardViewController, animated: true)
                                    let recommend = self.storyboard?.instantiateViewController(withIdentifier: "tabbarcontroller") as! TabBarViewController
                                    self.navigationController?.pushViewController(recommend, animated: true)
                                    self.present(Common.showAlertWithTitle("Kikspot", message: "You have successfully logged in", cancelButton: "OK"), animated: true, completion: nil)
                                    
                                    
                                    // move to tab bar controller find my spot
                                }
                                else {
                                    
                                    let registerSecondPage = self.storyboard?.instantiateViewController(withIdentifier: "RegisterSecondPage") as! RegistrationSecondPage
                                    registerSecondPage.parentController = "SocialLogin"
                                    self.navigationController?.pushViewController(registerSecondPage, animated: true)
                                }
                                
                            }
                        }
                        else {
                            if let error = JSON["ERROR"] as? String {
                                // let error = stats as! String
                                self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                            }
                        }
                        MBProgressHUD.hide(for: self.view, animated: true);
                    }
                    else {
                        MBProgressHUD.hide(for: self.view, animated: true);
                        print(response.result.error!)
                        self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                    }
                    MBProgressHUD.hide(for: self.view, animated: true);
            }
        }else {
            MBProgressHUD.hide(for: self.view, animated: true);
            self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
        }
        
        //        let req = request(.GET, baseURL + socialLoginURL, parameters: parameters, encoding :.JSON)
        //        debugPrint(req)
        
    }

    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField)
        
    {
        
        activeField = textField
        
        // registerForKeyboardNotifications
        
    }
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField)
        
    {
        
        activeField = nil
        
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    @IBAction func forgotPasswordPressed(_ sender: AnyObject) {
        
        print("forgot tapped")
        DisplayAlertForForgotPassword()
    }
    
    // function to display the alert view
    func DisplayAlertForForgotPassword()
        
    {
        
        
        
        // alert heading
        
        let alert = UIAlertController(title:"Kikspot", message:"Enter Email Address", preferredStyle: UIAlertControllerStyle.alert)
        
        
        
        // alert cancel button
        
        let cancel = UIAlertAction(title:"Cancel", style: UIAlertActionStyle.cancel, handler: nil)
        
        alert.addAction(cancel)
        
        
        
        // alert ok/send button where we are sending the email to the server
        
        let ok = UIAlertAction(title:"Send", style: UIAlertActionStyle.default , handler:{(action) -> Void in
            
            
            
            if let textFields = alert.textFields{
                
                let theTextFields = textFields as [UITextField]
                
                let enteredText = theTextFields[0].text
                
                print("\(enteredText!)")
                
                self.emailText = enteredText! as String
                
                
                
                print("\(self.emailText)")
                
                self.sendEmailButton()
                
            }
            
        })
        
        
        
        alert.addAction(ok)
        
        // adding the textfield in the alert
        
        alert.addTextField { (textField) -> Void in
            
            textField.placeholder = "Enter email address"
            
            textField.keyboardType = UIKeyboardType.emailAddress
            
            
            // textField.backgroundColor = UIColor.clearColor()
            
        }
        
        self.present(alert , animated: true, completion: nil)
        
        
        
    }
    
    
    
    
    /*func DisplayAlertForForgotPassword()
    {
        
        // alert heading
        let alert = UIAlertController(title:"Kikspot", message:"Enter Email Address", preferredStyle: UIAlertControllerStyle.alert)
        
        // alert cancel button
        let cancel = UIAlertAction(title:"Cancel", style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(cancel)
        
        // alert ok/send button where we are sending the email to the server
        let ok = UIAlertAction(title:"Send", style: UIAlertActionStyle.default , handler:{(action) -> Void in
            
            if let textFields = alert.textFields{
                let theTextFields = textFields as [UITextField]
              let enteredText = theTextFields[0].text
                print("\(enteredText)")
                self.emailText = enteredText! as String
                
                print("\(self.emailText)")
                self.sendEmailButton()
            }
        })
        
        alert.addAction(ok)
        // adding the textfield in the alert
        alert.addTextField { (textField) -> Void in
            textField.placeholder = "Enter email address"
            textField.keyboardType = UIKeyboardType.emailAddress

            // textField.backgroundColor = UIColor.clearColor()
        }
        self.present(alert , animated: true, completion: nil)
        
    }*/
    
    // function to send the email address to the server
    func sendEmailButton() {
        
        var emailAddress : String = self.emailText
        
        emailAddress = emailAddress.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        
        let parameters : [String : String]  = [
            "email": emailAddress
            
        ]
        if emailAddress == ""
        {
            self.present(Common.showAlertWithTitle("Kikspot", message: "Please enter the email address", cancelButton: "OK"), animated: true, completion: nil)
        }
        else {
            let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading"
                
                request(baseURL + forgotpassword, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                        //print("Progress: \(progress.fractionCompleted)")
                    }
                    .validate { request, response, data in
                        // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                        return .success
                    }
                    .responseJSON { response in
              
                    // result of response serialization
                    
                    
                   if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                        print("JSON: \(JSON)")
                        if let status = JSON["STATUS"] as? String {
                            
                            if (status == "success") {
                                if let Email = JSON["email"] as? NSString {
                                    // let user = userId as! NSNumber
                                    //  NSUserDefaults.standardUserDefaults().setObject("\(user)" , forKey: UserId)
                                    
                                    if emailAddress == Email as String {
                                        
                                        print("\(emailAddress)")
                                        let ResetPasswordView = self.storyboard?.instantiateViewController(withIdentifier: "ResetPassword") as! ResetPasswordViewController
                                        self.navigationController?.pushViewController(ResetPasswordView, animated: true)
                                        ResetPasswordView.email = Email ;
                                        
                                    }
                                    print("email \(Email) ")
                                }
                                
                            }
                        }
                        else {
                            if let error = JSON["ERROR"] as? String {
                                // let error = stats as! String
                                self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                            }
                        }
                        MBProgressHUD.hide(for: self.view, animated: true);
                    }
                    else {
                        MBProgressHUD.hide(for: self.view, animated: true);
                        print(response.result.error!)
                        self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                    }
                    MBProgressHUD.hide(for: self.view, animated: true);
                }
            }else {
                MBProgressHUD.hide(for: self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
        }
        
    }
    
    
}

