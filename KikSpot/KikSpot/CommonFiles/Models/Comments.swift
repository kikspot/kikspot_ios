//
//  Comments.swift
//  KikSpot
//
//  Created by Shruti Mittal on 12/01/16.
//  Copyright © 2016 Shruti Mittal. All rights reserved.
//

import Foundation
import UIKit

class Comments: NSObject {
    
    
    var city: String = ""
    var comment: String = ""
    var firstName: String = ""
    var lastName: String = ""
    var rating: String = ""
    var state: String = ""
    var username: String = ""
    var date : String = ""
    
}