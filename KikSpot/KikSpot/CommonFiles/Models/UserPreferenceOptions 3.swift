//
//  UserPreferenceOptions.swift
//  KikSpot
//
//  Created by knsmac003 on 2/3/16.
//  Copyright © 2016 Shruti Mittal. All rights reserved.
//

import UIKit

class UserPreferenceOptions: NSObject {

    var isSelected: String = ""
    var option: String = ""
    var userPreferenceOptionId: String = ""
}
