//
//  Recommendation.swift
//  KikSpot
//
//  Created by Shruti Mittal on 15/12/15.
//  Copyright © 2015 Shruti Mittal. All rights reserved.
//

import Foundation
import UIKit

class Recommendation: NSObject {
    
    
    var distance: String = ""
    var address: String = ""
    var iconUrl: String = ""
    var latitude: String = ""
    var locationAPIId: String = ""
    var locationName: String = ""
    var longitude: String = ""
    var trending: String = ""
    var rating: String = ""
    var isNew: String = ""
    var hasActiveRules: String = ""
    var hasActiveRewards: String = ""
    var rates: String = ""
    var newlyAdded: String = ""
//    init(distance: String, iconUrl: String, latitude: String , locationAPIId: String, locationName: String, longitude: String) {
//        
//        self.distance = distance
//        self.iconUrl = iconUrl
//        self.latitude = latitude
//        self.locationAPIId = locationAPIId
//        self.locationName = locationName
//        self.longitude = longitude
//    }
    
}
