//
//  locationComments.swift
//  KikSpot
//
//  Created by knsmac003 on 4/20/18.
//  Copyright © 2018 Shruti Mittal. All rights reserved.
//

import UIKit

class locationComments: NSObject {
    
    
    var comment: String = ""
    var firstName: String = ""
    var ratedImage: String = ""
    var lastName: String = ""
    var givenRating: String = ""
    var ratingDate: String = ""
    var ratedUserName: String = ""

}
