//
//  Common.swift
//  KikSpot
//
//  Created by Shruti Mittal on 25/11/15.
//  Copyright © 2015 Shruti Mittal. All rights reserved.
//

import Foundation
import UIKit


//let baseURL:String = "http://34.231.172.103:8080/kikspot"
let baseURL:String = "https://api.kikspot.com/kikspot"
//"http://34.231.172.103:8080/kikspot";//http://54.225.230.190:7090/kikspot // http://192.168.0.13:8080/kikspot //https://api.kikspot.com/kikspot
//let baseURL:String = "http://192.168.1.103:7070/kikspot";
let login:String = "/mobile/login.do";
let RegisterStep1 = "/mobile/signup.do"
let RegisterStep2 = "/mobile/completesignup.do"
let limitedLook =  "/mobile/bypassuser.do"
let dashBoard = "/mobile/dashboard.do"
let RecommendationURL = "/mobile/getrecommendationlocations.do"
let removeRecommendationURL = "/mobile/removerecommendation.do"
let forgotpassword = "/mobile/forgotpassword.do"
let ResetPassword = "/mobile/resetpassword.do"
let EditProfile =    "/mobile/editprofile.do"
let ChangePassword = "/mobile/changepassword.do"
let socialLoginURL = "/mobile/sociallogin.do"
let ListNotifications = "/mobile/usernotifications.do"
let changeNotificationConfig = "/mobile/notificationconfig.do"
let ProfileDetails = "/mobile/profiledetails.do"
let uploadImage = "/mobile/saveprofileimage.do"
let getShareURLservice = "/mobile/shareapp.do"
let getUserRatingForALocation = "/mobile/getuserlocationrating.do"
let sendLocationRatingToServer = "/mobile/ratelocation.do"
let sendLocImageToServer = "/mobile/adduserlocationimages.do"
let getLocationComments = "/mobile/getlocationcomments.do"
let updateUserLocationToServer = "/mobile/saveuserlocation.do"
let sendFeedback = "/mobile/feedback.do"
let getNotificationsList = "/mobile/notifications.do"
let getRulesOfAParticularLocation = "/mobile/getgamerulesoflocation.do"
let getRulesOfAListOfLocation = "/mobile/getgamerulesoflocationslist.do"
let getRulesForAll = "/mobile/getgamerules.do"
let userPreferance = "/mobile/getuserpreferences.do"
let saveUserPreference = "/mobile/saveuserpreferences.do"
let getLocationDetails = "/mobile/getrecommendationlocationdetails.do"
let refreshRecommendationLocation = "/mobile/refreshlocations.do"
let deleteANotificationFromList = "/mobile/deletenotification.do"
let getGameDetailsForUser = "/mobile/getgamedetailsofuser.do"
let getGameDetailsForEveryOne = "/mobile/getgamedetailsofallusers.do"
let getGameDetailsForFriends = "/mobile/getfriendsgamedetails.do"
let getGameDetails = "/mobile/getgamedetails.do"
let getLevelInformation = "/mobile/getgameleveldetails.do"   
let sendShareStatus = "/mobile/updatecredsforsharing.do"
let getUnreadNotificationCount = "/mobile/getnumberofunreadnotifications.do"
let getLocationReviewsComments  =  "/mobile/getlocationcommentsandimages.do"

let defaultColor: UIColor = UIColor( red: CGFloat(0/255.0), green: CGFloat(110/255.0), blue: CGFloat(255/255.0), alpha: CGFloat(1.0) )
let blueColorr: UIColor =  UIColor( red: CGFloat(10/255.0), green: CGFloat(102/255.0), blue: CGFloat(161/255.0), alpha: CGFloat(1.0) )
let defaultNavBarColor: UIColor =  UIColor( red: CGFloat(21/255.0), green: CGFloat(47/255.0), blue: CGFloat(107/255.0), alpha: CGFloat(1.0) )


let internetNotAvailable = "Internet Connection is not Available"
let errorMeesage = "Unable to get response from server. Please try again later"
let UserId = "UserID"
let userDeviceToken = "deviceToken"
let FbProfilePicUrl = "FBProfilePicURL"
let TweetProfilePicUrl = "TWProfilePicURL"
let isUserALimitedLook = "LimitedLookUser"
let googleAPIKey = "AIzaSyAuGLC1myv0vqDxg84eZjdzIMW4DO2qWto"
let googleServerKey = "AIzaSyDbHBn7gdNrfV4B9CMOWfpCDswA9HbOuEA"
let savedUserName = "UserName"
let messageToShare = "ShareMessage"
let urlToShare = "ShareURL"
let currentLatitude = "currentLatitude"
let currentLongitude = "currentLongitude"
let userreferralCode = "referralCode"
let recommendationsArray = "arrayData"
let UserEmail = "EmailADdress"

let UserlocationRatingID = "userlocationRatingID"

let isRated = "isRated"

var recomendationsArray : NSMutableArray = []

open class Common: NSObject {
    
   class func showAlertWithTitle (_ title: String, message: String, cancelButton: String) -> UIAlertController{
    
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: cancelButton, style: UIAlertActionStyle.default, handler: nil))
        //self.presentViewController(alert, animated: true, completion: nil)
    return alert
    
    }
    
    class func showAlertWithTextfield (_ title: String, message: String, doneButton : String ,cancelButton: String) -> UIAlertController{
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        
        alert.addAction(UIAlertAction(title: doneButton , style: UIAlertActionStyle.default , handler:nil))
        
        
        alert.addAction(UIAlertAction(title: cancelButton, style: UIAlertActionStyle.default, handler: nil))
        alert.addTextField{(textfield : UITextField!) -> Void in
            textfield.placeholder = "Enter email address"
        }
        
        
        // self.presentViewController(alert, animated: true, completion: nil)
        return alert
        
    }
    
    class func ValidateEmailString (_ strEmail:NSString) -> Bool
    {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailText = NSPredicate(format:"SELF MATCHES [c]%@",emailRegex)
        return (emailText.evaluate(with: strEmail))
    }
    
    class func location_setting_popUp(_ animated: Bool) -> UIAlertController {
        let alertController = UIAlertController (title: "KikSpot", message: "Kikspot needs access to your location. Please turn on Location Services in your device settings.", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Go To Settings", style: .default) { (_) -> Void in
            let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
            if let url = settingsUrl {
                OperationQueue.main.addOperation({
                    UIApplication.shared.openURL(url)
                })
                //UIApplication.sharedApplication().openURL(url)
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(settingsAction)
        alertController.addAction(cancelAction)
        
        return alertController
    }
    


}
