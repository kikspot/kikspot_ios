//
//  VenueDetailsTableViewCell.swift
//  KikSpot
//
//  Created by Apple on 03/04/18.
//  Copyright © 2018 Shruti Mittal. All rights reserved.
//

import UIKit

class VenueDetailsTableViewCell: UITableViewCell {

  
    @IBOutlet weak var commentslabel: UILabel!
    @IBOutlet weak var imageviewHeightConstraints: NSLayoutConstraint!
    @IBOutlet weak var rating_comments: UITextView!
    @IBOutlet weak var ratingImageView: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
