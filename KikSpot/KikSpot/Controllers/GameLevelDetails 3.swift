//
//  GameLevelDetails.swift
//  KikSpot
//
//  Created by Shruti Mittal on 17/03/16.
//  Copyright © 2016 Shruti Mittal. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
class GameLevelDetails: BaseViewController
{
    var selectedSegment: String = "Creds"
    @IBOutlet var segmentController: UISegmentedControl!
    
    @IBAction func segmentSelected(_ sender: Any) {
            print("selected index: \((sender as AnyObject).selectedSegmentIndex)")
            switch segmentController.selectedSegmentIndex
            {
            case 0:
                
                selectedSegment = "Creds"
               getGameDetailsInfo()
                break
                
            case 1:
                
                 selectedSegment = "Game"
                getGameLevelsInfo()
                break
                
            default:
                
                break
                
            }
    }
    
    
    let boldFont = UIFont(name: "Helvetica-Bold", size: 16.0)
    @IBOutlet var level_tableView:UITableView!
    var level_array: NSArray!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.setNavigationBarItem()
        self.navigationItem.title = "How to Earn Cred"
        segmentController.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.white], for: UIControlState.selected)
        
        self.navigationItem.rightBarButtonItem = nil
        self.navigationItem.setHidesBackButton(true, animated: false)
        
        let homeButton : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "Back.png"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(RewardsViewController.goback(_:)))
         self.navigationItem.leftBarButtonItem = homeButton
        getGameDetailsInfo()
    }
    func goback(_ sender: UIBarButtonItem) {
        // Perform your custom actions
        // ...
        // Go  to the previous ViewController
        
        self.navigationController?.popViewController(animated: true)
        
        // let gamelevel = self.storyboard?.instantiateViewController(withIdentifier: "tabbarcontroller") as!  TabBarViewController
        
        //  self.navigationController?.pushViewController(gamelevel, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func getGameLevelsInfo() {
        let userId =  UserDefaults.standard.value(forKey: UserId) as! String
        let parameters : [String : String]  = [
            "userId": userId,
        ]
        if userId != ""
        {
           let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading"

                request(baseURL + getLevelInformation, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                    }
                    .validate { request, response, data in
                        return .success
                    }
                    .responseJSON { response in
                        let convertedString = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                        print(convertedString!)
                         if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                            print("JSON: \(JSON)")
                            if let error = JSON["ERROR"] as? String {
                                self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                            }
                            else {
                                if let gameLevels = JSON ["GAME LEVEL DETAILS"] as? NSArray{
                                    let dataHolder : NSMutableArray = []
                                    for i in 0 ..< gameLevels.count {
                                        let tempDic = gameLevels .object(at: i) as! [String : AnyObject]

                                        let levels = Level()
                                        levels.level =  (tempDic["LEVEL"] as? String)!
                                        let minCreds = tempDic["MinCreds"] as? NSNumber
                                        if (minCreds != nil) {
                                            levels.minCreds = "\(minCreds!)"
                                        }
                                            let pintToNextLevel = tempDic["POINTNEEDEDTOCOMPLETETHISLEVEL"] as? NSNumber
                                        if (pintToNextLevel != nil) {
                                            levels.pointToCompleteLevel = "\(pintToNextLevel!)"
                                        }
                                        dataHolder.add(levels)
                                    }
                                    self.level_array = dataHolder
                                    self.level_tableView.reloadData()
                                }

                            }
                            MBProgressHUD.hide(for: self.view, animated: true);
                        }

                        else {
                            MBProgressHUD.hide(for: self.view, animated: true);
                            print(response.result.error!)
                            self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                        }
                }
            }else {
                MBProgressHUD.hide(for: self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
        }
    }
    

    func getGameDetailsInfo(){
        let userId =  UserDefaults.standard.value(forKey: UserId) as! String

        let parameters : [String : String]  = [
            "userId": userId,
            ]
        if userId != ""
        {
            let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading"
                request(baseURL + getGameDetails, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                    }
                    .validate { request, response, data in
                        return .success
                    }
                    .responseJSON { response in
                        let convertedString = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                        print(convertedString!)
                        if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                            print("JSON: \(JSON)")
                            if let error = JSON["ERROR"] as? String {
                                self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                            }
                            else {
                                if let gameLevels = JSON ["ALL GAME DETAILS"] as? NSArray{
                                    let dataHolder : NSMutableArray = []
                                    for i in 0 ..< gameLevels.count {
                                        let tempDic = gameLevels .object(at: i) as! [String : AnyObject]
                                        let rules = Rules()
                                        rules.locationName = (tempDic["GAME NAME"] as? String)!
                                        let gameRuleId = tempDic["gameRuleId"] as? NSNumber
                                        if (gameRuleId != nil) {
                                            rules.gameRuleId = "\(gameRuleId!)"
                                        }
                                        rules.rule = (tempDic["GameRule"] as? String)!
                                        let creds = tempDic["GameRuleCreds"] as? String
//                                            tempDic["GameRuleCreds"] as? NSNumber
                                        if (creds != nil) {
                                            rules.creds = "\(creds!)"
                                        }
                                        rules.expiryDate = (tempDic["expiryDate"] as? String)!
                                        let ruleLimit = tempDic["GameRuleCreds"] as? String
//                                       tempDic["GameRuleCreds"] as? NSNumber
                                        rules.ruleLimit = "\(ruleLimit!)"
                                        dataHolder.add(rules)
                                    }
                                    self.level_array = dataHolder
                                    self.level_tableView.reloadData()
                                }
                                MBProgressHUD.hide(for: self.view, animated: true);
                            }
                        }
                        else {
                            MBProgressHUD.hide(for: self.view, animated: true);
                            print(response.result.error!)
                            self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                        }
                }
            }else {
                MBProgressHUD.hide(for: self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (level_array != nil) {
            return self.level_array.count
        }
        else {
            return 0
        }
    }
    
    /*func numberOfSectionsInTableView(_ tableView: UITableView) -> Int {
        if (level_array != nil) {
            return self.level_array.count
        }
        else {
            return 0
        }
        
    }*/
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell{
        
        //  let cellidentifier = "cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as UITableViewCell
        
        var rule = Rules()
        var levels = Level()

        if selectedSegment == "Creds" {
            rule  = level_array[(indexPath as NSIndexPath).row] as! Rules
        }
        else {
            
           levels  = level_array[(indexPath as NSIndexPath).row] as! Level
            
        }
        
      
        
        
        let level_label = cell.viewWithTag(101) as? UILabel
        let minCreds_label = cell.viewWithTag(102) as? UILabel
        let requiredCreds_label = cell.viewWithTag(103) as? UILabel
          let expiryLAbel = cell.viewWithTag(104) as? UILabel
        
        level_label?.adjustsFontSizeToFitWidth = true
        minCreds_label?.adjustsFontSizeToFitWidth = true
        requiredCreds_label?.adjustsFontSizeToFitWidth = true
        
        if selectedSegment == "Game" {
            
        level_label?.text = levels.level
            
        minCreds_label?.text = "Minimum Cred Required : \(levels.minCreds)"
        
        let status = levels.pointToCompleteLevel
        if status == "0" {
             requiredCreds_label?.text = "Status : Achieved"
        }
        else {
            requiredCreds_label?.text = "Cred required to reach this level: \(levels.pointToCompleteLevel)"
        }
        expiryLAbel?.isHidden = true
        }
        else {
             expiryLAbel?.isHidden = false
            level_label?.text = rule.locationName

            minCreds_label?.text = rule.rule
            requiredCreds_label?.text = rule.creds
            let expiry = (rule.expiryDate)
            if expiry == "" {
                expiryLAbel?.text = ""
            }else {
                expiryLAbel?.text = "Expires in : \(rule.expiryDate)"
            }
//            expiryLAbel?.text = "Expires in : \(rule.expiryDate)"
//                rule.expiryDate
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
        footerView.backgroundColor = UIColor.clear
        
        return footerView
    }
    
   

}
