//
//  EditProfileViewController.swift
//  KikSpot
//
//  Created by knsmac003 on 12/18/15.
//  Copyright © 2015 Shruti Mittal. All rights reserved.
//

import UIKit
import Foundation
import MBProgressHUD

class EditProfileViewController: UIViewController,UITextFieldDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    
    @IBOutlet weak var deletePicButton: UIButton!
    
    @IBOutlet weak var editPhotoButton: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet var userNameTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var DobTextField: UITextField!
    @IBOutlet weak var add1TextField: UITextField!
    @IBOutlet weak var add2TextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var zipTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var datePicker :  UIDatePicker!
    var activeField: UITextField?
     var cameFrom: String! = ""
    var imagePicker:UIImagePickerController?=UIImagePickerController()
    var popover:UIPopoverController?=nil
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarItem()
        registerForKeyboardNotifications()
        self.navigationItem.title = "Profile"
        
        //self.navigationItem.hidesBackButton = true
        
        let logButton : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "Back.png"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(EditProfileViewController.howtoplay(_:)))
//        self.profileImage.image = UIImage(named: "defaultProfile")
        self.navigationItem.rightBarButtonItem = nil
        self.navigationItem.leftBarButtonItem = logButton
        createPaddingForTextField(self.userNameTextField)
        createPaddingForTextField(self.firstNameTextField)
        createPaddingForTextField(self.lastNameTextField)
        createPaddingForTextField(self.DobTextField)
        createPaddingForTextField(self.add1TextField)
        createPaddingForTextField(self.add2TextField)
        createPaddingForTextField(self.cityTextField)
        createPaddingForTextField(self.stateTextField)
        createPaddingForTextField(self.zipTextField)
        createPaddingForTextField(self.emailTextField)
//        self.profileImage.layer.cornerRadius = ((profileImage?.frame.size.height)!)/2;
//        self.profileImage.clipsToBounds = true;
        self.getProfileDetails()
        
        /*if cameFrom == "sideBar" {
            self.navigationItem.leftBarButtonItem = nil
        }else {
            self.navigationItem.leftBarButtonItem = self.newBackButton
        }*/
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func deletePhotoButtonClicked(_ sender: Any) {
        let alert:UIAlertController=UIAlertController(title: "Kikspot", message:"Are you sure want to delete the Photo", preferredStyle: UIAlertControllerStyle.actionSheet)
        let OK = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.profileImage.image = UIImage(named: "defaultProfile")
            self.sendImageToserver()
        }
        let Cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
        }
        alert.addAction(OK)
        alert.addAction(Cancel)
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func editPhotoButtonClicked(_ sender: Any) {
        
            let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
            {
                UIAlertAction in
                self.openCamera()
            }
            let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
            {
                UIAlertAction in
                self.openGallary()
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
            {
                UIAlertAction in
            }
            // Add the actions
            imagePicker?.delegate = self
            alert.addAction(cameraAction)
            alert.addAction(gallaryAction)
            alert.addAction(cancelAction)
            // Present the controller
            if UIDevice.current.userInterfaceIdiom == .phone
            {
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                popover=UIPopoverController(contentViewController: alert)
                popover!.present(from: editPhotoButton.frame, in: self.view, permittedArrowDirections: UIPopoverArrowDirection.any, animated: true)
            }
        }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker!.sourceType = UIImagePickerControllerSourceType.camera
            self .present(imagePicker!, animated: true, completion: nil)
        }
        else
        {
            let alertWarning = UIAlertView(title:"Warning", message: "You don't have camera", delegate:nil, cancelButtonTitle:"OK", otherButtonTitles:"")
            alertWarning.show()
        }
    }
    
    func openGallary()
    {
        imagePicker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(imagePicker!, animated: true, completion: nil)
    }
    //PickerView Delegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        var image: UIImage!
        
        // fetch the selected image
        if picker.allowsEditing {
            image = info[UIImagePickerControllerEditedImage] as! UIImage
        } else {
            image = info[UIImagePickerControllerOriginalImage] as! UIImage
        }
        
        self.profileImage.image = image
        // Do something about image by yourself
        
        // dissmiss the image picker controller window
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.imagePicker = UIImagePickerController()
        dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    deinit {
        deregisterFromKeyboardNotifications()
    }
    
    func howtoplay(_ sender: UIBarButtonItem) {
     // Perform your custom actions
     // ...
     // Go  to the mentioned ViewController
        
         self.navigationController?.popViewController(animated: true)
     
   //  let gamelevel = self.storyboard?.instantiateViewController(withIdentifier: "MoreOptions") as!  MoreViewController
     
   //  self.navigationController?.pushViewController(gamelevel, animated: false)
     }

    
    func sendImageToserver(){
//        if (self.profileImage.image == nil){
//            self.present(Common.showAlertWithTitle("Kikspot", message: "Please choose an image", cancelButton: "OK"), animated: true, completion: nil)
//        }
//        else {
            let userId =  UserDefaults.standard.value(forKey: UserId) as! String
            var prameterText : String  = ""
            do {

            } catch let error as NSError {
                print(error)
            }
            let imageData = UIImageJPEGRepresentation(self.profileImage.image!, 0.7)
            
            let parameters: [String : String] = [
                "preferredVenueType": "",
                "userId": userId,
                ]
            var theJSONText : NSString  = ""
            
            do {
                let theJSONData = try JSONSerialization.data(
                    withJSONObject: parameters ,
                    options: JSONSerialization.WritingOptions(rawValue: 0))
                theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)!
                
                
            } catch let error as NSError {
                print(error)
            }
            let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading"
                upload(
                    multipartFormData: { multipartFormData in
                        multipartFormData.append(imageData!, withName: "profileImage", fileName: "profileImage.png", mimeType: "image/jpeg")
                        multipartFormData.append(theJSONText.data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)!, withName: "json")
                },
                    to: baseURL + uploadImage,
                    encodingCompletion: { encodingResult in
                        switch encodingResult {
                        case .success(let upload, _, _):
                            upload.responseJSON { response in
                                debugPrint(response)
                                if let JSON:[String:AnyObject] = response.result.value as! [String : AnyObject]?  {
                                    print("JSON: \(JSON)")
                                    if let status = JSON["STATUS"] as? String {
                                        if (status == "success") {
                                        }
                                        else {
                                            if let error = JSON["ERROR"] as? String {
                                                self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                            }
                                        }
                                        MBProgressHUD.hide(for: self.view, animated: true)
                                    }
                                    else {
                                        MBProgressHUD.hide(for: self.view, animated: true);
                                        // print(response.result.error)
                                        self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                                    }
                                    MBProgressHUD.hide(for: self.view, animated: true);
                                }
                            }
                        case .failure(let encodingError):
                            print(encodingError)
                        }
                }
                )
                
            }else {
                MBProgressHUD.hide(for: self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
//        }
    }
    
    
    func getProfileDetails() {
        
        let userId =  UserDefaults.standard.value(forKey: UserId) as! String
        
        let parameters : [String : String]  = [
            "userId": userId,
            ]
        if userId != ""
        {
           let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading"
                request(baseURL + ProfileDetails, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                        //print("Progress: \(progress.fractionCompleted)")
                    }
                    .validate { request, response, data in
                        // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                        return .success
                    }
                    .responseJSON { response in
                        
                        // result of response serialization
                        
                        if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                            print("JSON: \(JSON)")
                            
                            
                            //                            if let status = JSON["STATUS"] as? String {
                            //                                if (status == "Success") {
                            
                            /*
                             var json: JSON =  ["response": ["key1":"value1", "key2":"value2"]]
                             if json["response"]["someKey"].exists() {
                             print("response someKey exists")
                             }
 */
                            
                            if let prodetails = JSON ["profileDetails "] as? NSDictionary{
                                
                                print("count \(prodetails.count)")
                                if prodetails.count > 0 {
                                    
                                    if let userName = prodetails["username"]
                                    {
                                        self.userNameTextField.text = (userName as? String)!
                                    }
                                    if let firstName = prodetails["firstName"]
                                    {
                                        self.firstNameTextField.text = (firstName as? String)!
                                    }
                                    if let lastName = prodetails["lastName"]
                                    {
                                        self.lastNameTextField.text = (lastName as? String)!
                                    }
                                    if let address1 = prodetails["address1"]
                                    {
                                        self.add1TextField.text = (address1 as? String)!
                                    }
                                    if let address2 = prodetails["address2"]
                                    {
                                        self.add2TextField.text = (address2 as? String)!
                                    }
                                    if let state = prodetails["state"]
                                    {
                                        self.stateTextField.text = (state as? String)!
                                    }
                                    if let city = prodetails["city"]
                                    {
                                        self.cityTextField.text = (city as? String)!
                                    }
                                    if let zip = prodetails["zip"]
                                    {
                                        self.zipTextField.text = (zip as? String)!
                                    }
                                    if let dateofBirth = prodetails["dateofBirth"]
                                    {
                                        self.DobTextField.text = (dateofBirth as? String)!
                                    }
                                    if let emailId = prodetails["emailId"]
                                    {
                                        self.emailTextField.text = (emailId as? String)!
                                    }
                                    if let profilePic = prodetails["profilePicture"]
                                    {
                                        self.profileImage?.downloadImageFrom(link: profilePic as! String, contentMode: UIViewContentMode.scaleAspectFit)
//                                        self.profileImage.image = UIImage(contentsOfFile: profilePic as! String)
                                    }
//                                    profilePicture
//                                    self.userNameTextField.text = (prodetails["username"] as? String)!
//                                    self.firstNameTextField.text = (prodetails["firstName"] as? String)!
//                                    self. lastNameTextField .text = (prodetails["lastName"] as? String)!
//                                    self.add1TextField.text = (prodetails["address1"] as? String)!
//                                    self.add2TextField.text = (prodetails["address2"] as? String)!
//                                    self.stateTextField.text = (prodetails["state"] as? String)!
//                                    self.cityTextField.text = (prodetails["city"] as? String)!
//                                    self.zipTextField.text = (prodetails["zip"] as? String)!
//                                    self.DobTextField.text = (prodetails["dateofBirth"] as? String)!
//                                    self.emailTextField.text = (prodetails["emailId"] as? String)!
                                    
                                }
                            }
                            else {
                                
                                if let error = JSON["ERROR"] as? String {
                                    self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                }
                            }
                            
                            MBProgressHUD.hide(for: self.view, animated: true);
                        }
                        else {
                            MBProgressHUD.hide(for: self.view, animated: true);
                            print(response.result.error!)
                            self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                        }
                        MBProgressHUD.hide(for: self.view, animated: true);
                }
            }else {
                MBProgressHUD.hide(for: self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
            //            let req = request(.GET, baseURL + ProfileDetails, parameters: parameters, encoding :.JSON)
            //            debugPrint(req)
        }
        
        
    }
    
    
    
    func createPaddingForTextField (_ textField: UITextField) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10,height: textField.frame.height))
        textField.leftView = paddingView
        textField.leftViewMode = UITextFieldViewMode.always
        textField.delegate = self;
       // textField .setValue(defaultColor, forKeyPath: "_placeholderLabel.textColor")
        
    }
    
    func registerForKeyboardNotifications()
    {
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(EditProfileViewController.keyboardWasShown(_:)),name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(EditProfileViewController.keyboardWillBeHidden(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func deregisterFromKeyboardNotifications()
    {
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWasShown(_ notification: Notification)
    {
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollView.isScrollEnabled = true
        let info : NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if (activeField != nil)
        {
            if (!aRect.contains(activeField!.frame.origin))
            {
                self.scrollView.scrollRectToVisible(activeField!.frame, animated: true)
            }
        }
    }
    
    func keyboardWillBeHidden(_ notification: Notification)
    {
        //Once keyboard disappears, restore original positions
        self.scrollView.contentInset = UIEdgeInsets.zero
        self.scrollView.scrollIndicatorInsets = UIEdgeInsets.zero
        self.activeField?.resignFirstResponder()
        self.scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        activeField = textField
        
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == self.DobTextField {
            createDatePickerViewWithAlertController()
            return false
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        activeField = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    
    func createDatePickerViewWithAlertController()
    {
        let viewDatePicker: UIView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 200))
        viewDatePicker.backgroundColor = UIColor.clear
        
        
        self.datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 200))
        self.datePicker.datePickerMode = UIDatePickerMode.date
        self.datePicker.addTarget(self, action: #selector(EditProfileViewController.dateSelected), for: UIControlEvents.valueChanged)
        
        viewDatePicker.addSubview(self.datePicker)
        
        
     //   if(UIDevice.current.systemVersion >= "8.0")
    //    {
            
            
            let alertController = UIAlertController(title: nil, message: "\n\n\n\n\n\n\n\n\n\n", preferredStyle: UIAlertControllerStyle.actionSheet)
            
            alertController.view.addSubview(viewDatePicker)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
            { (action) in
                // ...
            }
            
            alertController.addAction(cancelAction)
            
            let OKAction = UIAlertAction(title: "Done", style: .default)
            { (action) in
                
                self.dateSelected()
            }
            
            alertController.addAction(OKAction)
            
            /*
             let destroyAction = UIAlertAction(title: "Destroy", style: .Destructive)
             { (action) in
             println(action)
             }
             alertController.addAction(destroyAction)
             */
            
            self.present(alertController, animated: true)
            {
                // ...
            }
            
     //   }
     //   else
//        {
//            let actionSheet = UIActionSheet(title: "\n\n\n\n\n\n\n\n\n\n", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Done")
//            actionSheet.addSubview(viewDatePicker)
//            actionSheet.show(in: self.view)
//        }
        
    }
    
 
    
    
    func dateSelected()
    {
        var selectedDate: String = String()
        
        selectedDate =  self.dateformatterDateTime(self.datePicker.date) as String
        
        self.DobTextField.text =  selectedDate
        
    }
    
    
    func dateformatterDateTime(_ date: Date) -> NSString
    {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        return dateFormatter.string(from: date) as NSString
    }
    
    // Now Implement UIActionSheet Delegate Method just for support for iOS 7 not for iOS 8
    
    // MARK: - UIActionSheet Delegate Implementation ::
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int)
    {
        switch buttonIndex
        {
            
        case 0:
            print("Done")
            self.dateSelected()
            break;
        case 1:
            print("Cancel")
            break;
        default:
            print("Default")
            break;
        }
    }
    
    @IBAction func EditButtonPressed(_ sender: AnyObject) {
        self.sendImageToserver()
        self.view.endEditing(true)
        let firstName = self.firstNameTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let lastName = self.lastNameTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        let addressLine1 = self.add1TextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let addressLine2 = self.add2TextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let dateOfBirth = self.DobTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let city = self.cityTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let state = self.stateTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let zip = self.zipTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let email = self.emailTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        if firstName == "" || lastName == "" || email == ""
//            || dateOfBirth == "" || addressLine1 == "" || city == "" || state == "" || zip == "" || email == ""
        {
            if firstName == ""{
                self.present(Common.showAlertWithTitle("Kikspot", message: "Please fill first-name", cancelButton: "OK"), animated: true, completion: nil)
            }
            else if lastName == "" {
                self.present(Common.showAlertWithTitle("Kikspot", message: "Please fill last-name", cancelButton: "OK"), animated: true, completion: nil)
            }
            else if email == "" {
                self.present(Common.showAlertWithTitle("Kikspot", message: "Please fill email id", cancelButton: "OK"), animated: true, completion: nil)
            }
//            self.present(Common.showAlertWithTitle("Kikspot", message: "Please fill all the fields", cancelButton: "OK"), animated: true, completion: nil)
        }
        else if (Common.ValidateEmailString(email as NSString) == false) {
            self.present(Common.showAlertWithTitle("Kikspot", message: "Please enter proper Email Id", cancelButton: "OK"), animated: true, completion: nil)
        }
        else {
            
            let userId =  UserDefaults.standard.value(forKey: UserId) as! String
            
            let parameters : [String : String]  = [
                
                "userId": userId,
                "firstName": firstName, // MM/dd/yyyy
                "lastName": lastName,
                "dateofBirth": dateOfBirth,
                "address1": addressLine1,
                "address2": addressLine2,
                "city": city,
                "state": state,
                "zip": zip,
                "emailId" : email
                
            ]
            print("parameters \(parameters)")
           let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading"
                
                request(baseURL + EditProfile, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                        //print("Progress: \(progress.fractionCompleted)")
                    }
                    .validate { request, response, data in
                        // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                        return .success
                    }
                    .responseJSON { response in
                        
                        // result of response serialization
                        
                        if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                        
                            print("JSON: \(JSON)")
                            if let status = JSON["STATUS"] as? String{
                                //let status = stats as! String
                                if (status == "Profile Updated Successfully") {
                                    
                                    //self.DisplayAlertSuccessPassword()
                                    self.view.endEditing(true)
                                    self.view.resignFirstResponder()
                                    let alert = UIAlertController(title:"Kikspot", message:status, preferredStyle: UIAlertControllerStyle.alert)
                                    let ok = UIAlertAction(title:"OK", style: UIAlertActionStyle.default , handler:{(action) -> Void in
                                        self.navigationController?.popViewController(animated: true)
                                        
                                                })
                                    alert.addAction(ok)
                                    self.present(alert,animated: true,completion: nil)
//
//                                    self.present(Common.showAlertWithTitle("Kikspot", message:status, cancelButton: "OK"), animated: true, completion: nil)
//                                    self.navigationController?.popViewController(animated: true)
                                }
                            }
                            else {
                                if let error = JSON["ERROR"] as? String {
                                    //let error = stats as! String
                                    self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                }
                            }
                            
                            MBProgressHUD.hide(for: self.view, animated: true);
                        }
                        else {
                            MBProgressHUD.hide(for: self.view, animated: true);
                            print(response.result.error!)
                            self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                        }
                }
            }else {
                MBProgressHUD.hide(for: self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
            
        }
    }
    
    func edit_preferences_button_presssed() {
        
        let preferenceView = self.storyboard?.instantiateViewController(withIdentifier: "PreferencesViewController") as! PreferencesViewController
        self.navigationController?.pushViewController(preferenceView, animated: true)
        
    }
    
    
    //    func DisplayAlertSuccessPassword()
    //    {
    //
    //        // alert heading
    //        let alert = UIAlertController(title:"Kikspot", message:"Profile updated successfully", preferredStyle: UIAlertControllerStyle.Alert)
    //
    //        // alert ok/send button where we are sending the email to the server
    //        let ok = UIAlertAction(title:"OK", style: UIAlertActionStyle.Default , handler:{(action) -> Void in
    //
    //            let loginView = self.storyboard?.instantiateViewControllerWithIdentifier("LoginViewController") as! LoginViewController
    //            self.navigationController?.pushViewController(loginView, animated: true)
    //
    //        })
    //
    //        alert.addAction(ok)
    //
    //        self.presentViewController(alert , animated: true, completion: nil)
    //
    //    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
