//
//  RulesViewController.swift
//  KikSpot
//
//  Created by Shruti Mittal on 03/02/16.
//  Copyright © 2016 Shruti Mittal. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


class RulesViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet var segmentController: UISegmentedControl!
   
    @IBOutlet var rules_tableView: UITableView!
    var rules_array : NSMutableArray = []
    var locationId_array : NSArray = []
    var locationID: String!
    var locationName: String!
    var callServiceFore: String!
    var RewardsForTabItem: String!
    var selectedSort: String = "close"
    @IBOutlet var tabBar: UITabBar!
    
    //    lazy   var searchBars:UISearchBar = UISearchBar(frame: CGRectMake(200,50, 200, 20))
    
    
    
    @IBAction func segmentSelected(_ sender: Any) {
        print("selected index: \((sender as AnyObject).selectedSegmentIndex)")
        
        switch segmentController.selectedSegmentIndex
        {
        case 0:
            if RewardsComingFromLocation == "yes"{
                self.selectedSort = "close"
                self.getRulesofaLocation()
            }
            else{
            self.selectedSort = "close"
            self.getRulesforAll()
            }
            break
            
        case 1:
            if RewardsComingFromLocation == "yes"{
                self.selectedSort = "time"
                self.getRulesofaLocation()
            }else{
            self.selectedSort = "time"
            self.getRulesforAll()
            }
            break
            
        default:
            
            break
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarItem()
        self.navigationItem.title = "Rewards"
        
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController!.navigationBar.titleTextAttributes = titleDict as? [String : AnyObject]

        
         segmentController.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.white], for: UIControlState.selected)
        
        self.navigationController?.navigationBar.barTintColor = defaultNavBarColor
        
        
        self.navigationItem.setHidesBackButton(true, animated: false)
        
        let homeButton : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "Back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(RulesViewController.goback(_:)))
        
        let logButton : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "Howtoplay.png"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(RulesViewController.howtoplay(_:)))
        
        self.navigationItem.leftBarButtonItem = homeButton
        self.navigationItem.rightBarButtonItem = logButton
        navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSFontAttributeName: UIFont(name:"Helvetica", size:15)!], for: UIControlState.normal)
        
//        rules_tableView.delegate = self
//        rules_tableView.dataSource = self
        rules_tableView.allowsSelection = true
        self.view.bringSubview(toFront: rules_tableView)
        
        if callServiceFore == "oneLocation" {
            self.getRulesofaLocation()
        } else if callServiceFore == "listOfLocation" {
            self.getRulesforListOfLocation()
        }
        else if callServiceFore == "allLocation" {
            self.getRulesforAll()
        }
        else
        {
            self.getRulesforAll()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.tabBarController?.navigationItem.title = "Rewards"
        let logButton : UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "Howtoplay.png"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(RulesViewController.howtoplay(_:)))
        self.tabBarController?.navigationItem.rightBarButtonItem = logButton
        if callServiceFore == "oneLocation" {
            self.getRulesofaLocation()
        } else if callServiceFore == "listOfLocation" {
            self.getRulesforListOfLocation()
        }
        else if callServiceFore == "allLocation" {
            self.getRulesforAll()
        }
        else
        {
            self.getRulesforAll()
        }
    }
    
    func howtoplay(_ sender: UIBarButtonItem) {
        // Perform your custom actions
        // ...
        // Go  to the mentioned ViewController
        
        let gamelevel = self.storyboard?.instantiateViewController(withIdentifier: "GameLevelDetails") as!  GameLevelDetails
        
        self.navigationController?.pushViewController(gamelevel, animated: true)
    }
    
    func goback(_ sender: UIBarButtonItem) {
        // Perform your custom actions
        // ...
        // Go  to the previous ViewController
        
          self.navigationController?.popViewController(animated: true)
        
       // let gamelevel = self.storyboard?.instantiateViewController(withIdentifier: "tabbarcontroller") as!  TabBarViewController
        
      //  self.navigationController?.pushViewController(gamelevel, animated: false)
    }

    
    
    
    
    
    func tabBar(_ tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        
        self.rules_array.removeAllObjects()
        if item.tag == 101 {
            
            self.selectedSort = "close"
            
        } else if item.tag == 102 {
            self.selectedSort = "time"
        }
        if callServiceFore == "listOfLocation" {
            self.getRulesforListOfLocation()
        }
        else if callServiceFore == "allLocation" {
            self.getRulesforAll()
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func getRulesofaLocation() {
        let userId =  UserDefaults.standard.value(forKey: UserId) as! String
        let parameters : [String : String]  = [
            "locationAPIId": self.locationID,
            "isActive": "true",
            "checkExpiryDate": "true",
            "sortBy":self.selectedSort,
            "userId": userId
        ]
        print(parameters)
        if userId != ""
        {
           let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading"
                request(baseURL + getRulesOfAParticularLocation, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                    }
                    .validate { request, response, data in
                        return .success
                    }
                    .responseJSON { response in
                        let convertedString = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                        print(convertedString!)
                        if let JSON : [String:AnyObject] = response.result.value as? [String:AnyObject] {
                            print("JSON: \(JSON)")
                            if let notification = JSON ["locationGameRules"] as? NSArray{
                                if notification.count > 0 {
                                    let dataHolder : NSMutableArray = []
                                    for i in 0 ..< notification.count {
                                        let tempDic = notification .object(at: i) as! [String:AnyObject]
                                        let rules = Rules()
//                                        rules.locationName = (tempDic["locationName"] as? String!)!
                                         rules.locationName = (tempDic["locationName"] as? String)!
                                        let gameRuleId = tempDic["gameRuleId"] as? NSNumber
                                        if (gameRuleId != nil) {
                                            rules.gameRuleId = "\(gameRuleId!)"
                                        }
//                                        rules.rule = (tempDic["rule"] as? String!)!
                                        rules.rule = (tempDic["rule"] as? String)!
                                        let creds = tempDic["creds"] as? NSNumber
                                        if (creds != nil) {
                                            rules.creds = "\(creds!)"
                                        }
//                                        rules.expiryDate = (tempDic["expiryDate"] as? String!)!
                                        rules.expiryDate = (tempDic["expiryDate"] as? String)!
                                        let ruleLimit = tempDic["ruleLimit"] as? NSNumber
                                        rules.ruleLimit = "\(ruleLimit!)"
                                        dataHolder.add(rules)
                                    }
                                    self.rules_array = dataHolder
                                    self.rules_tableView.reloadData()
                                }else{
                                    self.rules_array = []
                                    self.rules_tableView.reloadData()
                                }
                            }
                            else {
                                if let error = JSON["ERROR"] as? String {
                                    self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                }
                            }
                            MBProgressHUD.hide(for: self.view, animated: true);
                        }
                        else {
                            MBProgressHUD.hide(for: self.view, animated: true);
                            print(response.result.error!)
                            self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                        }
                    MBProgressHUD.hide(for: self.view, animated: true);
                }
            }else {
                MBProgressHUD.hide(for: self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
        }
    }
    
    func getRulesforListOfLocation() {
        let userId =  UserDefaults.standard.value(forKey: UserId) as! String
        let parameters: [String : AnyObject] = [
            "locationAPIId": self.locationId_array,
            "isActive": "true" as AnyObject,
            "checkExpiryDate": "true" as AnyObject,
            "sortBy":selectedSort as AnyObject,
            "userId": userId as AnyObject
        ]
        print(parameters)
        if userId != ""
        {
           let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading"
                request(baseURL + getRulesOfAListOfLocation, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                    }
                    .validate { request, response, data in
                        // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                        return .success
                    }
                    .responseJSON { response in

                        // result of response serialization
                        let convertedString = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                        print(convertedString!)
                         if let JSON : [String:AnyObject] = response.result.value as? [String:AnyObject] {
                            print("JSON: \(JSON)")
                            if let notification = JSON ["gameRulesOfLocations"] as? NSArray{

                                if notification.count > 0 {
                                    let dataHolder : NSMutableArray = []
                                    for i in 0 ..< notification.count {
                                        let tempDic = notification .object(at: i) as! [String:AnyObject]
                                        let rules = Rules()
//                                        rules.locationName =  (tempDic["locationName"] as? String!)!
//                                        rules.locationAPIId =  (tempDic["locationAPIId"] as? String!)!
                                        rules.locationName =  (tempDic["locationName"] as? String)!
                                        rules.locationAPIId =  (tempDic["locationAPIId"] as? String)!
                                        
                                        let gameRuleId = tempDic["gameRuleId"] as? NSNumber
                                        if (gameRuleId != nil) {
                                            rules.gameRuleId = "\(gameRuleId!)"
                                        }
//                                        rules.rule = (tempDic["rule"] as? String!)!
                                        rules.rule = (tempDic["rule"] as? String)!
                                        let creds = tempDic["creds"] as? NSNumber
                                        if (creds != nil) {
                                            rules.creds = "\(creds!)"
                                        }
//                                        rules.expiryDate = (tempDic["expiryDate"] as? String!)!
                                        rules.expiryDate = (tempDic["expiryDate"] as? String)!
                                        let ruleLimit = tempDic["ruleLimit"] as? NSNumber
                                        rules.ruleLimit = "\(ruleLimit!)"
                                        dataHolder.add(rules)
                                    }
                                    self.rules_array = dataHolder
                                    self.rules_tableView.reloadData()
                                }else{
                                    self.rules_array = []
                                    self.rules_tableView.reloadData()
                                }
                            }
                            else {

                                if let error = JSON["ERROR"] as? String {
                                    // let error = stats as! String
                                    self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                }
                            }

                            MBProgressHUD.hide(for: self.view, animated: true);
                        }
                        else {
                            MBProgressHUD.hide(for: self.view, animated: true);
                            print(response.result.error!)
                            self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                        }
                        MBProgressHUD.hide(for: self.view, animated: true);
                }
            }else {
                MBProgressHUD.hide(for: self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
           
        }
    }
    

    func getRulesforAll() {
        let userId =  UserDefaults.standard.value(forKey: UserId) as! String
        let userDefaults : UserDefaults = UserDefaults.standard

        var latitude :String = ""
        var longitude : String = ""
        if (userDefaults.value(forKey: currentLatitude) != nil) {
            latitude =  UserDefaults.standard.value(forKey: currentLatitude) as! String
        }
        if (userDefaults.value(forKey: currentLongitude) != nil) {
            longitude =  UserDefaults.standard.value(forKey: currentLongitude) as! String
        }

        let parameters : [String : String]  = [
            "isActive": "true",
            "checkExpiryDate": "true",
            "sortBy":selectedSort,
            "userId": userId,
            "latitude":latitude,
            "longitude": longitude
        ]
        print(parameters)
        if userId != ""
        {
           let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading"
                request(baseURL + getRulesForAll, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                    }
                    .validate { request, response, data in
                        return .success
                    }
                    .responseJSON { response in
                        // result of response serialization
                        let convertedString = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                        print(convertedString!)
                        if let JSON : [String:AnyObject] = response.result.value as? [String:AnyObject] {
                            print("JSON: \(JSON)")
                            if let notification = JSON ["locationGameRules"] as? NSArray{

                                if notification.count > 0 {
                                    let dataHolder : NSMutableArray = []
                                    for i in 0 ..< notification.count {
                                        let tempDic = notification .object(at: i) as! [String:AnyObject]
                                        let rules = Rules()
//                                        rules.locationName =  (tempDic["locationName"] as? String!)!
//                                         rules.locationAPIId =  (tempDic["locationAPIId"] as? String!)!
                                        rules.locationName =  (tempDic["locationName"] as? String)!
                                        rules.locationAPIId =  (tempDic["locationAPIId"] as? String)!
                                        
                                        let gameRuleId = tempDic["gameRuleId"] as? NSNumber
                                        if (gameRuleId != nil) {
                                            rules.gameRuleId = "\(gameRuleId!)"
                                        }
//                                        rules.rule = (tempDic["rule"] as? String!)!
                                         rules.rule = (tempDic["rule"] as? String)!
                                        let creds = tempDic["creds"] as? NSNumber
                                        if (creds != nil) {
                                            rules.creds = "\(creds!)"
                                        }
//                                        rules.expiryDate = (tempDic["expiryDate"] as? String!)!
                                        rules.expiryDate = (tempDic["expiryDate"] as? String)!
                                        let ruleLimit = tempDic["ruleLimit"] as? NSNumber
                                        rules.ruleLimit = "\(ruleLimit!)"
                                        dataHolder.add(rules)
                                    }
                                    self.rules_array = dataHolder
                                    self.rules_tableView.reloadData()
                                }else{
                                    self.rules_array = []
                                    self.rules_tableView.reloadData()
                                }
                            }
                            else {

                                if let error = JSON["ERROR"] as? String {
                                    self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                }
                            }

                            MBProgressHUD.hide(for: self.view, animated: true);
                        }
                        else {
                            MBProgressHUD.hide(for: self.view, animated: true);
                            print(response.result.error!)
                            self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                        }
                        MBProgressHUD.hide(for: self.view, animated: true);
                }
            }else {
                MBProgressHUD.hide(for: self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.rules_array.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
         return 1;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        print("didselect invoked")
        let recommend = rules_array[(indexPath as NSIndexPath).row] as! Rules
        let locationId = recommend.locationAPIId
        let locationname = recommend.locationName
        
        if locationId.isEmpty {
            print("ruleLocationAppID is empty")
        }
        else
        {
            let venueDetails = self.storyboard?.instantiateViewController(withIdentifier: "VenueDetails") as! VenueDetailsViewController
            venueDetails.locationId = locationId
            venueDetails.locationName = locationname
            BackButtonComingFrom = "Rewards"
            self.navigationController?.pushViewController(venueDetails, animated: true)
        }

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as UITableViewCell
        let rule = rules_array[(indexPath as NSIndexPath).row] as! Rules
        let locationName_label = cell.viewWithTag(101) as? UILabel
        let rule_label = cell.viewWithTag(102) as? UILabel
        let ruleLimit_label = cell.viewWithTag(103) as? UILabel
        let cred_label = cell.viewWithTag(104) as? UILabel
        let expiryDate_label = cell.viewWithTag(105) as? UILabel
        rule_label!.numberOfLines = 0
        rule_label!.lineBreakMode = NSLineBreakMode.byWordWrapping
        let font =  UIFont.systemFont(ofSize: 15)
        rule_label!.font = font
        rule_label!.text = rule.rule
        
        rule_label!.sizeToFit()
        
        locationName_label?.adjustsFontSizeToFitWidth = true
        ruleLimit_label?.adjustsFontSizeToFitWidth = true
        cred_label?.adjustsFontSizeToFitWidth = true
        expiryDate_label?.adjustsFontSizeToFitWidth = true
        
        locationName_label?.text = rule.locationName
        let ruleLimit = (rule.ruleLimit)
        if ruleLimit == "-1" {
            ruleLimit_label?.text = ""
        }else {
        ruleLimit_label?.text = "Rule Limit : \(rule.ruleLimit)"
        }
        cred_label?.text = "Creds : \(rule.creds)"
        
        let expiry = (rule.expiryDate)
        if expiry == "" {
            expiryDate_label?.text = ""
        }else {
            expiryDate_label?.text = "Expires in : \(rule.expiryDate)"
        }

        //expiryDate_label?.text = "Expiry Date : \(rule.expiryDate)"
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
        footerView.backgroundColor = UIColor.clear
        
        return footerView
    }
    
}
