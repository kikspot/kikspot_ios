//
//  SettingsViewController.swift
//  KikSpot
//
//  Created by knsmac003 on 12/22/15.
//  Copyright © 2015 Shruti Mittal. All rights reserved.
//

import UIKit
import MBProgressHUD

class SettingsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    
    var newBackButton: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    var settingsArray : NSArray = []
    var notify_switch : UISwitch!
    var selectedIndexPath : IndexPath!
    
    //    lazy   var searchBars:UISearchBar = UISearchBar(frame: CGRectMake(200,50, 200, 20))
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.setNavigationBarItem()
        self.navigationItem.title = "Settings"
        self.navigationItem.rightBarButtonItem = nil
        self.navigationItem.hidesBackButton = true
        newBackButton = UIBarButtonItem(image: UIImage(named: "Back.png"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(SettingsViewController.back(_:)))
        self.navigationItem.leftBarButtonItem = newBackButton;

        
        
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController!.navigationBar.titleTextAttributes = titleDict as? [String : AnyObject]
        // Do any additional setup after loading the view.
        
        self.getuserNotification()
    }
    func back(_ sender: UIBarButtonItem) {
        // Perform your custom actions
        // ...
        // Go back to the previous ViewController
        self.navigationController?.popViewController(animated: true)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func getuserNotification() {
        let userId =  UserDefaults.standard.value(forKey: UserId) as! String
        let parameters : [String : String]  = [
            "userId": userId
        ]
        if userId != ""
        {
           let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading"
                request(baseURL + ListNotifications, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                    }
                    .validate { request, response, data in
                        return .success
                    }
                    .responseJSON { response in
                        let convertedString = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                        print(convertedString!)
                         if let JSON : [String:AnyObject] = response.result.value as? [String:AnyObject] {
                            print("JSON: \(JSON)")
                            if let notification = JSON ["notifications"] as? NSArray{
                                if notification.count > 0 {
                                    let dataHolder : NSMutableArray = []
                                    for i in 0 ..< notification.count {
                                        let tempDic = notification .object(at: i) as! [String:AnyObject]
                                        let notify = NotificationsModel()
                                        let enable = tempDic["enabled"] as? NSNumber
                                        notify.enabled = "\(enable!)"
//                                       notify.notification = (tempDic["notification"] as? String!)!
                                        notify.notification = (tempDic["notification"] as? String)!
                                        let notifyId = tempDic["notificationId"] as? NSNumber
                                        notify.notificationId = "\(notifyId!)"
                                        dataHolder.add(notify)
                                    }
                                    self.settingsArray = dataHolder
                                    self.tableView.reloadData()
                                }
                            }
                            MBProgressHUD.hide(for: self.view, animated: true);
                        }
                        else {
                            MBProgressHUD.hide(for: self.view, animated: true);
                            print(response.result.error!)
                            self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                        }
                        MBProgressHUD.hide(for: self.view, animated: true);
                }
            }else {
                MBProgressHUD.hide(for: self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return settingsArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        //  let cellidentifier = "cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as UITableViewCell
        let notify = settingsArray[(indexPath as NSIndexPath).row] as! NotificationsModel
        
        
        
        let notification_label = cell.viewWithTag(101) as? UILabel
        notify_switch = cell.viewWithTag(102) as? UISwitch
        notification_label?.text = notify.notification
        notify_switch!.addTarget(self, action: #selector(SettingsViewController.switch_value_changed(_:)), for: UIControlEvents.valueChanged)
        if notify.enabled == "1" {
            notify_switch?.setOn(true, animated: false)
            
//            notify_switch.tintColor = UIColor .red; // the "off" color
//            notify_switch.onTintColor = UIColor .green;
        }
        else
        {
            
//            notify_switch.tintColor = UIColor .white; // the "off" color
//            notify_switch.onTintColor = UIColor .white;
            notify_switch?.setOn(false, animated: false)
        }
        
        
        return cell
    }
    
    func switch_value_changed (_ sender : UISwitch) {
        
        print("switch value is changed \(sender.tag))")
        
        self.notify_switch = sender
        let view = notify_switch.superview!
        let cell = view.superview as! UITableViewCell
        
        selectedIndexPath = tableView.indexPath(for: cell)
        
        let notify = settingsArray[(selectedIndexPath! as NSIndexPath).row] as! NotificationsModel
        var isOn = false
        
        if notify_switch.isOn {
            isOn = true
        }
        else {
            isOn = false
        }
        self.sendServerRequestToChange(notify.notificationId, currentState: isOn)
    }
    
    func sendServerRequestToChange (_ notificationID : String, currentState: Bool) {
        
        let userId =  UserDefaults.standard.value(forKey: UserId) as! String
        var enableString = ""
        if currentState == true {
            enableString = "true"
        }
        else {
            enableString = "false"
        }
        let parameters : [String : String]  = [
            "userId": userId,
            "notificationId":notificationID,
            "enable": enableString
        ]
        if userId != ""
        {
           let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading"
                request(baseURL + changeNotificationConfig, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                        //print("Progress: \(progress.fractionCompleted)")
                    }
                    .validate { request, response, data in
                        // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                        return .success
                    }
                    .responseJSON { response in
                
                        // result of response serialization
                        let convertedString = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                        print(convertedString!)
                        if let JSON : [String:AnyObject] = response.result.value as? [String:AnyObject] {
                            print("JSON: \(JSON)")
                            if let status = JSON["STATUS"] as? String {
                                if (status == "Success") {
                                    self.present(Common.showAlertWithTitle("Kikspot", message: "Setting Saved", cancelButton: "OK"), animated: true, completion: nil)
                                    if currentState == true {
                                        self.notify_switch?.setOn(true, animated: false)
                                    }
                                    else {
                                        self.notify_switch?.setOn(false, animated: false)
                                    }
                                }
                            }
                            else {
                                if let error = JSON["ERROR"] as? String {
                                    // let error = stats as! String
                                    self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                    if currentState == true {
                                        self.notify_switch?.setOn(false, animated: false)
                                    }
                                    else {
                                        self.notify_switch?.setOn(true, animated: false)
                                    }
                                    
                                }
                                
                                
                                MBProgressHUD.hide(for: self.view, animated: true);
                            }
                            
                            
                        }
                        else {
                            MBProgressHUD.hide(for: self.view, animated: true);
                            print(response.result.error!)
                            self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                        }
                        MBProgressHUD.hide(for: self.view, animated: true);
                }
            }
            else {
                MBProgressHUD.hide(for: self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
            //            let req = request(.GET, baseURL + changeNotificationConfig, parameters: parameters, encoding :.JSON)
            //            debugPrint(req)
        }
        
        
    }
    
}
