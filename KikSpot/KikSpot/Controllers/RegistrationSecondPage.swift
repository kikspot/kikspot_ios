//
//  RegistrationSecondPage.swift
//  KikSpot
//
//  Created by Shruti Mittal on 26/11/15.
//  Copyright © 2015 Shruti Mittal. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import MBProgressHUD

class RegistrationSecondPage: UIViewController, UITextFieldDelegate, UIActionSheetDelegate,CLLocationManagerDelegate {
    
    var didFinishCallingLocation = false
    let locationManager = CLLocationManager()
    
    @IBOutlet var firstName_textField: UITextField!
    @IBOutlet var lastName_textField: UITextField!
    @IBOutlet var addressLine1_textField: UITextField!
    @IBOutlet var addressLine2_textField: UITextField!
    @IBOutlet var city_textField: UITextField!
    @IBOutlet var state_textField: UITextField!
    @IBOutlet var zip_textField: UITextField!
    @IBOutlet var dateOfBirth_textField: UITextField!
    @IBOutlet var username_textField : UITextField!
    @IBOutlet var skip_button: UIButton!
    //    @IBOutlet var backImageView : UIImageView!
    var popover:UIPopoverController?=nil
    var datePicker :  UIDatePicker!
    var parentController :String!
    @IBOutlet var scrollView: UIScrollView!
    var activeField: UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: false)
        
        self.navigationItem.title = "REGISTRATION"
        
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationController!.navigationBar.titleTextAttributes = titleDict as? [String : AnyObject]

        
        registerForKeyboardNotification()
        
        createPaddingForTextField(self.firstName_textField)
        createPaddingForTextField(self.lastName_textField)
        createPaddingForTextField(self.addressLine1_textField)
        createPaddingForTextField(self.addressLine2_textField)
        createPaddingForTextField(self.dateOfBirth_textField)
        createPaddingForTextField(self.city_textField)
        createPaddingForTextField(self.state_textField)
        createPaddingForTextField(self.zip_textField)
        createPaddingForTextField(self.username_textField)
        //  let screen = UIScreen.mainScreen().bounds
        //  let screenWidth = screen.size.width
        //let screenHeight = screen.size.height
        
        //    scrollView.frame = screen
        // scrollView.contentSize = CGSizeMake(screenWidth, 1000)
        //scrollView.scrollEnabled = true
        
        if self.didFinishCallingLocation == false {
            
            self.locationManager.requestWhenInUseAuthorization()
            
            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        let screen = UIScreen.main.bounds
        let screenWidth = screen.size.width
        scrollView.contentSize = CGSize(width: screenWidth, height: 504)
        //backImageView.frame = CGRectMake(0, 0, scrollView.contentSize.width, scrollView.contentSize.height)
    }
    
    
    func createPaddingForTextField (_ textField: UITextField) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: textField.frame.height))
        textField.leftView = paddingView
        textField.leftViewMode = UITextFieldViewMode.always
        textField.delegate = self;
       // textField .setValue(defaultColor, forKeyPath: "_placeholderLabel.textColor")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        registerForKeyboardNotification()
        if self.parentController == "SocialLogin" {
            self.username_textField.isHidden = false
            self.skip_button.isHidden = true
        } else {
            self.username_textField.isHidden = true
            self.skip_button.isHidden = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(true)
        deregisterFromKeyboardNotification()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        locationManager.stopUpdatingLocation()
        
        UserDefaults.standard.set("\(locValue.latitude)" , forKey: currentLatitude)
        UserDefaults.standard.set("\(locValue.longitude)" , forKey: currentLongitude)
        var country : String = ""
        var city  : String = ""
        let geocoder = CLGeocoder()
        let location = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        geocoder.reverseGeocodeLocation(location) {
            (placemarks, error) -> Void in
            if let validPlacemark = placemarks?[0]{
                let placemark = validPlacemark as? CLPlacemark;
             //   print(placemark!.addressDictionary)
                
                
                if let city1 = placemark!.addressDictionary!["City"] as? NSString {
                    city = city1 as String
                }
                
                if let country1 = placemark!.addressDictionary!["Country"] as? NSString {
                    country  = country1 as String
                }
            }
            
            if self.didFinishCallingLocation == false {
                self.sendLocationDetailsToServer("\(locValue.latitude)", longitude: "\(locValue.longitude)", city: city , country: country)
            }
            
            self.didFinishCallingLocation = true
            
            
        }
        
        
        
    }
    
    func sendLocationDetailsToServer( _ latitude:String, longitude:String, city:String, country: String) {
        
        locationManager.stopUpdatingLocation()
        //let userId =  NSUserDefaults.standardUserDefaults().valueForKey(UserId) as! String
        
        let userDefaults : UserDefaults = UserDefaults.standard
        var userId :String = ""
        if (userDefaults.value(forKey: UserId) != nil) {
            userId =  userDefaults.value(forKey: UserId) as! String
        }
        
        let parameters : [String : String]  = [
            "userId": userId,
            "latitude": latitude,
            "longitude": longitude,
            "city": city,
            "country": country
        ]
        print (parameters)
        
        if userId != ""
        {
           let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                request(baseURL + updateUserLocationToServer, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                        //print("Progress: \(progress.fractionCompleted)")
                    }
                    .validate { request, response, data in
                        // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                        return .success
                    }
                    .responseJSON { response in
                        // result of response serialization
                        // print(response.data)
                        let convertedString = NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)
                      //  print(convertedString)
                        if let JSON = response.result.value {
                            print("JSON: \(JSON)")
                            
                        }
                }
                
            }
        }
    }
    
    
    @IBAction func signUp_button_pressed (_ sender: UIButton) {
        
        let firstName = self.firstName_textField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let lastName = self.lastName_textField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let addressLine1 = self.addressLine1_textField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let addressLine2 = self.addressLine2_textField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let dateOfBirth = self.dateOfBirth_textField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let city = self.city_textField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let state = self.state_textField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let zip = self.zip_textField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let userName = self.username_textField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        if self.parentController == "SocialLogin" {
            if userName == "" {
                self.present(Common.showAlertWithTitle("Kikspot", message: "Please fill userName", cancelButton: "OK"), animated: true, completion: nil)
            }
                //            else if dateOfBirth == ""
                //            {
                //                self.presentViewController(Common.showAlertWithTitle("Kikspot", message: "Please fill Date Of Birth", cancelButton: "OK"), animated: true, completion: nil)
                //            }
            else {
                
                let userId =  UserDefaults.standard.value(forKey: UserId) as! String
                
                let parameters : [String : String]  = [
                    "username" : userName,
                    "firstName": firstName, // MM/dd/yyyy
                    "lastName": lastName,
                    "dateofBirth": dateOfBirth,
                    "address1": addressLine1,
                    "address2": addressLine2,
                    "city": city,
                    "state": state,
                    "zip": zip,
                    "userId": userId,
                ]
                
                print(parameters)
                
               let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                    let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                    loadingNotification.mode = MBProgressHUDMode.indeterminate
                    loadingNotification.label.text = "Loading"
                    request(baseURL + RegisterStep2, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                        .downloadProgress(queue: DispatchQueue.utility) { progress in
                            //print("Progress: \(progress.fractionCompleted)")
                        }
                        .validate { request, response, data in
                            // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                            return .success
                        }
                        .responseJSON { response in
                            if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                                print("JSON: \(JSON)")
                                if let status = JSON["STATUS"] as? String{
                                    //let status = stats as! String
                                    if (status == "Registration Completed Successfully") {
                                        UserDefaults.standard.set("NO" , forKey: isUserALimitedLook)
                                        UserDefaults.standard.set("\(userName)" , forKey: savedUserName)
                                        let uploadImage = self.storyboard?.instantiateViewController(withIdentifier: "UploadImageViewController") as! UploadImageViewController
                                        uploadImage.parentController = "Registration"
                                        self.navigationController?.pushViewController(uploadImage, animated: true)
                                        
                                    }
                                }
                                else {
                                    if let error = JSON["ERROR"] as? String {
                                        //let error = stats as! String
                                        self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                    }
                                }
                                
                                MBProgressHUD.hide(for: self.view, animated: true);
                            }
                            else {
                                MBProgressHUD.hide(for: self.view, animated: true);
                           //     print(response.result.error)
                                self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                            }
                            MBProgressHUD.hide(for: self.view, animated: true);
                    }
                }else {
                    MBProgressHUD.hide(for: self.view, animated: true);
                    self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
                }
                
            }
        } else {
            
            //        if dateOfBirth == ""
            //        {
            //            self.presentViewController(Common.showAlertWithTitle("Kikspot", message: "Please fill Date Of Birth", cancelButton: "OK"), animated: true, completion: nil)
            //        }
            //        else {
            
            let userId =  UserDefaults.standard.value(forKey: UserId) as! String
            
            let parameters : [String : String]  = [
                "firstName": firstName, // MM/dd/yyyy
                "lastName": lastName,
                "dateofBirth": dateOfBirth,
                "address1": addressLine1,
                "address2": addressLine2,
                "city": city,
                "state": state,
                "zip": zip,
                "userId": userId,
            ]
            print(parameters)
            
           let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading"
                request(baseURL + RegisterStep2, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                        //print("Progress: \(progress.fractionCompleted)")
                    }
                    .validate { request, response, data in
                        // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                        return .success
                    }
                    .responseJSON { response in
                        if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                            print("JSON: \(JSON)")
                            if let status = JSON["STATUS"] as? String{
                                //let status = stats as! String
                                if (status == "Registration Completed Successfully") {
                                    UserDefaults.standard.set("NO" , forKey: isUserALimitedLook)
                                    
                                    let uploadImage = self.storyboard?.instantiateViewController(withIdentifier: "UploadImageViewController") as! UploadImageViewController
                                    uploadImage.parentController = "Registration"
                                    self.navigationController?.pushViewController(uploadImage, animated: true)
                                    
                                }
                            }
                            else {
                                if let error = JSON["ERROR"] as? String {
                                    //let error = stats as! String
                                    self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                }
                            }
                            
                            MBProgressHUD.hide(for: self.view, animated: true);
                        }
                        else {
                            MBProgressHUD.hide(for: self.view, animated: true);
                        //    print(response.result.error)
                            self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                        }
                        MBProgressHUD.hide(for: self.view, animated: true);
                }
            }else {
                MBProgressHUD.hide(for: self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
            //            let req = request(.GET, baseURL + RegisterStep2, parameters: parameters, encoding :.JSON)
            //            debugPrint(req)
            
        }
        //  }
    }
    
    //    deinit {
    //        deregisterFromKeyboardNotification()
    //    }
    
    
    func registerForKeyboardNotification()
    {
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(RegistrationSecondPage.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RegistrationSecondPage.keyboardWillBeHid(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    func deregisterFromKeyboardNotification()
    {
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillShow(_ notification: Notification)
    {
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollView.isScrollEnabled = true
        let info : NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if (activeField != nil)
        {
            if (!aRect.contains(activeField!.frame.origin))
            {
                self.scrollView.scrollRectToVisible(activeField!.frame, animated: true)
            }
        }
        
        
        
    }
    
    func keyboardWillBeHid(_ notification: Notification)
    {
        //Once keyboard disappears, restore original positions
        //        let info : NSDictionary = notification.userInfo!
        //        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue().size
        //        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 64.0, -keyboardSize!.height, 0.0)
        self.scrollView.contentInset = UIEdgeInsets.zero
        self.scrollView.scrollIndicatorInsets = UIEdgeInsets.zero
        //        self.view.endEditing(true)
        //        self.scrollView.scrollEnabled = false
        self.activeField?.resignFirstResponder()
        self.scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        
    }
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        activeField = textField
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeField = textField
        if textField == self.dateOfBirth_textField {
            createDatePickerViewWithAlertController()
            return false
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        activeField = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    
    func createDatePickerViewWithAlertController()
    {
        let viewDatePicker: UIView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 200))
        viewDatePicker.backgroundColor = UIColor.clear
        
        
        self.datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 200))
        self.datePicker.datePickerMode = UIDatePickerMode.date
        self.datePicker.addTarget(self, action: #selector(RegistrationSecondPage.dateSelected), for: UIControlEvents.valueChanged)
        
        viewDatePicker.addSubview(self.datePicker)
        
     //   if(UIDevice.current.systemVersion >= "8.0")
     //   {
            
            let alertController = UIAlertController(title: nil, message: "\n\n\n\n\n\n\n\n\n\n", preferredStyle: UIAlertControllerStyle.actionSheet)
            
            alertController.view.addSubview(viewDatePicker)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                { (action) in
                    // ...
            }
            
            alertController.addAction(cancelAction)
            
            let OKAction = UIAlertAction(title: "Done", style: .default)
                { (action) in
                    
                    self.dateSelected()
            }
            
            alertController.addAction(OKAction)
            
            /*
            let destroyAction = UIAlertAction(title: "Destroy", style: .Destructive)
            { (action) in
            println(action)
            }
            alertController.addAction(destroyAction)
            */
            if UIDevice.current.userInterfaceIdiom == .phone
            {
                self.present(alertController, animated: true)
                    {
                        // ...
                }
            }
            else
            {
                popover=UIPopoverController(contentViewController: alertController)
                popover!.present(from: self.dateOfBirth_textField.frame, in: self.view, permittedArrowDirections: UIPopoverArrowDirection.any, animated: true)
            }
            
            
       // }
//        else
//        {
//            let actionSheet = UIActionSheet(title: "\n\n\n\n\n\n\n\n\n\n", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Done")
//            actionSheet.addSubview(viewDatePicker)
//            actionSheet.show(in: self.view)
//        }
        
        
        
        
        
    }
    
    func dateSelected()
    {
        var selectedDate: String = String()
        
        selectedDate =  self.dateformatterDateTime(self.datePicker.date) as String
        
        self.dateOfBirth_textField.text =  selectedDate
        
    }
    
    
    func dateformatterDateTime(_ date: Date) -> NSString
    {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        return dateFormatter.string(from: date) as NSString
    }
    
    // Now Implement UIActionSheet Delegate Method just for support for iOS 7 not for iOS 8
    
    // MARK: - UIActionSheet Delegate Implementation ::
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int)
    {
        switch buttonIndex
        {
            
        case 0:
            print("Done")
            self.dateSelected()
            break;
        case 1:
            print("Cancel")
            break;
        default:
            print("Default")
            break;
        }
    }
    
    @IBAction func skip_button_pressed () {
        
        let uploadImage = self.storyboard?.instantiateViewController(withIdentifier: "UploadImageViewController") as! UploadImageViewController
        uploadImage.parentController = "Registration"
        self.navigationController?.pushViewController(uploadImage, animated: true)
        
    }
    
}
