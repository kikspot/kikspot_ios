//
//  ReadMoreViewController.swift
//  KikSpot
//
//  Created by Ajay Kumar Drakshapelli on 21/10/19.
//  Copyright © 2019 Shruti Mittal. All rights reserved.
//

import UIKit

class ReadMoreViewController: UIViewController {
    var readMoreText: String!
    var headingText: String!
    @IBOutlet weak var displayView: UIView!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var readMoreTextView: UITextView!
    @IBOutlet weak var OkButton: UIButton!
    @IBAction func okButtonClicked(_ sender: Any) {
        dismiss(animated:true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.readMoreTextView.text = readMoreText
        self.readMoreTextView.isEditable = false
        self.headingLabel.text = headingText
        self.headingLabel.adjustsFontSizeToFitWidth = true
        self.displayView.layer.cornerRadius = 5
        self.displayView.layer.borderWidth = 1
        self.displayView.layer.borderColor = UIColor(red: 0/255, green: 122/255, blue: 255/255, alpha: 1).cgColor
    }

}
