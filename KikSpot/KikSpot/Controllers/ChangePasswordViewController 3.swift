//
//  ChangePasswordViewController.swift
//  KikSpot
//
//  Created by knsmac003 on 12/21/15.
//  Copyright © 2015 Shruti Mittal. All rights reserved.
//

import UIKit
import MBProgressHUD
import MBProgressHUD

class ChangePasswordViewController: UIViewController {
    
    @IBOutlet weak var oldPasswordTextField: UITextField!
    
    @IBOutlet weak var newPasswordTextField: UITextField!

  
    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.navigationItem.title = "CHANGE PASSWORD"
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.oldPasswordTextField.frame.height))
        oldPasswordTextField.leftView = paddingView
        oldPasswordTextField.leftViewMode = UITextFieldViewMode.always
        oldPasswordTextField.setValue(defaultColor, forKeyPath: "_placeholderLabel.textColor")
        
        
        let paddingView1 = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.newPasswordTextField.frame.height))
        newPasswordTextField.leftViewMode = UITextFieldViewMode.always
        newPasswordTextField.leftView = paddingView1
       // newPasswordTextField.setValue(defaultColor, forKeyPath: "_placeholderLabel.textColor")

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func ChangePasswordButtonPressed(_ sender: AnyObject) {
        
        var oldPassword : String = self.oldPasswordTextField.text!;
        var newPassword : String = self.newPasswordTextField.text!;
        
        oldPassword = oldPassword.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        newPassword = newPassword.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        let userId =  UserDefaults.standard.value(forKey: UserId) as! String
        
        let parameters : [String : String]  = [
             "userId": userId,
            "oldPassword": oldPassword,
            "newPassword": newPassword
        ]
        if oldPassword == "" || newPassword == ""
        {
            self.present(Common.showAlertWithTitle("Kikspot", message: "Please fill all the fields", cancelButton: "OK"), animated: true, completion: nil)
        }
        else {
           let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading"
                request(baseURL + ChangePassword, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                        //print("Progress: \(progress.fractionCompleted)")
                    }
                    .validate { request, response, data in
                        // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                        return .success
                    }
                    .responseJSON { response in
                        // result of response serialization
                        
                        if let JSON : [String:AnyObject] = response.result.value as? [String:AnyObject] {
                            print("JSON: \(JSON)")
                            if let status = JSON["STATUS"] as? String {
                                
                                if (status == "success") {
                                    
                                   self.DisplayAlertSuccessPassword()
                                }
                            }
                            else {
                                if let error = JSON["ERROR"] as? String {
                                    // let error = stats as! String
                                    self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                }
                            }
                            MBProgressHUD.hide(for: self.view, animated: true);
                        }
                        else {
                            MBProgressHUD.hide(for: self.view, animated: true);
                            print(response.result.error!)
                            self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                        }
                         MBProgressHUD.hide(for: self.view, animated: true);
                }
            }else {
                MBProgressHUD.hide(for: self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
        }

        
        
    }

    func DisplayAlertSuccessPassword()
    {
        
        // alert heading
        let alert = UIAlertController(title:"Kikspot", message:"Password Changed successfully", preferredStyle: UIAlertControllerStyle.alert)
        
        // alert ok/send button where we are sending the email to the server
        let ok = UIAlertAction(title:"OK", style: UIAlertActionStyle.default , handler:{(action) -> Void in
            
            let loginView = self.storyboard?.instantiateViewController(withIdentifier: "LoginView") as! LoginViewController
            self.navigationController?.pushViewController(loginView, animated: true)
            
        })
        
        alert.addAction(ok)
        
        self.present(alert , animated: true, completion: nil)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
