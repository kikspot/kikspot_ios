//
//  RecomendSwipedCell.swift
//  KikSpot
//
//  Created by Shruti Mittal on 28/12/15.
//  Copyright © 2015 Shruti Mittal. All rights reserved.
//

import Foundation
import UIKit

protocol RecommendCellSwipeDelegate {
    func performAction( _ didAction: String)
}

class RecomendSwipedCell: UIView {
    var delegate: RecommendCellSwipeDelegate?
     @IBOutlet var imageView: UIImageView!
    
     func showPopUpWithImage (_ imageUrl: String, WebURl: String, rules: String , latitude: String, longitude: String, delegate: AnyObject) -> RecomendSwipedCell{
        
        let recommendSwipeCell =  Bundle.main.loadNibNamed("RecommendSwipedCell", owner: self, options: nil)![0]  as? RecomendSwipedCell
        recommendSwipeCell!.delegate = delegate as? RecommendCellSwipeDelegate
        
        let url = URL(string:imageUrl)
         let data = try? Data(contentsOf: url!)
                if data != nil {
                    recommendSwipeCell!.imageView?.image = UIImage(data:data!)
                }

        return recommendSwipeCell!
    }

    @IBAction func web_button_pressed() {
        
        self.delegate?.performAction("WEB")
    }
    
    @IBAction func rules_button_pressed() {
         self.delegate?.performAction("RULES")
    }

    
    @IBAction func map_button_pressed() {
        self.delegate?.performAction("MAP")
    }

    
}
