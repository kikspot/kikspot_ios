//
//  FeebBackPopUp.swift
//  KikSpot
//
//  Created by Shruti Mittal on 13/01/16.
//  Copyright © 2016 Shruti Mittal. All rights reserved.
//

import Foundation
import Foundation
import UIKit

protocol FeedBAckPopUpDelegate {
    func performActionforFeedBack( _ didAction: String,  withComment: String)
}

class FeebBackPopUp: UIView, UITextViewDelegate{
    
    var delegate: FeedBAckPopUpDelegate?
    @IBOutlet var feedback_textView : UITextView!
    
    
    func showFeedBAckPopUp (_ delegate: AnyObject) -> FeebBackPopUp{
        
        let feedbackPopUp =  Bundle.main.loadNibNamed("FeedBackPopUp", owner: self, options: nil)![0]  as? FeebBackPopUp
        feedbackPopUp!.delegate = delegate as? FeedBAckPopUpDelegate
        feedbackPopUp!.feedback_textView.layer.cornerRadius = 15
        feedbackPopUp!.feedback_textView.layer.borderWidth = 2
        feedbackPopUp!.feedback_textView.layer.borderColor = defaultColor.cgColor
        feedbackPopUp!.feedback_textView.delegate = self
        
        return feedbackPopUp!
    }
    
    @IBAction func attach_button_pressed () {
        
        self.delegate?.performActionforFeedBack("Attach", withComment: "")
    }
    @IBAction func cancel_button_pressed () {
        
        self.delegate?.performActionforFeedBack("Cancel", withComment: "")
    }
    @IBAction func send_button_pressed () {
        
        self.delegate?.performActionforFeedBack("Send", withComment: self.feedback_textView.text)
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
       
        return true
    }
    
    func textViewDidBeginEditing (_ textView: UITextView) {
        
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }

    
}
