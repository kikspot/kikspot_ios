//
//  LoginViewController.swift
//  KikSpot
//
//  Created by Shruti Mittal on 02/11/15.
//  Copyright © 2015 Shruti Mittal. All rights reserved.
//

import Foundation
import MBProgressHUD

import UIKit

class RegistrationViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet var userNameTextField: UITextField!
    @IBOutlet var passWordTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var confirmPassWordTextField: UITextField!
    @IBOutlet var refrralCodeTextField: UITextField!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var backGroundImageView: UIImageView!
    @IBOutlet var showPassword_button: UIButton!
    
    var activeField: UITextField?
    var isShowPasswordChecked : Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isShowPasswordChecked = false
        
        //        NSNotificationCenter.defaultCenter().addObserver(self, selector: "makeSpaceForKeyboard:", name: UIKeyboardWillShowNotification, object: nil)
        //        NSNotificationCenter.defaultCenter().addObserver(self, selector: "makeSpaceForKeyboard:", name: UIKeyboardWillHideNotification, object: nil)
        
        createPaddingForTextField(self.userNameTextField)
        createPaddingForTextField(self.passWordTextField)
        createPaddingForTextField(self.emailTextField)
        createPaddingForTextField(self.confirmPassWordTextField)
        createPaddingForTextField(self.refrralCodeTextField)
//        userNameTextField.text =
        
    }
    
    func createPaddingForTextField (_ textField: UITextField) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: textField.frame.height))
        textField.leftView = paddingView
        textField.leftViewMode = UITextFieldViewMode.always
        textField.delegate = self;
       // textField .setValue(defaultColor, forKeyPath: "_placeholderLabel.textColor")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        registerForKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(true)
        deregisterFromKeyboardNotifications()
    }
    
    //    deinit {
    //        deregisterFromKeyboardNotifications()
    //    }
    //
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func logIn_button_pressed (_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func signUp_button_pressed (_ sender: UIButton) {
        //        let registerSecondPage = self.storyboard?.instantiateViewControllerWithIdentifier("RegisterSecondPage") as! RegistrationSecondPage
        //        self.navigationController?.pushViewController(registerSecondPage, animated: true)
        let username = self.userNameTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let password = self.passWordTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let email = self.emailTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let confirmPassword = self.confirmPassWordTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let referralCode = self.refrralCodeTextField.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        var deviceToken :String = ""
        let userDefaults : UserDefaults = UserDefaults.standard
        
        if (userDefaults.value(forKey: userDeviceToken) != nil) {
            deviceToken =  UserDefaults.standard.value(forKey: userDeviceToken) as! String
        }
        if username == "" || password == "" || email == "" || confirmPassword == ""
        {
            self.present(Common.showAlertWithTitle("Kikspot", message: "Please fill all the fields", cancelButton: "OK"), animated: true, completion: nil)
        }
        else if (password != confirmPassword) {
            
            self.present(Common.showAlertWithTitle("Kikspot", message: "Password and confirm password do not match", cancelButton: "OK"), animated: true, completion: nil)
        }
        else if (Common.ValidateEmailString(email as NSString) == false) {
            self.present(Common.showAlertWithTitle("Kikspot", message: "Please enter proper Email Id", cancelButton: "OK"), animated: true, completion: nil)
        }
        else {
            let parameters : [String : String]  = [
                "username": username,
                "emailId": email,
                "password": password,
                "referralCode": referralCode,
                "deviceToken": deviceToken
            ]
            print (parameters)
           let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading"
                
                request(baseURL + RegisterStep1, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                        //print("Progress: \(progress.fractionCompleted)")
                    }
                    .validate { request, response, data in
                        // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                        return .success
                    }
                    .responseJSON { response in
              
                        print(response.request!)  // original URL request
                        print(response.response!) // URL response
                        print(response.data!)     // server data
                        print(response.result)   // result of response serialization
                        
                        if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                            print("JSON: \(JSON)")
                            
                            
                            //  let stats = JSON["STATUS"]
                            if let status = JSON["STATUS"] as? String{
                                // let status = stats as! String
                                UserDefaults.standard.removeObject(forKey: FbProfilePicUrl)
                                if (status == "Success") {
                                    if let user = JSON["UserId"] as? NSNumber {
                                        //let user = userId as! NSNumber
                                        UserDefaults.standard.set("\(user)" , forKey: UserId)
                                    }
                                    UserDefaults.standard.set("\(username)" , forKey: savedUserName)
                                    UserDefaults.standard.set("NO" , forKey: isUserALimitedLook)
                                    let registerSecondPage = self.storyboard?.instantiateViewController(withIdentifier: "RegisterSecondPage") as! RegistrationSecondPage
                                    registerSecondPage.parentController = "Registration"
                                    self.navigationController?.pushViewController(registerSecondPage, animated: true)
                                }
                            }
                            else {
                                if let error = JSON["ERROR"] as? String {
                                    //let error = stats as! String
                                    self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                }
                            }
                            
                            MBProgressHUD.hide(for: self.view, animated: true);
                        }
                        else {
                            MBProgressHUD.hide(for: self.view, animated: true);
                            print(response.result.error!)
                            self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                        }
                        MBProgressHUD.hide(for: self.view, animated: true);
                }
            }else {
                MBProgressHUD.hide(for: self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
            
        }
    }
    
    func registerForKeyboardNotifications()
    {
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(RegistrationViewController.keyboardWasShown(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RegistrationViewController.keyboardWillBeHidden(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    func deregisterFromKeyboardNotifications()
    {
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWasShown(_ notification: Notification)
    {
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollView.isScrollEnabled = true
        let info : NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if (activeField != nil)
        {
            if (!aRect.contains(activeField!.frame.origin))
                //                if (!CGRectContainsPoint(aRect, CGPointMake(activeField!.frame.origin.x, activeField!.frame.origin.x + 64)))
            {
                self.scrollView.scrollRectToVisible(activeField!.frame, animated: true)
            }
        }
        
    }
    
    //    func keyboardWasShown(notification: NSNotification) -> Void {
    //
    //        let info: NSDictionary = notification.userInfo!
    //
    //        if let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue().size{
    //
    //            let buttonOrigin: CGPoint = self.activeField!.frame.origin
    //
    //            let buttonHeight: CGFloat = self.activeField!.frame.size.height
    //
    //            var visibleRect: CGRect = self.view.frame
    //
    //            visibleRect.size.height -= CGFloat(keyboardSize.height) as CGFloat
    //
    //            if (!CGRectContainsPoint(visibleRect, buttonOrigin)){
    //
    //                let scrollPoint: CGPoint = CGPointMake(0.0, buttonOrigin.y - visibleRect.size.height + buttonHeight)
    //
    //                self.scrollView.setContentOffset(scrollPoint, animated: true)
    //
    //            }
    //
    //        }
    //
    //    }
    
    func keyboardWillBeHidden(_ notification: Notification)
    {
        //Once keyboard disappears, restore original positions
        //        let info : NSDictionary = notification.userInfo!
        //        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue().size
        //        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 64.0, -keyboardSize!.height, 0.0)
        self.scrollView.contentInset = UIEdgeInsets.zero
        self.scrollView.scrollIndicatorInsets = UIEdgeInsets.zero
        //        self.view.endEditing(true)
        //        self.scrollView.scrollEnabled = false
        self.activeField?.resignFirstResponder()
        self.scrollView.setContentOffset(CGPoint(x: 0, y: -64), animated: true)
        
    }
    
    
    //
    //    func textFieldDidBeginEditing(textField: UITextField) {
    //        animateViewMoving(true, moveValue: textField.frame.origin.y)
    //    }
    //    func textFieldDidEndEditing(textField: UITextField) {
    //        animateViewMoving(false, moveValue: textField.frame.origin.y)
    //    }
    //
    //    func animateViewMoving (up:Bool, moveValue :CGFloat){
    //        let movementDuration:NSTimeInterval = 0.3
    //        let movement:CGFloat = ( up ? -moveValue : moveValue)
    //        UIView.beginAnimations( "animateView", context: nil)
    //        UIView.setAnimationBeginsFromCurrentState(true)
    //        UIView.setAnimationDuration(movementDuration )
    //        self.view.frame = CGRectOffset(self.view.frame, 0,  movement)
    //        UIView.commitAnimations()
    //    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        activeField = textField
        // registerForKeyboardNotifications
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        userNameTextField.autocapitalizationType = .sentences
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        activeField = nil
    }
//    func capitalizingFirstLetter() -> String {
//        let first = String(characters.prefix(1)).capitalized
//        let other = String(characters.dropFirst())
//        return first + other
//    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    @IBAction func showPassword_button_pressed () {
        
        if isShowPasswordChecked == false {
            
            showPassword_button.setBackgroundImage(UIImage (named: "checkbox_selected.png"), for: UIControlState())
            isShowPasswordChecked = true
            self.passWordTextField.isSecureTextEntry = false
            self.confirmPassWordTextField.isSecureTextEntry = false
        }
        else {
            showPassword_button.setBackgroundImage(UIImage (named: "checkbox.png"), for: UIControlState())
            isShowPasswordChecked = false
            self.passWordTextField.isSecureTextEntry = true
            self.confirmPassWordTextField.isSecureTextEntry = true
        }
    }
    //
    //    func makeSpaceForKeyboard(notification: NSNotification) {
    //        let info = notification.userInfo!
    //        let keyboardHeight:CGFloat = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue().size.height
    //        let duration:Double = info[UIKeyboardAnimationDurationUserInfoKey] as! Double
    //
    //        if notification.name == UIKeyboardWillShowNotification {
    //            UIView.animateWithDuration(duration, animations: { () -> Void in
    //                var frame = self.view.frame
    //                frame.size.height = frame.size.height - keyboardHeight
    //                self.view.frame = frame
    //            })
    //        } else {
    //            UIView.animateWithDuration(duration, animations: { () -> Void in
    //                var frame = self.view.frame
    //                frame.size.height = frame.size.height + keyboardHeight
    //                self.view.frame = frame
    //            })
    //        }
    //        
    //    }
}
