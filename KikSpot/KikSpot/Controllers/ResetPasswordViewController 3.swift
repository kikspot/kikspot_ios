//
//  ResetPasswordViewController.swift
//  KikSpot
//
//  Created by knsmac003 on 12/16/15.
//  Copyright © 2015 Shruti Mittal. All rights reserved.
//

import UIKit
import MBProgressHUD

class ResetPasswordViewController: UIViewController {

    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet var scrollView: UIScrollView!
    var activeField: UITextField?
    
    var email : NSString!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerForKeyboardNotification()
         self.navigationItem.title = "RESET PASSWORD"
        print("email reset \(email)")
         self.setNavigationBarItem()
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.codeTextField.frame.height))
        codeTextField.leftView = paddingView
        codeTextField.leftViewMode = UITextFieldViewMode.always
        codeTextField .setValue(defaultColor, forKeyPath: "_placeholderLabel.textColor")
        
        let paddingView1 = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.newPasswordTextField.frame.height))
        newPasswordTextField.leftView = paddingView1
        newPasswordTextField.leftViewMode = UITextFieldViewMode.always
        newPasswordTextField.setValue(defaultColor, forKeyPath: "_placeholderLabel.textColor")
      
        
        let paddingView2 = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.confirmPasswordTextField.frame.height))
        confirmPasswordTextField.leftView = paddingView2
        confirmPasswordTextField.leftViewMode = UITextFieldViewMode.always
       // confirmPasswordTextField.setValue(defaultColor, forKeyPath: "_placeholderLabel.textColor")
        
       
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.deregisterFromKeyboardNotification()
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.registerForKeyboardNotification()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func resetPasswordButton(_ sender: AnyObject) {
        
        var uniqueCode : String = self.codeTextField.text!;
        var newPassword : String = self.newPasswordTextField.text!;
        var confirmPassword : String = self.confirmPasswordTextField.text!;
        
        uniqueCode = uniqueCode.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        newPassword = newPassword.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        confirmPassword = confirmPassword.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        let parameters : [String : String]  = [
            "email": email as String,
            "token": uniqueCode,
            "password":confirmPassword
        ]
        if uniqueCode == "" || newPassword == "" || confirmPassword == ""
        {
            self.present(Common.showAlertWithTitle("Kikspot", message:"Please fill the details", cancelButton: "OK"), animated: true, completion: nil)
        }
       else if( newPassword != confirmPassword){
         self.present(Common.showAlertWithTitle("Kikspot", message:"password did not match , Please try again ", cancelButton: "OK"), animated: true, completion: nil)
        }
       else {
           let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading"
                
                request(baseURL + ResetPassword, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                        //print("Progress: \(progress.fractionCompleted)")
                    }
                    .validate { request, response, data in
                        // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                        return .success
                    }
                    .responseJSON { response in
                   
                        // result of response serialization
                        
                         if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                            print("JSON: \(JSON)")
                            if let status = JSON["STATUS"] as? String {
                                
                                if (status == "success") {
                                   
                             self.DisplayAlertSuccessPassword()

                                }

                            }
                            else {
                                if let error = JSON["ERROR"] as? String {
                                    // let error = stats as! String
                                    self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                }
                            }
                            MBProgressHUD.hide(for: self.view, animated: true);
                        }
                        else {
                            MBProgressHUD.hide(for: self.view, animated: true);
                            print(response.result.error!)
                            self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                        }
                         MBProgressHUD.hide(for: self.view, animated: true);
                }
            }else {
                MBProgressHUD.hide(for: self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
        }
        
    }
    
    func DisplayAlertSuccessPassword()
    {
        
        // alert heading
        let alert = UIAlertController(title:"Kikspot", message:"Password updated successfully", preferredStyle: UIAlertControllerStyle.alert)
        
        // alert ok/send button where we are sending the email to the server
        let ok = UIAlertAction(title:"OK", style: UIAlertActionStyle.default , handler:{(action) -> Void in
            
            let loginView = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.navigationController?.pushViewController(loginView, animated: true)
            
        })
        
        alert.addAction(ok)
       
        self.present(alert , animated: true, completion: nil)
        
    }
    
    
    func registerForKeyboardNotification()
    {
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(ResetPasswordViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ResetPasswordViewController.keyboardWillBeHid(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    func deregisterFromKeyboardNotification()
    {
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillShow(_ notification: Notification)
    {
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollView.isScrollEnabled = true
        let info : NSDictionary = (notification as NSNotification).userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize!.height
        if (activeField != nil)
        {
            if (!aRect.contains(activeField!.frame.origin))
            {
                self.scrollView.scrollRectToVisible(activeField!.frame, animated: true)
            }
        }
        
        
        
    }
    
    func keyboardWillBeHid(_ notification: Notification)
    {
        //Once keyboard disappears, restore original positions
        //        let info : NSDictionary = notification.userInfo!
        //        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue().size
        //        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 64.0, -keyboardSize!.height, 0.0)
        self.scrollView.contentInset = UIEdgeInsets.zero
        self.scrollView.scrollIndicatorInsets = UIEdgeInsets.zero
        //        self.view.endEditing(true)
        //        self.scrollView.scrollEnabled = false
        self.activeField?.resignFirstResponder()
        self.scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        
    }
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        activeField = textField
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeField = textField
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        activeField = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }


}
