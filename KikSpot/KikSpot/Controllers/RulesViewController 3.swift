//
//  RulesViewController.swift
//  KikSpot
//
//  Created by Shruti Mittal on 03/02/16.
//  Copyright © 2016 Shruti Mittal. All rights reserved.
//

import Foundation
import UIKit

class RulesViewController: BaseViewController {
    
    @IBOutlet var rules_tableView: UITableView!
    var rules_array : NSMutableArray = []
    var locationId_array : NSArray = []
    var locationID: String!
    var locationName: String!
    var callServiceFore: String!
    var selectedSort: String = "close"
    @IBOutlet var tabBar: UITabBar!
    
    //    lazy   var searchBars:UISearchBar = UISearchBar(frame: CGRectMake(200,50, 200, 20))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarItem()
        self.navigationItem.title = "RULES"
        let titleDict: NSDictionary = [NSForegroundColorAttributeName: UIColor.blackColor()]
        self.navigationController!.navigationBar.titleTextAttributes = titleDict as? [String : AnyObject]
        // Do any additional setup after loading the view.
        
        if callServiceFore == "oneLocation" {
            self.getRulesofaLocation()
            self.tabBar.hidden = true
        } else if callServiceFore == "listOfLocation" {
            self.getRulesforListOfLocation()
            self.tabBar.hidden = false
        }
        else if callServiceFore == "allLocation" {
            self.getRulesforAll()
            self.tabBar.hidden = false
        }
        
        for var i = 0; i < tabBar.items?.count; i++ {
            let tabBarItem = tabBar.items![i]
            
            // Adjust tab images (Like mstysf says, these values will vary)
            tabBarItem.imageInsets = UIEdgeInsetsMake(0, 0, 0, 0);
            
            // Let's find and set the icon's default and selected states
            // (use your own image names here)
            var imageName = ""
            switch (i) {
            case 0: imageName = "close_big.png"
            case 1: imageName = "time.png"
            default: break
            }
            
            var selectedImageName = ""
            switch (i) {
            case 0: selectedImageName = "close_big_arrow.png"
            case 1: selectedImageName = "time_arrow.png"
            default: break
            }
            
            
            let screenSize: CGRect = UIScreen.mainScreen().bounds
            let screenWidth = screenSize.width
            let image1 = UIImage(named:imageName)!
            
            let size = CGSizeMake(screenWidth/2, 50)
            UIGraphicsBeginImageContextWithOptions(size, false, 0)
            image1.drawInRect(CGRectMake(0, 0, screenWidth/2, 54))
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            tabBarItem.image = image.imageWithRenderingMode(.AlwaysOriginal)
            
            
            let selectedImage = UIImage (named: selectedImageName)!
            UIGraphicsBeginImageContextWithOptions(size, false, 0)
            selectedImage.drawInRect(CGRectMake(0, 0, screenWidth/2, 50))
            let selectIamge = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            tabBarItem.selectedImage = selectIamge.imageWithRenderingMode(.AlwaysOriginal)
            
        }
        self.tabBar.selectedItem = tabBar.items![0] as UITabBarItem
        
    }
    
    
    func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        
        self.rules_array.removeAllObjects()
        if item.tag == 101 {
            
            self.selectedSort = "close"
            
        } else if item.tag == 102 {
            self.selectedSort = "time"
        }
        
        //        if callServiceFore == "oneLocation" {
        //            self.getRulesofaLocation()
        //        } else if callServiceFore == "listOfLocation" {
        if callServiceFore == "listOfLocation" {
            self.getRulesforListOfLocation()
        }
        else if callServiceFore == "allLocation" {
            self.getRulesforAll()
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getRulesofaLocation() {
        
        let userId =  NSUserDefaults.standardUserDefaults().valueForKey(UserId) as! String
        
        let parameters : [String : String]  = [
            "locationAPIId": self.locationID, //"ChIJ3RGpIJ22j4ARrZEBUTtqDXc"
            "isActive": "true",
            "checkExpiryDate": "true",
            "sortBy":"",
            "userId": userId
        ]
        print(parameters)
        if userId != ""
        {
            if  Reachability.isConnectedToNetwork() {
                let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.Indeterminate
                loadingNotification.labelText = "Loading"
                request(.POST, baseURL + getRulesOfAParticularLocation, parameters: parameters, encoding :.JSON)
                    .responseJSON { response in
                        // result of response serialization
                        let convertedString = NSString(data: response.data!, encoding: NSUTF8StringEncoding)
                        print(convertedString)
                        if let JSON = response.result.value {
                            print("JSON: \(JSON)")
                            //                            if let status = JSON["STATUS"] as? String {
                            //                                if (status == "Success") {
                            
                            if let notification = JSON ["locationGameRules"] as? NSArray{
                                
                                if notification.count > 0 {
                                    let dataHolder : NSMutableArray = []
                                    for (var i = 0; i < notification.count; i++) {
                                        let tempDic = notification .objectAtIndex(i)
                                        
                                        let rules = Rules()
                                        
                                        rules.locationName = (tempDic["locationName"] as? String!)!
                                        
                                        let gameRuleId = tempDic["gameRuleId"] as? NSNumber
                                        if (gameRuleId != nil) {
                                            rules.gameRuleId = "\(gameRuleId!)"
                                        }
                                        rules.rule = (tempDic["rule"] as? String!)!
                                        let creds = tempDic["creds"] as? NSNumber
                                        if (creds != nil) {
                                            rules.creds = "\(creds!)"
                                        }
                                        rules.expiryDate = (tempDic["expiryDate"] as? String!)!
                                        //rules.locationAPIId = (tempDic["locationAPIId"] as? String!)!
                                        let ruleLimit = tempDic["ruleLimit"] as? NSNumber
                                        rules.ruleLimit = "\(ruleLimit!)"
                                        dataHolder.addObject(rules)
                                    }
                                    self.rules_array = dataHolder
                                    self.rules_tableView.reloadData()
                                }
                            }
                            else {
                                
                                if let error = JSON["ERROR"] as? String {
                                    // let error = stats as! String
                                    self.presentViewController(Common.showAlertWithTitle("KikSpot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                }
                            }
                            
                            MBProgressHUD.hideAllHUDsForView(self.view, animated: true);
                        }
                        else {
                            MBProgressHUD.hideAllHUDsForView(self.view, animated: true);
                            print(response.result.error)
                            self.presentViewController(Common.showAlertWithTitle("KikSpot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                        }
                        MBProgressHUD.hideAllHUDsForView(self.view, animated: true);
                }
            }else {
                MBProgressHUD.hideAllHUDsForView(self.view, animated: true);
                self.presentViewController(Common.showAlertWithTitle("KikSpot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
            //            let req = request(.GET, baseURL + ListNotifications, parameters: parameters, encoding :.JSON)
            //            debugPrint(req)
        }
        
        
    }
    
    func getRulesforListOfLocation() {
        
        let userId =  NSUserDefaults.standardUserDefaults().valueForKey(UserId) as! String
        
        
        
        let parameters: [String : AnyObject] = [
            "locationAPIId": self.locationId_array,
            "isActive": "true",
            "checkExpiryDate": "true",
            "sortBy":selectedSort,
            "userId": userId
        ]
        print(parameters)
        if userId != ""
        {
            if  Reachability.isConnectedToNetwork() {
                let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.Indeterminate
                loadingNotification.labelText = "Loading"
                request(.POST, baseURL + getRulesOfAListOfLocation, parameters: parameters, encoding :.JSON)
                    .responseJSON { response in
                        // result of response serialization
                        let convertedString = NSString(data: response.data!, encoding: NSUTF8StringEncoding)
                        print(convertedString)
                        if let JSON = response.result.value {
                            print("JSON: \(JSON)")
                            //                            if let status = JSON["STATUS"] as? String {
                            //                                if (status == "Success") {
                            
                            if let notification = JSON ["gameRulesOfLocations"] as? NSArray{
                                
                                if notification.count > 0 {
                                    let dataHolder : NSMutableArray = []
                                    for (var i = 0; i < notification.count; i++) {
                                        let tempDic = notification .objectAtIndex(i)
                                        
                                        let rules = Rules()
                                        
                                        rules.locationName =  (tempDic["locationName"] as? String!)!
                                        
                                        let gameRuleId = tempDic["gameRuleId"] as? NSNumber
                                        if (gameRuleId != nil) {
                                            rules.gameRuleId = "\(gameRuleId!)"
                                        }
                                        rules.rule = (tempDic["rule"] as? String!)!
                                        let creds = tempDic["creds"] as? NSNumber
                                        if (creds != nil) {
                                            rules.creds = "\(creds!)"
                                        }
                                        rules.expiryDate = (tempDic["expiryDate"] as? String!)!
                                        //rules.locationAPIId = (tempDic["locationAPIId"] as? String!)!
                                        let ruleLimit = tempDic["ruleLimit"] as? NSNumber
                                        rules.ruleLimit = "\(ruleLimit!)"
                                        dataHolder.addObject(rules)
                                    }
                                    self.rules_array = dataHolder
                                    self.rules_tableView.reloadData()
                                }
                            }
                            else {
                                
                                if let error = JSON["ERROR"] as? String {
                                    // let error = stats as! String
                                    self.presentViewController(Common.showAlertWithTitle("KikSpot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                }
                            }
                            
                            MBProgressHUD.hideAllHUDsForView(self.view, animated: true);
                        }
                        else {
                            MBProgressHUD.hideAllHUDsForView(self.view, animated: true);
                            print(response.result.error)
                            self.presentViewController(Common.showAlertWithTitle("KikSpot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                        }
                        MBProgressHUD.hideAllHUDsForView(self.view, animated: true);
                }
            }else {
                MBProgressHUD.hideAllHUDsForView(self.view, animated: true);
                self.presentViewController(Common.showAlertWithTitle("KikSpot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
            // let req = request(.GET, baseURL + getRulesOfAListOfLocation, parameters: parameters, encoding :.JSON)
            //      debugPrint(req)
        }
        
        
    }
    
    
    func getRulesforAll() {
        
        let userId =  NSUserDefaults.standardUserDefaults().valueForKey(UserId) as! String
        let userDefaults : NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        var latitude :String = ""
        var longitude : String = ""
        if (userDefaults.valueForKey(currentLatitude) != nil) {
            latitude =  NSUserDefaults.standardUserDefaults().valueForKey(currentLatitude) as! String
        }
        if (userDefaults.valueForKey(currentLongitude) != nil) {
            longitude =  NSUserDefaults.standardUserDefaults().valueForKey(currentLongitude) as! String
        }

        let parameters : [String : String]  = [
            "isActive": "true",
            "checkExpiryDate": "true",
            "sortBy":selectedSort,
            "userId": userId,
            "latitude":latitude,
            "longitude": longitude
        ]
        print(parameters)
        if userId != ""
        {
            if  Reachability.isConnectedToNetwork() {
                let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.Indeterminate
                loadingNotification.labelText = "Loading"
                request(.POST, baseURL + getRulesForAll, parameters: parameters, encoding :.JSON)
                    .responseJSON { response in
                        // result of response serialization
                        let convertedString = NSString(data: response.data!, encoding: NSUTF8StringEncoding)
                        print(convertedString)
                        if let JSON = response.result.value {
                            print("JSON: \(JSON)")
                            //                            if let status = JSON["STATUS"] as? String {
                            //                                if (status == "Success") {
                            
                            if let notification = JSON ["locationGameRules"] as? NSArray{
                                
                                if notification.count > 0 {
                                    let dataHolder : NSMutableArray = []
                                    for (var i = 0; i < notification.count; i++) {
                                        let tempDic = notification .objectAtIndex(i)
                                        
                                        let rules = Rules()
                                        
                                        //  rules.locationName = self.locationName
                                        rules.locationName =  (tempDic["locationName"] as? String!)!
                                        let gameRuleId = tempDic["gameRuleId"] as? NSNumber
                                        if (gameRuleId != nil) {
                                            rules.gameRuleId = "\(gameRuleId!)"
                                        }
                                        rules.rule = (tempDic["rule"] as? String!)!
                                        let creds = tempDic["creds"] as? NSNumber
                                        if (creds != nil) {
                                            rules.creds = "\(creds!)"
                                        }
                                        rules.expiryDate = (tempDic["expiryDate"] as? String!)!
                                        //rules.locationAPIId = (tempDic["locationAPIId"] as? String!)!
                                        let ruleLimit = tempDic["ruleLimit"] as? NSNumber
                                        rules.ruleLimit = "\(ruleLimit!)"
                                        dataHolder.addObject(rules)
                                    }
                                    self.rules_array = dataHolder
                                    self.rules_tableView.reloadData()
                                }
                            }
                            else {
                                
                                if let error = JSON["ERROR"] as? String {
                                    // let error = stats as! String
                                    self.presentViewController(Common.showAlertWithTitle("KikSpot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                }
                            }
                            
                            MBProgressHUD.hideAllHUDsForView(self.view, animated: true);
                        }
                        else {
                            MBProgressHUD.hideAllHUDsForView(self.view, animated: true);
                            print(response.result.error)
                            self.presentViewController(Common.showAlertWithTitle("KikSpot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                        }
                        MBProgressHUD.hideAllHUDsForView(self.view, animated: true);
                }
            }else {
                MBProgressHUD.hideAllHUDsForView(self.view, animated: true);
                self.presentViewController(Common.showAlertWithTitle("KikSpot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
            //            let req = request(.GET, baseURL + ListNotifications, parameters: parameters, encoding :.JSON)
            //            debugPrint(req)
        }
        
        
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1;
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.rules_array.count
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        
        //  let cellidentifier = "cell"
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as UITableViewCell
        let rule = rules_array[indexPath.section] as! Rules
        
        
        let locationName_label = cell.viewWithTag(101) as? UILabel
        let rule_label = cell.viewWithTag(102) as? UILabel
        let ruleLimit_label = cell.viewWithTag(103) as? UILabel
        let cred_label = cell.viewWithTag(104) as? UILabel
        let expiryDate_label = cell.viewWithTag(105) as? UILabel
        
        
        rule_label!.numberOfLines = 0
        rule_label!.lineBreakMode = NSLineBreakMode.ByWordWrapping
        let font =  UIFont.systemFontOfSize(15)
        rule_label!.font = font
        rule_label!.text = rule.rule
        
        rule_label!.sizeToFit()
        
        locationName_label?.adjustsFontSizeToFitWidth = true
        ruleLimit_label?.adjustsFontSizeToFitWidth = true
        cred_label?.adjustsFontSizeToFitWidth = true
        expiryDate_label?.adjustsFontSizeToFitWidth = true
        
        locationName_label?.text = rule.locationName
        let ruleLimit = (rule.ruleLimit)
        if ruleLimit == "-1" {
            ruleLimit_label?.text = ""
        }else {
        ruleLimit_label?.text = "Rule Limit : \(rule.ruleLimit)"
        }
        cred_label?.text = "Creds : \(rule.creds)"
        
        let expiry = (rule.expiryDate)
        if expiry == "" {
            expiryDate_label?.text = ""
        }else {
            expiryDate_label?.text = "Expiry Date  : \(rule.expiryDate)"
        }

        //expiryDate_label?.text = "Expiry Date : \(rule.expiryDate)"
        
        
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, 40))
        footerView.backgroundColor = UIColor.clearColor()
        
        return footerView
    }
    
}