//
//  PreferencesViewController.swift
//  KikSpot
//
//  Created by Shruti Mittal on 04/02/16.
//  Copyright © 2016 Shruti Mittal. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}


class PreferencesViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var preferences_tableView: UITableView!
    @IBOutlet weak var skip_button: UIButton!
    var answers_tableView: UITableView  = UITableView()
    
    var preferencesArray : NSMutableArray = []
    var answerArray : NSMutableArray = []
    var userAnswerIdArray : NSMutableArray = []
    var userAnswerIdDictionary : NSMutableDictionary = [:]
    var activeField: UITextField?
    var selectedSection : Int = 0
    var numberOfRun : Int = 1
    var parentController : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.preferences_tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.preferences_tableView.delegate = self;
        self.preferences_tableView.dataSource = self;
        self.preferences_tableView.backgroundColor = UIColor.clear
        if self.parentController == "Registration" {
            self.skip_button.isHidden = false
            removeNavigationBarItem()
        }
        else {
            self.skip_button.isHidden = true
            self.setNavigationBarItem()
        }
        getuserpreferances()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//      func getuserpreferances() {
//        let userId =  UserDefaults.standard.value(forKey: UserId) as! String
//
//        let parameters : [String : String]  = [
//            "userId": userId
//        ]
//        if userId != ""
//        {
//            let reachability: Reachability = Reachability.forInternetConnection()
//            if  reachability.currentReachabilityStatus().rawValue != 0  {
//                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
//                loadingNotification.mode = MBProgressHUDMode.indeterminate
//                loadingNotification.label.text = "Loading"
//
//                request( baseURL + userPreferance, method: .post, parameters: parameters, encoding: JSONEncoding.default)
//                    .downloadProgress(queue: DispatchQueue.utility) { progress in
//                        //print("Progress: \(progress.fractionCompleted)")
//                    }
//                    .validate { request, response, data in
//                        // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
//                        return .success
//                    }
//                    .responseJSON { response in
//                        if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
//                            print("JSON: \(JSON)")
//
//                            if let userPreferences = JSON ["userPreferences"] as? NSArray{
//                                  if userPreferences.count > 0 {
//                                     let dataHolder : NSMutableArray = []
//                                    for i in 0 ..< userPreferences.count {
//                                        let tempDic = userPreferences .object(at: i) as! [String : AnyObject]
//                                        let preferences = UserPreference()
//                                        //self.pointerToken =  (JSON ["pageToken"] as? String)!
//                                        preferences.preferenceItem = (tempDic["preferenceItem"] as? String)!
//                                        let prefId = tempDic["preferenceId"] as? NSNumber
//                                        if (prefId != nil) {
//                                            preferences.preferenceId = "\(prefId!)"
//                                        }
//                                        preferences.preferenceOptions = (tempDic["preferenceOptions"] as? NSArray)!
//                                        dataHolder .add(preferences)
//                                    }
//
//                                }
//                            }
//
////                            if let userPreferences = JSON ["userPreferences"] as? NSArray{
////
////                                if userPreferences.count > 0 {
////                                    let dataHolder : NSMutableArray = []
////                                    for i in 0 ..< userPreferences.count {
////                                        let tempDic = userPreferences .object(at: i) as! [String : AnyObject]
////
////                                        let preferences = UserPreference()
////
////                                        preferences.preferenceItem = (tempDic["preferenceItem"] as? String!)!
////
////                                        let prefId = tempDic["preferenceId"] as? NSNumber
////                                        if (prefId != nil) {
////                                            preferences.preferenceId = "\(prefId!)"
////                                        }
////                                        preferences.preferenceOptions = (tempDic["preferenceOptions"] as? NSArray!)!
////                                        //rules.locationAPIId = (tempDic["locationAPIId"] as? String!)!
////                                        dataHolder .add(preferences)
////                                    }
////                                    self.preferencesArray = dataHolder
////
////                                    for i in 0  ..< self.preferencesArray.count  {
////                                        let preference = self.preferencesArray[i] as! UserPreference
////                                        let prefArr = preference.preferenceOptions
////                                        for j in 0  ..< prefArr.count  {
////                                            let tempDic = prefArr[j] as! [String : AnyObject]
////                                            var isSelected = ""
////                                            let selectId = tempDic["isSelected"] as? NSNumber
////                                            if (selectId != nil) {
////                                                isSelected = "\(selectId!)"
////                                            }
////                                            if self.numberOfRun == 1 {
////                                                if isSelected == "1" {
////                                                    //  let string = (tempDic["option"] as? String)!
////                                                    // self.answerArray[i] = (tempDic["option"] as? String)!
////                                                    self.answerArray .insert((tempDic["option"] as? String)!, at: i)
////                                                    break;
////                                                }
////                                                else {
////                                                    //  self.answerArray[i] = ""
////                                                    self.answerArray .insert("", at: i)
////                                                }
////                                            }
////                                        }
////                                    }
////
////                                    self.preferences_tableView.reloadData()
////                                }
////                            }
////                            else {
////
////                                if let error = JSON["ERROR"] as? String {
////                                    // let error = stats as! String
////                                    if (error == "User Preferences Not Found") {
////                                        if self.parentController == "Registration" {
////                                            let recommend = self.storyboard?.instantiateViewController(withIdentifier: "tabbarcontroller") as! TabBarViewController
////                                            self.navigationController?.pushViewController(recommend, animated: true)
////                                            self.present(Common.showAlertWithTitle("Kikspot", message: "You have successfully logged in", cancelButton: "OK"), animated: true, completion: nil)
////                                        }
////                                        else {
////                                            self.navigationController?.popViewController(animated: true)
////                                        }
////                                        self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
////                                    }else {
////                                        self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
////                                    }
////                                }
////
////                            }
//                        }
//                        MBProgressHUD.hide(for: self.view, animated: true);
//                }
//
//            }
//            else {
//                MBProgressHUD.hide(for: self.view, animated: true);
//                self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
//            }
//
//        }
//
//    }
    
    func getuserpreferances() {
        let userId =  UserDefaults.standard.value(forKey: UserId) as! String
        let parameters : [String : String]  = [
            "userId": userId
        ]
        if userId != "" {
           let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading"
                request( baseURL + userPreferance, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                    }
                    .validate { request, response, data in
                        return .success
                    }
                    .responseJSON { response in
                        if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                            print("JSON: \(JSON)")
                            if let userPreferences = JSON ["userPreferences"] as? NSArray{
                                if userPreferences.count > 0 {
                                    let dataHolder : NSMutableArray = []
                                    for i in 0 ..< userPreferences.count {
                                        let tempDic = userPreferences .object(at: i) as! [String : AnyObject]
                                        let preferences = UserPreference()

                                        preferences.preferenceItem = (tempDic["preferenceItem"] as? String)!
                                        let prefId = tempDic["preferenceId"] as? NSNumber
                                        if (prefId != nil) {
                                            preferences.preferenceId = "\(prefId!)"
                                        }
                                        preferences.preferenceOptions = (tempDic["preferenceOptions"] as? NSArray)!
                                        dataHolder .add(preferences)
                                    }
                                    self.preferencesArray = dataHolder
                                    for i in 0  ..< self.preferencesArray.count  {
                                        let preference = self.preferencesArray[i] as! UserPreference
                                        let prefArr = preference.preferenceOptions
                                        for j in 0  ..< prefArr.count  {
                                            let tempDic = prefArr[j] as! [String : AnyObject]
                                            var isSelected = ""
                                            let selectId = tempDic["isSelected"] as? NSNumber
                                            if (selectId != nil) {
                                                isSelected = "\(selectId!)"
                                            }
                                            if self.numberOfRun == 1 {
                                                if isSelected == "1" {
                                                    self.answerArray .insert((tempDic["option"] as? String)!, at: i)
                                                    break;
                                                }
                                                else {
                                                    self.answerArray .insert("", at: i)
                                                }
                                            }
                                        }
                                    }
                                    self.preferences_tableView.reloadData()
                                }
                            }
                            else {

                                if let error = JSON["ERROR"] as? String {
                                    if (error == "User Preferences Not Found") {
                                        if self.parentController == "Registration" {
                                            let recommend = self.storyboard?.instantiateViewController(withIdentifier: "tabbarcontroller") as! TabBarViewController
                                            self.navigationController?.pushViewController(recommend, animated: true)
                                            self.present(Common.showAlertWithTitle("Kikspot", message: "You have successfully logged in", cancelButton: "OK"), animated: true, completion: nil)
                                        }
                                        else {
                                            self.navigationController?.popViewController(animated: true)
                                        }
                                        self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                    }else {
                                        self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                    }
                                }

                            }
                        }
                        MBProgressHUD.hide(for: self.view, animated: true);
                }        }
            else {
                MBProgressHUD.hide(for: self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
            }

        }
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 1 {
            return 1
        }else {
            let preference = preferencesArray[selectedSection] as! UserPreference
            return preference.preferenceOptions.count
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if tableView.tag == 1 {
            return self.preferencesArray.count
        }else {
            return 1
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if tableView.tag == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as UITableViewCell
            let preference = preferencesArray[(indexPath as NSIndexPath).section] as! UserPreference
            let question_label = cell.viewWithTag(101) as? UILabel
            let answer_textField = cell.contentView.viewWithTag(102) as? UITextField
            answer_textField?.delegate = self
            
            //answer_textField?.addTarget(self, action: "onTextChanged:", forControlEvents: UIControlEvents.EditingChanged)
            if (self.answerArray.count > (indexPath as NSIndexPath).section) {
               // print(self.answerArray[(indexPath as NSIndexPath).section] as? String)
                answer_textField?.text = self.answerArray[(indexPath as NSIndexPath).section] as? String
            }
            question_label?.adjustsFontSizeToFitWidth = true
            question_label?.text = preference.preferenceItem
            
            numberOfRun += 1
            
            return cell
            
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "answerCell", for: indexPath) as UITableViewCell
            
            let preference = preferencesArray[selectedSection] as! UserPreference
            let prefArr = preference.preferenceOptions
            let tempDic = prefArr[(indexPath as NSIndexPath).row] as! [String : AnyObject]
            cell.textLabel?.text = tempDic["option"] as? String
            let selectId = tempDic["isSelected"] as? NSNumber
            var isSelected = ""
            if (selectId != nil) {
                isSelected = "\(selectId!)"
            }
            if isSelected == "1" {
                cell.accessoryType = UITableViewCellAccessoryType.checkmark
            }
            else
            {
                cell.accessoryType = UITableViewCellAccessoryType.none
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    { if tableView.tag == 1 {
        let cell = self.preferences_tableView.cellForRow(at: indexPath)
        let answer_textField = cell!.viewWithTag(102) as? UITextField
        answer_textField?.isUserInteractionEnabled = true
        answer_textField?.becomeFirstResponder()
    }
    else {
        // let cell = self.preferences_tableView.cellForRowAtIndexPath(indexPath)
        let preference = preferencesArray[selectedSection] as! UserPreference
        let prefArr = preference.preferenceOptions
        let tempDic = prefArr[(indexPath as NSIndexPath).row] as! [String : AnyObject]
        //userAnswerIdArray .addObject(<#T##anObject: AnyObject##AnyObject#>)
        // //let answer_textField = tableViewCell.contentView.viewWithTag(102) as? UITextField
        activeField?.text = tempDic["option"] as? String
        var value = ""
        let prefId = tempDic["userPreferenceOptionsId"] as? NSNumber
        if (prefId != nil) {
            value = "\(prefId!)"
        }
        let key = preference.preferenceItem
        userAnswerIdDictionary.setValue(value, forKey: key)
        
        print((tempDic["option"] as? String)!)
        //answerArray[selectedSection] = (tempDic["option"] as? String)!
        //self.answerArray .insertObject((tempDic["option"] as? String)!, atIndex: selectedSection)
        self.answerArray.replaceObject(at: selectedSection, with: (tempDic["option"] as? String)!)
        }
        
        answers_tableView .removeFromSuperview()
    }
    
    
    //    func onTextChanged(sender: UITextField) {
    //        let cell = sender.superview?.superview as! UITableViewCell
    //        let indexPath = self.preferences_tableView.indexPathForCell(cell)!
    //        answerArray[indexPath.section] = sender.text!
    //    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
        footerView.backgroundColor = UIColor.clear
        
        return footerView
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        
        // registerForKeyboardNotifications
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        activeField = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        let tableViewCell = textField.superview?.superview as! UITableViewCell
        activeField = tableViewCell.contentView.viewWithTag(102) as? UITextField
        
        let indexPath = self.preferences_tableView.indexPath(for: tableViewCell)
     //   print((indexPath as NSIndexPath?)?.section)
        selectedSection = ((indexPath as NSIndexPath?)?.section)!
        
        activeField = textField
        if (indexPath as NSIndexPath?)?.section >= self.preferencesArray.count - 2 {
            answers_tableView.frame         =   CGRect(x: activeField!.frame.origin.x, y: tableViewCell.frame.origin.y + activeField!.frame.origin.y - 200 , width: activeField!.frame.size.width, height: 200);
        }else {
            answers_tableView.frame         =   CGRect(x: activeField!.frame.origin.x, y: activeField!.frame.origin.y + activeField!.frame.size.height + tableViewCell.frame.origin.y , width: activeField!.frame.size.width, height: 200);
        }
        // answers_tableView.frame         =   CGRectMake(activeField!.frame.origin.x, activeField!.frame.origin.y + activeField!.frame.size.height + tableViewCell.frame.origin.y , activeField!.frame.size.width, 200);
        answers_tableView.delegate      =   self
        answers_tableView.dataSource    =   self
        answers_tableView.tag = 2
        answers_tableView.register(UITableViewCell.self, forCellReuseIdentifier: "answerCell")
        answers_tableView.reloadData()
        self.preferences_tableView.addSubview(answers_tableView)
        return false
        
    }
    
    @IBAction func submit_button_pressed (){
        let userId =  UserDefaults.standard.value(forKey: UserId) as! String
        
        let answerArray = self.userAnswerIdDictionary.allValues
        
        if self.userAnswerIdDictionary.count == 0
        {
            self.present(Common.showAlertWithTitle("Kikspot", message: "Please select atleast one preference", cancelButton: "OK"), animated: true, completion: nil)
        }else {
            
            
            let parameters: [String : AnyObject] = [
                "options":answerArray as AnyObject,
                "userId": userId as AnyObject
            ]
            print(parameters)
            if userId != ""
            {
               let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                    let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                    loadingNotification.mode = MBProgressHUDMode.indeterminate
                    loadingNotification.label.text = "Loading"
                    request(baseURL + saveUserPreference, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                        .downloadProgress(queue: DispatchQueue.utility) { progress in
                            //print("Progress: \(progress.fractionCompleted)")
                        }
                        .validate { request, response, data in
                            // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                            return .success
                        }
                        .responseJSON { response in
                            if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                    
                                print("JSON: \(JSON)")
                                
                                
                                if let status = JSON["STATUS"] as? String {
                                    if (status == "User Preferences Saved Successfully") {
                                        
                                        
                                        if self.parentController == "Registration" {
                                            
//                                            let dashBoardViewController = self.storyboard?.instantiateViewControllerWithIdentifier("Dashboard") as! DashboardViewController
//                                            self.navigationController?.pushViewController(dashBoardViewController, animated: true)
                                            
                                            let recommend = self.storyboard?.instantiateViewController(withIdentifier: "tabbarcontroller") as! TabBarViewController
                                            self.navigationController?.pushViewController(recommend, animated: true)
//                                            let recommend = self.storyboard?.instantiateViewController(withIdentifier: "Recommend") as! RecommendationViewController
//                                            self.navigationController?.pushViewController(recommend, animated: true)
                                            self.present(Common.showAlertWithTitle("Kikspot", message: "You have successfully logged in", cancelButton: "OK"), animated: true, completion: nil)
                                            
                                            
                                            
                                        } else {
                                            self.navigationController?.popViewController(animated: true)
                                        }
                                    }
                                }
                                else {
                                    if let error = JSON["ERROR"] as? String {
                                        // let error = stats as! String
                                        self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                    }
                                }
                                
                                MBProgressHUD.hide(for: self.view, animated: true);
                            }
                            else {
                                MBProgressHUD.hide(for: self.view, animated: true);
                                print(response.result.error!)
                                self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                            }
                            MBProgressHUD.hide(for: self.view, animated: true);
                    }
                }else {
                    MBProgressHUD.hide(for: self.view, animated: true);
                    self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
                }
                // let req = request(.GET, baseURL + getRulesOfAListOfLocation, parameters: parameters, encoding :.JSON)
                //      debugPrint(req)
            }
        }
    }
    
    @IBAction func skip_button_pressed () {
        
//        let dashBoardViewController = self.storyboard?.instantiateViewControllerWithIdentifier("Dashboard") as! DashboardViewController
//        self.navigationController?.pushViewController(dashBoardViewController, animated: true)
     //   let recommend = self.storyboard?.instantiateViewController(withIdentifier: "Recommend") as! RecommendationViewController
        
        let recommend = self.storyboard?.instantiateViewController(withIdentifier: "tabbarcontroller") as! TabBarViewController
        self.navigationController?.pushViewController(recommend, animated: true)
        
     
     //   self.navigationController?.pushViewController(recommend, animated: true)
        self.present(Common.showAlertWithTitle("Kikspot", message: "You have successfully logged in", cancelButton: "OK"), animated: true, completion: nil)
        
        
    }
}
