//
//  UploadImageViewController.swift
//  KikSpot
//
//  Created by Shruti Mittal on 30/12/15.
//  Copyright © 2015 Shruti Mittal. All rights reserved.
//

import Foundation
import MBProgressHUD

class UploadImageViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITableViewDataSource,UITableViewDelegate {
    
    
    @IBOutlet weak var chooseImage_button: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var skip_button: UIButton!
    var parentController : String = ""
    
    // @IBOutlet var preference_textField: UITextField!
    var imagePicker:UIImagePickerController?=UIImagePickerController()
//    var popover:UIPopoverController?=nil
    // @IBOutlet var preference_tableView : UITableView!
    var prefString:String!
    let preferenceArray : NSMutableArray = []
    var jsonData:Data!
    let prefArray: NSArray = [
        "Club",
        "Lounge",
        "Pub",
        ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: false)
        imagePicker!.delegate = self
        
        let userDefaults : UserDefaults = UserDefaults.standard
        var FbProfilePic :String = ""
        //print("************* \(userDefaults.valueForKey(FbProfilePicUrl)!)")
        if (userDefaults.value(forKey: FbProfilePicUrl) != nil) {
            FbProfilePic =  userDefaults.value(forKey: FbProfilePicUrl) as! String
            let url = URL(string:FbProfilePic)
            let data = try? Data(contentsOf: url!)
            if data != nil {
                imageView?.image = UIImage(data:data!)
            }
            if self.parentController == "Registration" {
                self.skip_button.isHidden = false
            }
            else {
                self.skip_button.isHidden = true
            }
        }
        
        
        //        preference_tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        //        preference_tableView.numberOfRowsInSection(5)
        //        var indexPath = NSIndexPath()
        //        let cell : UITableViewCell = preference_tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as UITableViewCell
        //        cell.textLabel!.text = "hello WOrld"
        //        preference_tableView.indexPathForCell(cell)
        //view.addSubview(preference_tableView)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(false)
        
        //let textFieldFrame = self.preference_textField.frame
        //preference_tableView = UITableView(frame: (CGRectMake(textFieldFrame.origin.x, textFieldFrame.origin.y  - 100 , textFieldFrame.size.width, 100)), style: UITableViewStyle.Grouped)
        //        self.preference_tableView.delegate = self;
        //        self.preference_tableView.dataSource = self;
        //        self.preference_tableView.separatorColor = defaultColor
        //        self.preference_tableView.backgroundColor = UIColor.clearColor()
    }
    
    @IBAction func chooseImage_button_pressed(_ sender: AnyObject)
    {
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.alert)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        // Add the actions
        imagePicker?.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
        // Present the controller
//        if UIDevice.current.userInterfaceIdiom == .phone
//        {
//            self.present(alert, animated: true, completion: nil)
//        }
//        else
//        {
//            popover=UIPopoverController(contentViewController: alert)
//            popover!.present(from: chooseImage_button.frame, in: self.view, permittedArrowDirections: UIPopoverArrowDirection.any, animated: true)
//        }
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker!.sourceType = UIImagePickerControllerSourceType.camera
            self .present(imagePicker!, animated: true, completion: nil)
        }
        else
        {
            let alertWarning = UIAlertView(title:"Warning", message: "You don't have camera", delegate:nil, cancelButtonTitle:"OK", otherButtonTitles:"")
            alertWarning.show()
        }
    }
    
    func openGallary()
    {
        imagePicker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(imagePicker!, animated: true, completion: nil)
    }
    
    //PickerView Delegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        var image: UIImage!
        
        // fetch the selected image
        if picker.allowsEditing {
            image = info[UIImagePickerControllerEditedImage] as! UIImage
        } else {
            image = info[UIImagePickerControllerOriginalImage] as! UIImage
        }
        
        self.imageView.image = image
        // Do something about image by yourself
        
        // dissmiss the image picker controller window
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.imagePicker = UIImagePickerController()
        dismiss(animated: true, completion: nil)
    }
    
    //    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
    //
    //        if textField == self.preference_textField {
    //            self.view.addSubview(self.preference_tableView)
    //            return false
    //        }
    //        return true
    //    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 3;
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // let  cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell
        
        let cellIdentifier = "cell"
        var cell:UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        //you CAN check this against nil, if nil then create a cell (don't redeclare like you were doing...
        if(cell == nil)
        {
            cell = UITableViewCell(style: UITableViewCellStyle.subtitle,reuseIdentifier:cellIdentifier)
        }
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.textLabel?.textColor = defaultColor
        //        if indexPath.row == 0 {
        //            cell.textLabel?.text = "Club"
        //
        //        } else if indexPath.row == 1 {
        //            cell.textLabel?.text = "Lounge"
        //        } else if indexPath.row == 2 {
        //            cell.textLabel?.text = "Pub"
        //        }
        
        cell.textLabel?.text = self.prefArray.object(at: (indexPath as NSIndexPath).row) as? String
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let cell = tableView.cellForRow(at: indexPath)
        
        if cell!.isSelected == true
        {
            cell!.accessoryType = UITableViewCellAccessoryType.checkmark
            self.preferenceArray .add((self.prefArray.object(at: (indexPath as NSIndexPath).row) as? String)!)
            self.prefString = (self.prefArray.object(at: (indexPath as NSIndexPath).row) as? String)!
        }
        else
        {
            cell!.accessoryType = UITableViewCellAccessoryType.none
            self.preferenceArray.remove((self.prefArray.object(at: (indexPath as NSIndexPath).row) as? String)!)
        }
    }
    
    @IBAction func continue_button_pressed (_ sender: UIButton) {
        self.sendDetailsToserver()
    }
    
    
    func sendDetailsToserver () {
        if (self.imageView.image == nil){
            
            self.present(Common.showAlertWithTitle("Kikspot", message: "Please choose an image", cancelButton: "OK"), animated: true, completion: nil)
        }
            //        else if self.preferenceArray.count == 0 {
            //
            //            self.presentViewController(Common.showAlertWithTitle("Kikspot", message: "Please select a preference type", cancelButton: "OK"), animated: true, completion: nil)
            //        }
        else {
            let userId =  UserDefaults.standard.value(forKey: UserId) as! String
            
            var prameterText : String  = ""
            do {
                let theJSONData = try JSONSerialization.data(
                    withJSONObject: self.preferenceArray ,
                    options: JSONSerialization.WritingOptions(rawValue: 0))
                prameterText = NSString(data: theJSONData,
                                        encoding: String.Encoding.ascii.rawValue)! as String
                
            } catch let error as NSError {
                print(error)
            }
            
            
            let imageData = UIImageJPEGRepresentation(self.imageView.image!, 0.7)
            
            let parameters: [String : String] = [
                "preferredVenueType": "",
                "userId": userId,
                ]
            var theJSONText : NSString  = ""
            
            do {
                let theJSONData = try JSONSerialization.data(
                    withJSONObject: parameters ,
                    options: JSONSerialization.WritingOptions(rawValue: 0))
                theJSONText = NSString(data: theJSONData,
                                       encoding: String.Encoding.ascii.rawValue)!
                
                
            } catch let error as NSError {
                print(error)
            }
            
            
            let reachability: Reachability = Reachability.forInternetConnection()
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Loading"
                
                //  let urlRequest = urlRequestWith(baseURL + sendLocImageToServer, parameters: theJSONText  , imageData: imageData!)
                upload(
                    multipartFormData: { multipartFormData in
                        // multipartFormData.append(data: imageData, name: "images", fileName: "images.png", mimeType: "image/jpeg")
                        multipartFormData.append(imageData!, withName: "profileImage", fileName: "profileImage.png", mimeType: "image/jpeg")
                        multipartFormData.append(theJSONText.data(using: String.Encoding.utf8.rawValue, allowLossyConversion: false)!, withName: "json")
                    },
                    to: baseURL + uploadImage,
                    encodingCompletion: { encodingResult in
                        switch encodingResult {
                        case .success(let upload, _, _):
                            upload.responseJSON { response in
                                debugPrint(response)
                                if let JSON:[String:AnyObject] = response.result.value as! [String : AnyObject]?  {
                                    print("JSON: \(JSON)")
                                    if let status = JSON["STATUS"] as? String {
                                        
                                        if (status == "success") {
                                            let recommend = self.storyboard?.instantiateViewController(withIdentifier: "tabbarcontroller") as! TabBarViewController
                                            self.navigationController?.pushViewController(recommend, animated: true)
                                            
                                                                                    self.present(Common.showAlertWithTitle("Kikspot", message: "You have successfully logged in", cancelButton: "OK"), animated: true, completion: nil)
//                                            let preferenceView = self.storyboard?.instantiateViewController(withIdentifier: "PreferencesViewController") as! PreferencesViewController
//                                            preferenceView.parentController = self.parentController
//                                            self.navigationController?.pushViewController(preferenceView, animated: true)
                                        }
                                            
                                        else {
                                            if let error = JSON["ERROR"] as? String {
                                                // let error = stats as! String
                                                self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                                            }
                                        }
                                        MBProgressHUD.hide(for: self.view, animated: true)
                                    }
                                    else {
                                        MBProgressHUD.hide(for: self.view, animated: true);
                                       // print(response.result.error)
                                        self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                                    }
                                    MBProgressHUD.hide(for: self.view, animated: true);
                                }
                            }
                        case .failure(let encodingError):
                            print(encodingError)
                        }
                    }
                )
                
                
                //                let loadingNotification = MBProgressHUD.showAdded(to: self.view, animated: true)
                //                loadingNotification.mode = MBProgressHUDMode.indeterminate
                //                loadingNotification.label.text = "Loading"
                //
                //
                //                let urlRequest = urlRequestWithComponents(baseURL + uploadImage, parameters: theJSONText  , imageData: imageData!)
                //
                //                upload(urlRequest.0, data: urlRequest.1)
                //                    .progress { (bytesWritten, totalBytesWritten, totalBytesExpectedToWrite) in
                //                        //print("\(totalBytesWritten) / \(totalBytesExpectedToWrite)")
                //                    }
                //                    .responseJSON {response in
                //                        if let JSON = response.result.value {
                //                            print("JSON: \(JSON)")
                //                            if let status = JSON["STATUS"] as? String {
                //
                //                                if (status == "success") {
                //                                     //                                    let dashBoardViewController = self.storyboard?.instantiateViewControllerWithIdentifier("Dashboard") as! DashboardViewController
                ////                                    self.navigationController?.pushViewController(dashBoardViewController, animated: true)
                //
                //                                    let preferenceView = self.storyboard?.instantiateViewController(withIdentifier: "PreferencesViewController") as! PreferencesViewController
                //                                    preferenceView.parentController = self.parentController
                //                                    self.navigationController?.pushViewController(preferenceView, animated: true)
                //                                }
                //                            }
                //                            else {
                //                                if let error = JSON["ERROR"] as? String {
                //                                    // let error = stats as! String
                //                                    self.present(Common.showAlertWithTitle("Kikspot", message: error, cancelButton: "OK"), animated: true, completion: nil)
                //                                }
                //                            }
                //                            MBProgressHUD.hide(for: self.view, animated: true);
                //                        }
                //                        else {
                //                            MBProgressHUD.hide(for: self.view, animated: true);
                //                            print(response.result.error)
                //                            self.present(Common.showAlertWithTitle("Kikspot", message: errorMeesage, cancelButton: "OK"), animated: true, completion: nil)
                //                        }
                //                        MBProgressHUD.hide(for: self.view, animated: true);
                //                }
                //
            }else {
                MBProgressHUD.hide(for: self.view, animated: true);
                self.present(Common.showAlertWithTitle("Kikspot", message: internetNotAvailable, cancelButton: "OK"), animated: true, completion: nil)
            }
            //        let req = request(.GET, baseURL + uploadImage, parameters: parameters , encoding :.JSON)
            //        debugPrint(req)
        }
    }
    
    //    func urlRequestWithComponents(_ urlString:String, parameters: NSString, imageData:Data) -> (URLRequestConvertible, Data) {
    //
    //        // create url request to send
    //        let mutableURLRequest = NSMutableURLRequest(url: URL(string: urlString)!)
    //      //  mutableURLRequest.httpMethod = Method.POST.rawValue
    //        mutableURLRequest.httpMethod = "POST"
    //        let boundaryConstant = "myRandomBoundary12345";
    //        let contentType = "multipart/form-data; boundary="+boundaryConstant
    //        mutableURLRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
    //
    //        let uploadData = NSMutableData()
    //
    //        uploadData.append("--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
    //        uploadData.append("Content-Disposition: form-data; name=\"json\"\r\n\r\n".data(using: String.Encoding.utf8)!)
    //        uploadData.append("\(parameters)\r\n".data(using: String.Encoding.utf8)!)
    //
    //        // add image
    //        uploadData.append("\r\n--\(boundaryConstant)\r\n".data(using: String.Encoding.utf8)!)
    //        uploadData.append("Content-Disposition: form-data; name=\"profileImage\"; filename=\"profileImage.png\"\r\n".data(using: String.Encoding.utf8)!)
    //        uploadData.append("Content-Type: image/png\r\n\r\n".data(using: String.Encoding.utf8)!)
    //        uploadData.append(imageData)
    //
    //        uploadData.append("\r\n--\(boundaryConstant)--\r\n".data(using: String.Encoding.utf8)!)
    //
    //        return (ParameterEncoding.encode(mutableURLRequest, parameters: nil).0, uploadData)
    //
    //    }
    
    @IBAction func skip_button_pressed () {
        let recommend = self.storyboard?.instantiateViewController(withIdentifier: "tabbarcontroller") as! TabBarViewController
        self.navigationController?.pushViewController(recommend, animated: true)
        
        
        //   self.navigationController?.pushViewController(recommend, animated: true)
        self.present(Common.showAlertWithTitle("Kikspot", message: "You have successfully logged in", cancelButton: "OK"), animated: true, completion: nil)
//        let preferenceView = self.storyboard?.instantiateViewController(withIdentifier: "PreferencesViewController") as! PreferencesViewController
//        preferenceView.parentController = self.parentController
//        self.navigationController?.pushViewController(preferenceView, animated: true)
    }
}

