//
//  RewardsViewController.swift
//  KikSpot
//
//  Created by Apple on 03/04/18.
//  Copyright © 2018 Shruti Mittal. All rights reserved.
//

import UIKit

class RewardsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    let sectionHeads:[String] = ["Active Rewards", "Reward History"]
    
    let items = [ ["Last Society Reward ","You Have Entered 1 free drink.","Expires(02-12-2018)"], ["Marvin","You have earned 1 free appetizer.","Used 01-03-2017", "Busboys and Poets",                       "You Have Earned $5 off an antree.","Used 01-03-2018"]]

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.navigationItem.setHidesBackButton(true, animated: false)
//        
//         let homeButton : UIBarButtonItem = UIBarButtonItem(title: "< Home", style: UIBarButtonItemStyle.plain, target: self, action: #selector(RewardsViewController.goback(_:)))
//        
//        let logButton : UIBarButtonItem = UIBarButtonItem(title: "How To play", style: UIBarButtonItemStyle.plain, target: self, action: #selector(RewardsViewController.howtoplay(_:)))
//        
//        self.navigationItem.leftBarButtonItem = homeButton
//        self.navigationItem.rightBarButtonItem = logButton
//         self.tabBarController?.navigationItem.rightBarButtonItem = logButton
                }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.setHidesBackButton(true, animated: false)
        
        let homeButton : UIBarButtonItem = UIBarButtonItem(title: "< Back", style: UIBarButtonItemStyle.plain, target: self, action: #selector(RewardsViewController.goback(_:)))
        
        let logButton : UIBarButtonItem = UIBarButtonItem(title: "How To play", style: UIBarButtonItemStyle.plain, target: self, action: #selector(RewardsViewController.howtoplay(_:)))
        
        self.navigationItem.leftBarButtonItem = homeButton
//        self.navigationItem.rightBarButtonItem = logButton
        self.tabBarController?.navigationItem.rightBarButtonItem = logButton
    }
    
    func howtoplay(_ sender: UIBarButtonItem) {
        // Perform your custom actions
        // ...
        // Go  to the mentioned ViewController
        
         let gamelevel = self.storyboard?.instantiateViewController(withIdentifier: "GameLevelDetails") as!  GameLevelDetails
        
        self.navigationController?.pushViewController(gamelevel, animated: true)
    }
    
    func goback(_ sender: UIBarButtonItem) {
        // Perform your custom actions
        // ...
        // Go  to the mentioned ViewController
        
        let gamelevel = self.storyboard?.instantiateViewController(withIdentifier: "tabbarcontroller") as!  TabBarViewController
        
        self.navigationController?.pushViewController(gamelevel, animated: false)
    }

        override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return self.sectionHeads.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
                return self.sectionHeads[section]
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView()
        
        view.backgroundColor = defaultNavBarColor
        
        let label = UILabel()
        
        label.text = sectionHeads[section]
        
        label.frame = CGRect(x: 10, y: 5, width: 200, height: 35)
        
        label.textColor = UIColor.white
        
        label.font = UIFont.boldSystemFont(ofSize: 20)
        
        view.addSubview(label)
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return items[section].count
    }
    
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        // Configure the cell...
        cell.textLabel?.text = self.items[indexPath.section][indexPath.row]
        
        cell.textLabel?.numberOfLines = 1
        
        cell.textLabel?.textAlignment = NSTextAlignment.left
        
        cell.textLabel?.textColor = UIColor.white
        
        return cell
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
