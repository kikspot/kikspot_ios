//
//  AppDelegate.swift
//  KikSpot
//
//  Created by Shruti Mittal on 27/10/15.
//  Copyright © 2015 Shruti Mittal. All rights reserved.
//

import UIKit
import GoogleMaps;
import CoreLocation
import UserNotifications
import TwitterKit

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate , CLLocationManagerDelegate , UNUserNotificationCenterDelegate{
    
    var didFinishCallingLocation = false
    
    var window: UIWindow?
    
    let locationManager = CLLocationManager()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
                
        // Override point for customization after application launch.
        
        TWTRTwitter.sharedInstance().start(withConsumerKey: "peB2Y67WyITQqRiUh0XDExhao", consumerSecret: "eFZwisu2OsPVWXQVBRbXUURPmGfnldnrpV9W4FrSxBl1aCWW5w")
        
        GMSServices.provideAPIKey("AIzaSyAuGLC1myv0vqDxg84eZjdzIMW4DO2qWto")
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
    //    let rightViewController = storyboard.instantiateViewController(withIdentifier: "RightViewController") as! RightSideBarViewController
        
        // let leftViewController = storyboard.instantiateViewControllerWithIdentifier("LeftViewController") as! LeftViewController
        
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        
        // leftViewController.mainViewController = nvc
        
     //   let slideMenuController = SlideMenuController(mainViewController:nvc, leftMenuViewController: rightViewController, rightMenuViewController: rightViewController)
        
        self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        
        self.window?.rootViewController = nvc
        
        self.window?.makeKeyAndVisible()
        
        
        
        //   if #available(iOS 8.0, *) {
        
        if #available(iOS 10.0, *) {
            
            let center  = UNUserNotificationCenter.current()
            
            center.delegate = self
            
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                
                if error == nil{
                    
                    UIApplication.shared.registerForRemoteNotifications()
                    
                }
                
            }
            
        } else {
            
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            
            UIApplication.shared.registerUserNotificationSettings(settings)
            
            UIApplication.shared.registerForRemoteNotifications()
            
        }
     
        nvc.view.backgroundColor = UIColor.white
        
        if self.didFinishCallingLocation == false {
            
            self.locationManager.requestAlwaysAuthorization()
            
            self.locationManager.requestWhenInUseAuthorization()
            
            if CLLocationManager.locationServicesEnabled() {
                
                locationManager.delegate = self
                
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                
                locationManager.startUpdatingLocation()
                
            }
            
        }
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        return true
        
        //  return true
        
    }

    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        
        
        if url.host == "kikspot" {
            
            if url.path == "recommendation" {
                
                let userId =  UserDefaults.standard.value(forKey: UserId) as! String
                
                
                
                if userId != ""
                    
                {
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    
                    //  let recommend = storyboard.instantiateViewControllerWithIdentifier("Recommend") as! RecommendationViewController
                    
                    // self.navigationController?.pushViewController(recommend, animated: true)
                    
                    
                    
                    let mainViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                    
                    
                    
                    //                    let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
                    
                    
                    
                    
                    
                    //                    let mainViewController = storyboard.instantiateViewController(withIdentifier: "Recommend") as! RecommendationViewController
                    
               //     let rightViewController = storyboard.instantiateViewController(withIdentifier: "RightViewController") as! RightSideBarViewController
                    
                    let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
                    
                //    let slideMenuController = SlideMenuController(mainViewController:nvc, leftMenuViewController: rightViewController, rightMenuViewController: rightViewController)
                    
                    self.window?.rootViewController = nvc
                    
                    // self.slideMenuController()?.changeMainViewController(nvc, close: true)
                    
                    
                    
                }
                    
                else {
                    
                    
                    
                }
                
            }
            
        }
        
        
        
        FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        
        
        return true
        
    }
    
    
    
    @available(iOS 10.0, *)
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        
        
        TWTRTwitter.sharedInstance().application(app, open: url, options:options)
        
        
        
        FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        
        return true
       
    }
    
    
    func application(_ application: UIApplication,didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        //send this device token to server
        
        var deviceTokenString = ""
        
        for i in 0..<deviceToken.count {
            
            deviceTokenString = deviceTokenString + String(format: "%02.2hhx", arguments: [deviceToken[i]])
            
        }
        
        print(deviceTokenString)
        
        UserDefaults.standard.set("\(deviceTokenString)" , forKey: userDeviceToken)
        
    }
    
    
    
    //Called if unable to register for APNS.
    
    @available(iOS 10.0, *)
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
    }
    
    @available(iOS 10.0, *)
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        print("Recived: \(userInfo)")
        
        //Parsing userinfo:
        
        //let temp : NSDictionary = userInfo
        
        if let info = userInfo["aps"] as? Dictionary<String, AnyObject>
            
        {
            
            let notification = info["alert"] as! NSDictionary
            
            let alertMessage = notification["body"] as! String
            
            let alert = UIAlertController(title: "KikSpot", message: alertMessage, preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
            
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        locationManager.stopUpdatingLocation()
        
        UserDefaults.standard.set("\(locValue.latitude)" , forKey: currentLatitude)
        
        UserDefaults.standard.set("\(locValue.longitude)" , forKey: currentLongitude)
        
        var country : String = ""
        
        var city  : String = ""
        
        let geocoder = CLGeocoder()
        
        let location = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        
        geocoder.reverseGeocodeLocation(location) {
            
            (placemarks, error) -> Void in
            
            if let validPlacemark = placemarks?[0]{
                
                let placemark = validPlacemark as CLPlacemark
                
                print(placemark.addressDictionary)
                
                if let city1 = placemark.addressDictionary!["City"] as? NSString {
                    
                    city = city1 as String
                    
                }
                
                if let country1 = placemark.addressDictionary!["Country"] as? NSString {
                    
                    country  = country1 as String
                    
                }
                
            }
            
            if self.didFinishCallingLocation == false {
                
                self.sendLocationDetailsToServer("\(locValue.latitude)", longitude: "\(locValue.longitude)", city: city , country: country)
                
            }
            
            self.didFinishCallingLocation = true
            
        }
      
    }
    
    
    
    func sendLocationDetailsToServer( _ latitude:String, longitude:String, city:String, country: String) {
        
        
        locationManager.stopUpdatingLocation()
        
        //let userId =  NSUserDefaults.standardUserDefaults().valueForKey(UserId) as! String
        
        let userDefaults : UserDefaults = UserDefaults.standard
        
        var userId :String = ""
        
        if (userDefaults.value(forKey: UserId) != nil) {
            
            userId =  userDefaults.value(forKey: UserId) as! String
            
        }
        
        let parameters : [String : String]  = [
            
            "userId": userId,
            
            "latitude": latitude,
            
            "longitude": longitude,
            
            "city": city,
            
            "country": country
            
        ]
        
        print (parameters)
        
        if userId != ""
            
        {
            
            let reachability: Reachability = Reachability.forInternetConnection()
            
            if  reachability.currentReachabilityStatus().rawValue != 0  {
                
                request(baseURL + updateUserLocationToServer, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                    
                    .downloadProgress(queue: DispatchQueue.utility) { progress in
                        
                        //print("Progress: \(progress.fractionCompleted)")
                        
                    }
                    
                    .validate { request, response, data in
                        
                        // Custom evaluation closure now includes data (allows you to parse data to dig out error messages if necessary)
                        
                        return .success
                        
                    }
                    
                    .responseJSON { response in
                        
                        if let JSON:[String:AnyObject]   = response.result.value as! [String : AnyObject]?  {
                            
                            print("JSON: \(JSON)")
                            
                        }
                     
                }
            
            }
            
        }
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        
    }
    
    /*func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
     
     return Twitter.sharedInstance().application(app, open: url, options: options)
     
     }*/
  
    
}
